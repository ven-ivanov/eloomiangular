/**
 * Created by Venelin Ivanov on 1/27/15.
 *
 */
angular.module('xenon.controllers')
    .controller('TeamsCtrl',['$scope','$rootScope','$http','$location','$filter','Team','Alert', function($scope, $rootScope, $http, $location,$filter,Team,Alert) {
        var urlBase = '/api/team?include=user_count,course_count';
        var onResourceComplete = function(response) {
            $scope.resources = response.data;
            console.log(response.data);
            $rootScope.isLoading = 'loaded';
        };

        var onError = function(reason) {
            $scope.error = "Could not fetch data";
        };

        $http.get(urlBase)
            .then(onResourceComplete, onError);

        //team bulk selector starts here
        $scope.allTeamSelect = false;

        //buck select management
        $scope.selected = {
            teams: {},
            action: "delete"
        };


        //team bulk actions
         $scope.changeAllTeamSelect = function() {
                 $scope.allTeamSelect = !$scope.allTeamSelect;
                 if(!$scope.allTeamSelect)
                 {
                     $scope.selected.teams = [];

                 }else{
                     $scope.selected.teams = angular.copy($scope.resources.data);
                 }
         }

        //bulk action
        $scope.applyTeamBulkAction = function ()
        {

            //populate team ids
            var team_ids = {ids: []};
            angular.forEach($scope.selected.teams, function(team){
               team_ids.ids.push(team.id);
            });

            //call action
            if($scope.selected.action=='delete')
            {
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                Team.multidelete(team_ids)
                    .then(function(data){
                        console.log('success');
                        for(var i=0; i< team_ids.ids.length; i++) {
                            var found = $filter('filter')($scope.resources.data, {id: team_ids.ids[i]}, true);
                            if(found.length) //found
                                $scope.resources.data.splice($scope.resources.data.indexOf(found[0]),1);
                                //$scope.selected.teams.splice(i, 1);
                        }
                        $rootScope.isLoading = 'loaded';
                    }).catch(function(data){
                        console.log('error');
                        $rootScope.isLoading = 'loaded';
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            }
        }
    }])
    .controller('TeamUpdateCtrl', ['$scope','$rootScope','$stateParams','$location','$state','$http','Team','Alert',
        function($scope, $rootScope, $stateParams, $location, $state,$http,  Team, Alert){
            console.log($stateParams);
            var urlBase = '/api/team/'+$stateParams.id+'?include=users.image,courses';
            console.log(urlBase);
            $scope.teaminfo = {};

            //this is for adding users on modal
            $scope.usermodal = {
                allUserSelect : false,
                selected: { users:{}}
            };
            //this is for removing users
            $scope.allUserSelect = false;
            //buck select management
            $scope.selected = {
                users: {},
            };

            //preprocess image info


            var onResourceComplete = function(response) {
                $scope.teaminfo = {
                        id       : response.data.id,
                        name     : response.data.name,
                    description  : response.data.description,
                        users    : response.data.users.data,
                        courses  : response.data.courses.data
                };

                //convert image info to image
                angular.forEach($scope.teaminfo.users, function(user)
                {
                    if(user.image == null || typeof user.image === 'undefined' )
                        user.image  = {path: null, id: null};
                });
                console.log(response);

                //all users
                //possible uses to add
                var urlBaseAll = '/api/user';
                $http.get(urlBaseAll)
                    .then(onResourceCompleteAll, onErrorAll);
            };

            var onError = function(reason) {
                $scope.error = "Could not fetch data";
            };

            $http.get(urlBase)
                .then(onResourceComplete, onError);


            onResourceCompleteAll = function(response) {
                $scope.usermodal.allUsers = response.data.data;

                console.log('all users');
                console.log($scope.usermodal.allUsers);

                angular.forEach($scope.teaminfo.users, function(user){
                    var found = _.find($scope.usermodal.allUsers,{id: user.id});
                   $scope.usermodal.allUsers.splice($scope.usermodal.allUsers.indexOf(found),1);
                });
                //remove users already added
            };

            onErrorAll = function(reason) {
                $scope.error = "Could not fetch data";
            };


            //change all user select
            $scope.changeAllUserSelect = function() {
                $scope.allUserSelect = !$scope.allUserSelect;
                if(!$scope.allUserSelect)
                {
                    $scope.selected.users = [];

                }else{
                    $scope.selected.users = angular.copy($scope.teaminfo.users);
                }

            }


            //change all user select
            $scope.changeAllAddUserSelect = function() {
                //$scope.allUserSelect = !$scope.allUserSelect;
                if(!$scope.usermodal.allUserSelect)
                {
                    $scope.usermodal.selected.users = [];

                }else{
                    $scope.usermodal.selected.users = angular.copy($scope.usermodal.allUsers);
                }
            }

            //update user data
            $scope.updateTeam = function(userForm) {
                if(!userForm.$valid) return;

                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get userinfo
                var teaminfo = $scope.teaminfo;

                var data = {
                            name      : teaminfo.name,
                    description       : teaminfo.description
                };

                Team.updateteam(teaminfo.id,data).
                    then(function(team){
                        alert('team updated' + teaminfo.id);
                        console.log(team);
                        $scope.teaminfo.name = team.name;
                        $scope.teaminfo.description = team.description;
                        $state.go("app.organization.teams");
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            };

            //delete team
            $scope.deleteTeam = function(){
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                Team.deleteteam([$scope.teaminfo.id])
                    .then(function(data){
                        console.log(data);
                        $state.go("app.organization.teams");
                    })
                    .catch(function(errors){
                        console.log(errors);
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            }

            //parent funct
            $scope.parentUpdate = function(newUsers){
                console.log('modal updated');
                console.log(newUsers);
                angular.forEach(newUsers,function(newUser){
                    $scope.teaminfo.users.push(newUser);
                });
                angular.forEach($scope.teaminfo.users, function(user){
                    user.first_name = 'activated';
                });
                console.log($scope.teaminfo.users);
            }


            //bulk action
            $scope.applyUserBulkAction = function (action)
            {
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //populate team ids
                var user_ids = {user_ids: []};
                angular.forEach($scope.teaminfo.users, function(user){
                    user_ids.user_ids.push(user.id);
                });

                //call action
                if(action=='delete')
                {
                    //remove users
                    angular.forEach($scope.selected.users, function(user){
                        var index = user_ids.user_ids.indexOf(user.id);
                        //remove from current user base
                        user_ids.user_ids.splice(index,1);
                    });

                    Team.deleteusers($scope.teaminfo.id,user_ids)
                        .then(function(data){
                            console.log('success');
                            for(var i=0; i< $scope.selected.users.length; i++) {
                               // var found = $filter('filter')($scope.teaminfo.users, {id:$scope.selected.users[i].id}, true);
                                var found = _.find($scope.teaminfo.users,{id:$scope.selected.users[i].id});
                                $scope.teaminfo.users.splice($scope.teaminfo.users.indexOf(found),1);
                            }
                            $scope.selected.users = [];
                            $rootScope.isLoading = 'loaded';
                        }).catch(function(data){
                            console.log('error');
                            $rootScope.isLoading = 'loaded';
                        }).finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });
                }else if(action =='add')
                {
                    //remove users
                    angular.forEach($scope.usermodal.selected.users, function(user){
                        user_ids.user_ids.push(user.id);
                    });

                    console.log('adding useres');
                    console.log(user_ids);

                    Team.addusers($scope.teaminfo.id,user_ids)
                        .then(function(data){
                            console.log('success');
                            //add existing
                            for(var i=0; i< $scope.usermodal.selected.users.length; i++) {
                                //add there
                                var index = _.find($scope.usermodal.allUsers, {id: $scope.usermodal.selected.users[i].id});
                                $scope.usermodal.allUsers.splice($scope.usermodal.allUsers.indexOf(index), 1);
                            }
                            var returnUsers = angular.copy($scope.usermodal.selected.users);
                            $scope.usermodal.selected.users = [];
                            //remove from pool

                            //reload
                            $rootScope.isLoading = 'loaded';
                            $rootScope.currentModal.close();
                            $state.go('app.organization.teams.team',{id : $stateParams.id },{reload : true})
                            //$scope.parentUpdate(returnUsers);
                           // $scope.$emit('modalupdate',returnUsers);
                        }).catch(function(data){
                            console.log('error');

                        }).finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });
                }
            }

        }])
    .controller('TeamCourseUpdateCtrl', ['$scope','$rootScope','$stateParams','$location','$state','$http','Team','Alert',
        function($scope, $rootScope, $stateParams, $location, $state,$http,  Team, Alert){
            console.log($stateParams);
            var urlBase = '/api/team/'+$stateParams.id+'?include=courses';

            console.log(urlBase);
            $scope.teaminfo = {};

            //this is for adding users on modal
            $scope.coursemodal = {
                allCourseSelect : false,
                selected: { courses:{}}
            };
            //this is for removing courses
            $scope.allCourseSelect = false;
            //buck select management
            $scope.selected = {
                courses: {},
            };

            //preprocess image info


            var onResourceComplete = function(response) {
                $scope.teaminfo = {
                    id       : response.data.id,
                    name     : response.data.name,
                    description  : response.data.description,
                    courses  : response.data.courses.data
                };
                console.log('team info');
                console.log(response);

                //all users
                //possible courses to add
                var urlBaseAll = '/api/course/all';
                $http.get(urlBaseAll)
                    .then(onResourceCompleteAll, onErrorAll);
            };

            var onError = function(reason) {
                $scope.error = "Could not fetch data";
            };

            $http.get(urlBase)
                .then(onResourceComplete, onError);


            onResourceCompleteAll = function(response) {
                $scope.coursemodal.allCourses = response.data.data;

                console.log('all courses');
                console.log($scope.coursemodal.allCourses);

                angular.forEach($scope.teaminfo.courses, function(course){
                    var found = _.find($scope.coursemodal.allCourses,{id: course.id});
                    $scope.coursemodal.allCourses.splice($scope.coursemodal.allCourses.indexOf(found),1);
                });
                //remove courses already added
            };

            onErrorAll = function(reason) {
                $scope.error = "Could not fetch data";
            };


            //change all course select
            $scope.changeAllCourseSelect = function() {
                $scope.allCourseSelect = !$scope.allCourseSelect;
                if(!$scope.allCourseSelect)
                {
                    $scope.selected.courses = [];

                }else{
                    $scope.selected.courses = angular.copy($scope.teaminfo.courses);
                }

            }


            //change all course select
            $scope.changeAllAddCourseSelect = function() {
                //$scope.allCourseSelect = !$scope.allCourseSelect;
                if(!$scope.coursemodal.allCourseSelect)
                {
                    $scope.coursemodal.selected.courses = [];

                }else{
                    $scope.coursemodal.selected.courses = angular.copy($scope.coursemodal.allCourses);
                }
            }



            //parent funct
            $scope.parentUpdate = function(newCourses){
                console.log('modal updated');
                console.log(newCourses);
                angular.forEach(newCourses,function(newCourse){
                    $scope.teaminfo.courses.push(newCourse);
                });

                console.log($scope.teaminfo.courses);
            }


            //bulk action
            $scope.applyCourseBulkAction = function (action)
            {
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //populate team ids
                var course_ids = {course_ids: []};
                angular.forEach($scope.teaminfo.courses, function(course){
                    course_ids.course_ids.push(course.id);
                });

                //call action
                if(action=='delete')
                {
                    //remove courses
                    angular.forEach($scope.selected.courses, function(course){
                        var index = course_ids.course_ids.indexOf(course.id);
                        //remove from current course base
                        course_ids.course_ids.splice(index,1);
                    });
/*
                    Team.deletecourses($scope.teaminfo.id,course_ids)
                        .then(function(data){
                            console.log('success');
                            for(var i=0; i< $scope.selected.courses.length; i++) {
                                // var found = $filter('filter')($scope.teaminfo.courses, {id:$scope.selected.courses[i].id}, true);
                                var found = _.find($scope.teaminfo.courses,{id:$scope.selected.courses[i].id});
                                $scope.teaminfo.courses.splice($scope.teaminfo.courses.indexOf(found),1);
                            }
                            $scope.selected.courses = [];
                            $rootScope.isLoading = 'loaded';
                        }).catch(function(data){
                            console.log('error');
                            $rootScope.isLoading = 'loaded';
                        }).finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });*/
                }else if(action =='add')
                {
                    //remove courses
                    angular.forEach($scope.coursemodal.selected.courses, function(course){
                        course_ids.course_ids.push(course.id);
                    });

                    console.log('adding courses');
                    console.log(course_ids);

                    Team.addcourses($scope.teaminfo.id,course_ids)
                        .then(function(data){
                            console.log('success');
                            //add existing
                            for(var i=0; i< $scope.coursemodal.selected.courses.length; i++) {
                                //add there
                                var index = _.find($scope.coursemodal.allCourses, {id: $scope.coursemodal.selected.courses[i].id});
                                $scope.coursemodal.allCourses.splice($scope.coursemodal.allCourses.indexOf(index), 1);
                            }
                            var returnCourses = angular.copy($scope.coursemodal.selected.courses);
                            $scope.coursemodal.selected.courses = [];
                            //remove from pool

                            //reload
                            $rootScope.isLoading = 'loaded';
                            $rootScope.currentModal.close();
                            $state.go('app.organization.teams.team',{id : $stateParams.id },{reload : true})
                            //$scope.parentUpdate(returnCourses);
                            // $scope.$emit('modalupdate',returnCourses);
                        }).catch(function(data){
                            console.log('error');

                        }).finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });
                }
            }
        }])
    .controller('TeamCreateCtrl', ['$scope','$rootScope','$state','$stateParams','$location','Team','Alert',
        function($scope, $rootScope,$state, $stateParams, $location, Team, Alert){

            //init Alert
            Alert.init($rootScope);
            $scope.teaminfo = {};

            $scope.createteam = function(userForm) {
                if(!userForm.$valid) return;

                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get userinfo
                var teaminfo = $scope.teaminfo;

                var data = {
                    name      : teaminfo.name,
                    description       : teaminfo.description
                };

                Team.createteam(data).
                    then(function(team){
                        alert('New Team Created' + team.id);
                        console.log(team);
                        //go back to users
                        $scope.resources.data.push(team);
                        $state.go('app.organization.teams',{},{reload :true});

                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            };

            //signup and refresh
            $scope.createteamandrefresh = function(userForm) {
                if(!userForm.$valid) return;
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');

                //get teaminfo
                var teaminfo = $scope.teaminfo;

                var data = {
                            name      : teaminfo.name,
                    description       : teaminfo.description
                };

                Team.createteam(data).
                    then(function(team){
                        alert('New Team Created and refreshing' + team.id);
                        console.log(team);
                        //reset form
                        $scope.resources.data.push(team);
                        $scope.teaminfo = {};
                        $scope.userForm.$setPristine();
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });;
            };
        }]);
