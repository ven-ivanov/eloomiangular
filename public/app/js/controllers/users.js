/**
 * Created by Venelin Ivanov on 1/27/15.
 *
 */
angular.module('xenon.controllers')
    .controller('UsersCtrl',['$scope','$rootScope','$state','$http','$location','User','Alert', function($scope, $rootScope,$state, $http, $location, User,Alert) {
        var urlBase = '/api/user?include=unit_name,image';

        var onResourceComplete = function(response) {
            $scope.resources = response.data;
            //convert image info to image
            angular.forEach($scope.resources.data, function(user)
            {
                if(user.image == null || typeof user.image === 'undefined' )
                    user.image  = {path: null, id: null};
            });

            console.log($scope.resources);
        };

        var onError = function(reason) {
            $scope.error = "Could not fetch data";
        };

        $http.get(urlBase)
            .then(onResourceComplete, onError);

        //user bulk selector starts here
        $scope.allUserSelect = false;
         $scope.changeAllUserSelect = function() {
                 $scope.allUserSelect = !$scope.allUserSelect;
                 if(!$scope.allUserSelect)
                 {
                     $scope.selected.users = [];

                 }else{
                     $scope.selected.users = angular.copy($scope.resources.data);
                 }

         }
        //buck select management
        $scope.selected = {
            users: {},
            action: "delete"
        };

        //bulk action

        $scope.applyUserBulkAction = function ()
        {
            //alert($scope.selected.action);
            //alert($scope.selected.users.length);

            //iterate comment.
            $rootScope.isLoading = '';
            jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //call action
                //bulk activate
                if($scope.selected.action=='activate')
                {

                    //bulk deactivate
                    var bulk_user_ids = {ids:[]};
                    angular.forEach($scope.selected.users,function(user){
                        bulk_user_ids.ids.push(user.id);
                    });

                    User.activatebulk(bulk_user_ids)
                        .then(function(response){
                            //reflect data
                            // console.log(response);

                            //set activate
                            angular.forEach(response.data, function(user){
                                var found = _.find($scope.resources.data, {id:user.id});
                                found.active = user.active;
                                //$scope.resources.data[index].active = user.active;
                            });
                        })
                        .catch(function(data){
                           console.log('error');
                            console.log(data);
                        })
                        .finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });

                }else if($scope.selected.action=='deactivate')
                {
                    //bulk deactivate
                    var bulk_user_ids = {ids:[]};
                    angular.forEach($scope.selected.users,function(user){
                        bulk_user_ids.ids.push(user.id);
                    });

                    User.deactivatebulk(bulk_user_ids)
                        .then(function(response){
                            ///reflect data
                           // console.log(response);

                            //set activate
                            angular.forEach(response.data, function(user) {
                                var found = _.find($scope.resources.data, {id: user.id});
                                console.log(found);
                                found.active = user.active;
                                //$scope.resources.data[index].active = user.active;
                            });
                        })
                        .catch(function(data){
                            console.log('error');
                            console.log(data);
                        }).finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });
                }else if($scope.selected.action=='delete')
                {
                    var delete_ids = {ids:[]};
                    angular.forEach($scope.selected.users,function(user){
                            delete_ids.ids.push(user.id);
                    });
                    //
                    User.deleteusers(delete_ids)
                        .then(function(data){
                            $scope.selected.users = [];
                            $state.go("app.organization.users",{},{reload: true});
                        }).catch(function(data){
                            console.log(data);
                        }).finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });
                }

        }

    }])
    .controller('UserSigninController',['$scope','$rootScope','$state','$location','User','Alert',
        function($scope, $rootScope,$state, $location, User, Alert){
            //init
            Alert.init($rootScope);

            $scope.user = {};

            //if already logged
            /* if(User.isLogged()){
             return;
             }*/
            //Login
            $scope.signin = function(userForm){
                //check validity
                if(!userForm.$valid)
                {
                    alert('error');
                    return;
                }

                //sanitize login form data
                var user = $scope.user;
                //se form data
                var data = {
                    email       : user.email,
                    password    : user.password,
                    /* Remember    : user.Remember*/
                };
                //Call Service
                User.signin(data)
                    .then(function(user){
                        //go to homepage
                        alert('welcome:'+user.first_name);
                        $location.url('/forside');
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                        alert($scope.errors.message);
                    });
            };

            //Log out
            $scope.logout = function(){
                User.signout()
                    .then(function(){
                        $state.go('login');
                    });
                $state.go('login');
            }
        }])
    .controller('UserResetPasswordController',['$scope','$rootScope','$stateParams','$location','User','Alert',
        function($scope, $rootScope,$stateParams, $location, User, Alert){
            //init
            Alert.init($rootScope);

            $scope.reset = {};

            //if already logged
            /* if(User.isLogged()){
             return;
             }*/
            //make reset request
            $scope.requestresetpassword = function(userForm){
                //check validity
                if(!userForm.$valid)
                {
                    alert('error');
                    return;
                }
                //se form data
                var data = {
                    email       : $scope.reset.email
                };
                //Call Service
                User.requestresetpassword(data)
                    .then(function(user){
                        //go to homepage
                        alert('pasword request has been made!');
                        $location.url('/login');
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                        alert($scope.errors.message);
                    });
            };

            //reset password
            $scope.resetpassword = function(userForm){
                //check validity
                if(!userForm.$valid)
                {
                    alert('error');
                    return;
                }
                user_id = $stateParams.user;
                reset_code = $stateParams.reset_code;

                //sanitize login form data
                //se form data
                var data = {
                    password_reset_code : reset_code,
                    password            : $scope.reset.password,
                    password_confirmation: $scope.reset.password_confirmation
                };
                //Call Service
                User.resetpassword(user_id,data)
                    .then(function(user){
                        //go to homepage
                        alert('pasword has been reset'+user.first_name);
                        $location.url('/login');
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                        alert($scope.errors.message);
                    });
            };
        }])
    .controller('UserActivateCtrl',['$scope','$rootScope','$location','$stateParams','$state','User','Alert',
        function($scope, $rootScope, $location, $stateParams, $state, User, Alert){
            //scope variables...
            $rootScope.isLoginPage        = false;
            $rootScope.isLightLoginPage   = false;
            $rootScope.isLockscreenPage   = false;
            $rootScope.isMainPage         = true;


            //call API activate user
            $scope.activateuser = function () {
                //retrive user id & activation id
                var user_id = $stateParams.user;
                var activation_code = $stateParams.activation_code;

                //retrieve password and confirm password
                var passwords = $scope.newpasswords;

                //for testing we set one account
                //user_id = 157;
                //activation_code = '$2y$10$bDr5yXYr1WCGDEtdtApnq.9xuPA2k6oPNnqzLmxlneUAVIcw6hvqy';
                var payload = {
                    activation_code  : activation_code,
                    password        : passwords.password1,
                    password_confirmation: passwords.password2
                };

                User.activate(user_id,payload)
                    .then(function(data){
                        //match user information
                        alert('user has been activated');
                        console.log(data);
                    })
                    .catch(function(errors){
                       $scope.errors = errors;
                        alert($scope.errors.message);
                    });
            }

            //reset password
            $scope.resetpassword = function () {

                //retrive user id & activation id
                var user_id = $stateParams.user;
                var activation_code = $stateParams.activation_code;

                //retrieve password and confirm password
                var passwords = $scope.newpasswords;

                //for testing we set one account
                //user_id = 157;
               // activation_code = '$2y$10$bDr5yXYr1WCGDEtdtApnq.9xuPA2k6oPNnqzLmxlneUAVIcw6hvqy';
                var payload = {
                    password_reset_code  : activation_code,
                    password        : passwords.password1,
                    password_confirmation: passwords.password2
                };

                User.resetpassword(user_id,payload)
                    .then(function(data){
                        //match user information
                        alert('password has been reset');
                        console.log(data);
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                        alert($scope.errors.message);
                    });
            }

            //delete user
            //Log out
            var delete_user_id = $scope.delete_user_id;
            $scope.deleteuser = function(){
                User.deleteuser(delete_user_id)
                    .then(function(){
                        //$location.go('/login');
                        window.location.href='#/login';
                    });
                $location.go('/login');
            }
        }])
    .controller('UserUpdateCtrl', ['$scope','$rootScope','$compile','$q','$stateParams','$location','$state','$http','User','Alert',
        function($scope, $rootScope, $compile,$q, $stateParams,$location, $state,$http, User, Alert){
        var urlBase = '/api/user/';
            $scope.activateafter = false;
        urlBase += $stateParams.id;
            //
            urlBase += "?include=image";

        var onResourceComplete = function(response) {
            $scope.image_info = {id: null};
            if(typeof response.data.image !== undefined && response.data.image != null) {
                $scope.image_info = response.data.image;
            }
            $scope.userinfo = response.data;
            if(typeof $scope.image_info.id !== undefined && $scope.image_info.id != null)
                $scope.userinfo.image_id = $scope.image_info.id;

            console.log('retrieved user data');
            console.log(response.data);
        };

        var onError = function(reason) {
            $scope.error = "Could not fetch data";
        };

        $http.get(urlBase)
            .then(onResourceComplete, onError);


        //update user data
        $scope.updateuser = function(userForm) {
            if(!userForm.$valid) return;

            $rootScope.isLoading = '';
            jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
            //get userinfo
            var userinfo = $scope.userinfo;

            var data = {
                first_name      : userinfo.first_name,
                last_name       : userinfo.last_name,
                email           : userinfo.email,
                birthday        : userinfo.birthday,
                gender          : userinfo.gender,
                title           : userinfo.title,
                phone           : userinfo.phone,
                timezone        : userinfo.timezone,
                image_id        : userinfo.image_id
            };

            User.update(userinfo.id,data).
                then(function(user){
                    alert('user updated' + userinfo.id);
                    console.log(user);
                    $scope.userinfo = user;
                    //$location.path("/users");
                    if($scope.activateafter)
                    {
                        User.activatebulk({ids:[userinfo.id]})
                            .then(function(response){
                                //reflect data
                                console.log(response);
                                $state.go("app.organization.users",{},{reload: true});
                            })
                            .catch(function(data){
                                console.log('error');
                                console.log(data);
                            })
                            .finally(function(data){
                                $rootScope.isLoading = 'loaded';
                            });
                    }else{
                        $state.go("app.organization.users",{},{reload: true});
                    }
                })
                .catch(function(errors){
                    $scope.errors = errors;
                }).finally(function(data){
                    $rootScope.isLoading = 'loaded';
                });
        };

            $scope.uploader = {
                controllerFn: function ($flow, $file, $message) {
                    console.log($flow, $file, $message); // Note, you have to JSON.parse message yourself.
                    $file.msg = $message;// Just display message for a convenience
                    console.log('xxx');
                    console.log($message);
                    //retrieve image id
                    var image_info = JSON.parse($message);
                    console.log(image_info);
                    //$scope.userinfo.image_id = image_info.images[0].id;
                    $scope.userinfo.image_id = image_info.data[0].id;
                    console.log($scope.userinfo);
                }
            };

    }])
    .controller('UserImportCtrl', ['$scope','$rootScope','$state','$stateParams','$location','User','Alert',
        function($scope, $rootScope, $state,$stateParams, $location, User, Alert){
            //import users from CSV
            //init
            $scope.import = {};
            //uploader event
            $scope.uploader = {
                controllerFn: function ($flow, $file, $message) {
                    $file.msg = $message;// Just display message for a convenience
                    //retrieve image id
                    var file_info = JSON.parse($message);
                    console.log(file_info);
                    $scope.import.file_id = file_info.data[0].id;
                    console.log($scope.import);
                }
            };
            $scope.userImport = function(userForm){
                if(!userForm.$valid) return;
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');

                var fileinfo = $scope.import;
                fileinfo.type = 'csv';

                User.import(fileinfo).
                    then(function(data){
                        alert('Users imported');
                        console.log(data);
                        //go back to users
                            $state.go("app.organization.users",{},{reload: true});
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            }
        }])
    .controller('UserSignUpCtrl', ['$scope','$rootScope','$state','$stateParams','$location','User','Alert',
        function($scope, $rootScope, $state,$stateParams, $location, User, Alert){

            //init Alert
            Alert.init($rootScope);
            $scope.userinfo = {};
            $scope.activateafter = false;
            //sign up

            //parm ref
            //https://bitbucket.org/RickMarker/eloomi/wiki/Users
            /*
             POST api/user
             Creates a user
             Input
             email: string
             first_name: string
             last_name: string
             birthday: date
             gender: string ("F" or "M")
             title: string
             phone: string
             timezone: string
             image: File (Will only be saved if it is an actual image)
             Output
             */

            $scope.signup = function(userForm) {
                if(!userForm.$valid) return;

                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get userinfo
                var userinfo = $scope.userinfo;
                var data = {
                    first_name      : userinfo.firstname,
                    last_name       : userinfo.lastname,
                    email           : userinfo.email,
                    birthday        : userinfo.birthday,
                    gender          : userinfo.gender,
                    title           : userinfo.title,
                    phone           : userinfo.phone,
                    timezone        : userinfo.timezone
                };

                User.signup(data).
                    then(function(user){
                        alert('New User Created' + user.id);
                        console.log(user);
                        //go back to users
                        if($scope.activateafter)
                        {
                            User.activatebulk({ids:[user.id]})
                                .then(function(response){
                                    //reflect data
                                     console.log(response);
                                    $state.go("app.organization.users",{},{reload: true});
                                })
                                .catch(function(data){
                                    console.log('error');
                                    console.log(data);
                                })
                                .finally(function(data){
                                    $rootScope.isLoading = 'loaded';
                                });
                        }else{
                            $state.go("app.organization.users",{},{reload: true});
                        }
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                });
            };

            //signup and refresh
            $scope.signupandrefresh = function(userForm) {
                if(!userForm.$valid) return;
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get userinfo
                var userinfo = $scope.userinfo;

                var data = {
                    first_name      : userinfo.firstname,
                    last_name       : userinfo.lastname,
                    email           : userinfo.email,
                    birthday        : userinfo.birthday,
                    gender          : userinfo.gender,
                    title           : userinfo.title,
                    phone           : userinfo.phone,
                    timezone        : userinfo.timezone
                };

                User.signup(data).
                    then(function(user){
                        alert('New User Created and refreshed' + user.id);
                        console.log(user);
                        if($scope.activateafter)
                        {
                            User.activatebulk({ids:[user.id]})
                                .then(function(response){
                                    //reflect data
                                    console.log(response);
                                    //reset form
                                    $scope.userinfo = {};
                                    $scope.userForm.$setPristine();
                                })
                                .catch(function(data){
                                    console.log('error');
                                    console.log(data);
                                })
                                .finally(function(data){
                                    $rootScope.isLoading = 'loaded';
                                });
                        }else{
                            //reset form
                            $scope.userinfo = {};
                            $scope.userForm.$setPristine();
                        }

                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            };
            $scope.uploader = {
                controllerFn: function ($flow, $file, $message) {
                    console.log($flow, $file, $message); // Note, you have to JSON.parse message yourself.
                    $file.msg = $message;// Just display message for a convenience
                    console.log('xxx');
                    console.log($message);
                    //retrieve image id
                    var image_info = JSON.parse($message);
                    console.log(image_info);
                    //$scope.userinfo.image_id = image_info.images[0].id;
                    $scope.userinfo.image_id = image_info.data[0].id;
                    console.log($scope.userinfo);
                }
            };
        }]);


