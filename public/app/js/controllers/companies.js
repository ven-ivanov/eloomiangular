/**
 * Created by Venelin Ivanov on 1/27/15.
 *
 */
angular.module('xenon.controllers')
    .controller('CompaniesCtrl',['$scope','$rootScope','$http','$location','$filter','Company','Alert', function($scope, $rootScope, $http, $location,$filter,Company,Alert) {
        var urlBase = '/api/company?include=admin_attributes';

        var onResourceComplete = function(response) {
            $scope.companies = response.data.data;
            console.log('companies');
            console.log(response.data);
            $rootScope.isLoading = 'loaded';
        };

        var onError = function(reason) {
            $scope.error = "Could not fetch data";
        };

        $http.get(urlBase)
            .then(onResourceComplete, onError);

        //company bulk selector starts here
        $scope.allCompanySelect = false;

        //buck select management
        $scope.selected = {
            companys: {},
            action: "delete"
        };


        //company bulk actions
         $scope.changeAllCompanySelect = function() {
                 $scope.allCompanySelect = !$scope.allCompanySelect;
                 if(!$scope.allCompanySelect)
                 {
                     $scope.selected.companys = [];

                 }else{
                     $scope.selected.companys = angular.copy($scope.resources.data);
                 }
         }

        //bulk action
        $scope.applyCompanyBulkAction = function ()
        {

            //populate company ids
            var company_ids = {ids: []};
            angular.forEach($scope.selected.companys, function(company){
               company_ids.ids.push(company.id);
            });

            //call action
            if($scope.selected.action=='delete')
            {
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                Company.multidelete(company_ids)
                    .then(function(data){
                        console.log('success');
                        for(var i=0; i< company_ids.ids.length; i++) {
                            var found = $filter('filter')($scope.resources.data, {id: company_ids.ids[i]}, true);
                            if(found.length) //found
                                $scope.resources.data.splice($scope.resources.data.indexOf(found[0]),1);
                                //$scope.selected.companys.splice(i, 1);
                        }
                        $rootScope.isLoading = 'loaded';
                    }).catch(function(data){
                        console.log('error');
                        $rootScope.isLoading = 'loaded';
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            }
        }
    }])
    .controller('CompanyUpdateCtrl', ['$scope','$rootScope','$stateParams','$location','$state','$http','Company','Alert',
        function($scope, $rootScope, $stateParams, $location, $state,$http,  Company, Alert){
            console.log($stateParams);
            var urlBase = '/api/company/'+$stateParams.id+'?include=admin_attributes,users';
            console.log(urlBase);
            $scope.companyinfo = {};

            //this is for adding users on modal
            $scope.allApps = [];
            //this is for removing users
            $scope.allAppSelect = false;
            //buck select management

            var onResourceComplete = function(response) {
                $scope.companyinfo =  response.data;

                console.log(response);

                //all users
                //possible uses to add
                var urlBaseAllApps = '/api/misc/app_parts';
                $http.get(urlBaseAllApps)
                    .then(onResourceCompleteAll, onErrorAll);
            };

            var onError = function(reason) {
                $scope.error = "Could not fetch data";
            };

            $http.get(urlBase)
                .then(onResourceComplete, onError);


            onResourceCompleteAll = function(response) {
                $scope.allApps = response.data.data;

                console.log('all apps');
                console.log($scope.allApps);
                //remove users already added
            };

            onErrorAll = function(reason) {
                $scope.error = "Could not fetch data";
            };


            //update user data
            $scope.updateCompany = function(userForm) {
                if (!userForm.$valid) return;

                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity', '0.5');
                //get userinfo
                var companyinfo = $scope.companyinfo;

                var app_ids = [];
                var data = {
                    /*name: string*/
                    name : companyinfo.name,
                    /*domain: string*/
                    domain: companyinfo.domain,
                    /*color: string - he hex code for color without "#"*/
                    color: companyinfo.color,
                    /*address (optional): string*/
                    address:companyinfo.admin_attributes.address,
                    image_id:companyinfo.image_id,
                    /*zip (optional): string*/
                    zip: companyinfo.admin_attributes.zip,
                    /*city (optional): string*/
                    city:companyinfo.admin_attributes.city,
                    /*cvr (optional): string*/
                    cvr: companyinfo.admin_attributes.cvr,
                    /*phone (optional): string*/
                    phone: companyinfo.admin_attributes.phone,
                    /*homepage (optional): string*/
                    homepage:companyinfo.admin_attributes.homepage,
                    /*contact_name (optional): string*/
                    contact_name:companyinfo.admin_attributes.contact,
                    /*langauge_id (optional): integer - The id of the default language of the company*/
                    /*app_part_ids (optional): list of ids: The ids which corresponds to the app_parts that this company should have*/

                };
                if (typeof companyinfo.app_parts.data != 'undefined' && companyinfo.app_parts.data != null)
                {
                    angular.forEach(companyinfo.app_parts.data, function(app){
                        app_ids.push(app.id);
                    });
                }
                data.app_part_ids = app_ids;
                delete(data.app_parts);
                console.log(data);

                Company.updatecompany(companyinfo.id,data).
                    then(function(company){
                        alert('company updated' + companyinfo.id);
                        console.log(company);
                        $scope.companyinfo.name = company.name;
                        $scope.companyinfo.description = company.description;
                        $state.go("app.organization.companies");
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            };

            //delete company
            $scope.deleteCompany = function(){
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                Company.deletecompany([$scope.companyinfo.id])
                    .then(function(data){
                        console.log(data);
                        $state.go("app.organization.companys");
                    })
                    .catch(function(errors){
                        console.log(errors);
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            }

            //parent funct
            $scope.parentUpdate = function(newUsers){
                console.log('modal updated');
                console.log(newUsers);
                angular.forEach(newUsers,function(newUser){
                    $scope.companyinfo.users.push(newUser);
                });
                angular.forEach($scope.companyinfo.users, function(user){
                    user.first_name = 'activated';
                });
                console.log($scope.companyinfo.users);
            }

            $scope.uploader = {
                controllerFn: function ($flow, $file, $message) {
                    console.log($flow, $file, $message); // Note, you have to JSON.parse message yourself.
                    $file.msg = $message;// Just display message for a convenience

                    //retrieve image id
                    var image_info = JSON.parse($message);
                    console.log(image_info);
                    //$scope.companyinfo.image_id = image_info.images[0].id;
                    $scope.companyinfo.image_id = image_info.data[0].id;
                    console.log($scope.companyinfo);
                }
            };
            //bulk action
            $scope.applyUserBulkAction = function (action)
            {
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //populate company ids
                var user_ids = {user_ids: []};
                angular.forEach($scope.companyinfo.users, function(user){
                    user_ids.user_ids.push(user.id);
                });

                //call action
                if(action=='delete')
                {
                    //remove users
                    angular.forEach($scope.selected.users, function(user){
                        var index = user_ids.user_ids.indexOf(user.id);
                        //remove from current user base
                        user_ids.user_ids.splice(index,1);
                    });

                    Company.deleteusers($scope.companyinfo.id,user_ids)
                        .then(function(data){
                            console.log('success');
                            for(var i=0; i< $scope.selected.users.length; i++) {
                               // var found = $filter('filter')($scope.companyinfo.users, {id:$scope.selected.users[i].id}, true);
                                var found = _.find($scope.companyinfo.users,{id:$scope.selected.users[i].id});
                                $scope.companyinfo.users.splice($scope.companyinfo.users.indexOf(found),1);
                            }
                            $scope.selected.users = [];
                            $rootScope.isLoading = 'loaded';
                        }).catch(function(data){
                            console.log('error');
                            $rootScope.isLoading = 'loaded';
                        }).finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });
                }else if(action =='add')
                {
                    //remove users
                    angular.forEach($scope.usermodal.selected.users, function(user){
                        user_ids.user_ids.push(user.id);
                    });

                    console.log('adding useres');
                    console.log(user_ids);

                    Company.addusers($scope.companyinfo.id,user_ids)
                        .then(function(data){
                            console.log('success');
                            //add existing
                            for(var i=0; i< $scope.usermodal.selected.users.length; i++) {
                                //add there
                                var index = _.find($scope.usermodal.allUsers, {id: $scope.usermodal.selected.users[i].id});
                                $scope.usermodal.allUsers.splice($scope.usermodal.allUsers.indexOf(index), 1);
                            }
                            var returnUsers = angular.copy($scope.usermodal.selected.users);
                            $scope.usermodal.selected.users = [];
                            //remove from pool

                            //reload
                            $rootScope.isLoading = 'loaded';
                            $rootScope.currentModal.close();
                            $state.go('app.saleforce1.companies.edit',{id : $stateParams.id },{reload : true})
                            //$scope.parentUpdate(returnUsers);
                           // $scope.$emit('modalupdate',returnUsers);
                        }).catch(function(data){
                            console.log('error');

                        }).finally(function(data){
                            $rootScope.isLoading = 'loaded';
                        });
                }
            }

        }])
    .controller('CompanyCreateCtrl', ['$scope','$rootScope','$state','$stateParams','$location','$http','Company','Alert',
        function($scope, $rootScope,$state, $stateParams, $location, $http, Company, Alert){

            //init Alert
            Alert.init($rootScope);
            $scope.companyinfo = {};

            //this is for adding users on modal
            $scope.allApps = [];



            onResourceCompleteAll = function(response) {
                $scope.allApps = response.data.data;

                console.log('all apps');
                console.log($scope.allApps);
                //remove users already added
            };

            onErrorAll = function(reason) {
                $scope.error = "Could not fetch data";
            };
            var urlBaseAllApps = '/api/misc/app_parts';
            $http.get(urlBaseAllApps)
                .then(onResourceCompleteAll, onErrorAll);

            //create company
            $scope.createcompany = function(userForm) {
                if(!userForm.$valid) return;

                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get companyinfo
                var companyinfo = $scope.companyinfo;
                var data = companyinfo;

                Company.createcompany(data).
                    then(function(user){
                        alert('New Company Created' + user.id);
                        console.log(user);
                        //go back to users
                        $state.go("app.saleforce1.companies",{},{reload: true});

                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            };

            //signup and refresh
            $scope.createcompanyandrefresh = function(userForm) {
                if(!userForm.$valid) return;
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get companyinfo

                var companyinfo = $scope.companyinfo;

                var data = companyinfo;

                Company.createcompany(data).
                    then(function(user){
                        alert('New Company Created and refreshed' + user.id);
                        console.log(user);
                        if($scope.activateafter)
                        {
                            User.activatebulk({ids:[user.id]})
                                .then(function(response){
                                    //reflect data
                                    console.log(response);
                                    //reset form
                                    $scope.companyinfo = {};
                                    $scope.userForm.$setPristine();
                                })
                                .catch(function(data){
                                    console.log('error');
                                    console.log(data);
                                })
                                .finally(function(data){
                                    $rootScope.isLoading = 'loaded';
                                });
                        }else{
                            //reset form
                            $scope.companyinfo = {};
                            $scope.userForm.$setPristine();
                        }

                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            };
            $scope.uploader = {
                controllerFn: function ($flow, $file, $message) {
                    console.log($flow, $file, $message); // Note, you have to JSON.parse message yourself.
                    $file.msg = $message;// Just display message for a convenience

                    //retrieve image id
                    var image_info = JSON.parse($message);
                    console.log(image_info);
                    //$scope.companyinfo.image_id = image_info.images[0].id;
                    $scope.companyinfo.image_id = image_info.data[0].id;
                    console.log($scope.companyinfo);
                }
            };
        }]);
