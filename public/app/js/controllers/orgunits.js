/**
 * Created by Venelin Ivanov on 1/27/15.
 *
 */
angular.module('xenon.controllers')
    .controller('UnitsCtrl',['$scope','$rootScope','$http','$location','$filter','OrgUnit','Alert', function($scope, $rootScope, $http, $location,$filter,OrgUnit,Alert) {
//        var urlBase = '/api/organizational-unit';
        var urlBase = '/api/organizational-unit?include=user_count';

        var onResourceComplete = function(response) {
            $scope.resources = response.data;
            $scope.data =$scope.resources.data;
            $scope.savedata =[];
            //$scope.data.push($scope.resources.data);
        };

        var onError = function(reason) {
            $scope.error = "Could not fetch data";
        };

        $http.get(urlBase)
            .then(onResourceComplete, onError);


        //Callbacks
        $scope.treeOptions =  {
            accept: function(sourceNodeScope, destNodesScope, destIndex) {
                    return true;
            },
            dropped : function(event)
            {
                console.log('dropped');
                var updated =  $scope.refactor($scope.data);
                console.log(updated);
                //$scope.bulksaveunits();
                if(updated.length>0)
                {
                    var target = updated[0];
                    var new_data = {parent_id : target.parent_id};
                    OrgUnit.updateunit(target.id, new_data)
                        .then(function(data)
                        {
                            console.log('saved');
                        }).catch(function(data){
                            console.log('error');
                        }).finally(function(data){
                            //console.log('off');
                        });
                }
                return true;
            }
        }


        //bulk save org units
        //update unit data
        $scope.bulksaveunits = function() {

            $rootScope.isLoading = '';
            jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
            //get unitinfo

            OrgUnit.saveunits($scope.data).
                then(function(data){
                    alert('updated' + data.meta)
                    console.log(data.meta);
                })
                .catch(function(errors){
                    $scope.errors = errors;
                }).finally(function(data){
                    $rootScope.isLoading = 'loaded';
                });
        };

        $scope.refactor = function(json){
            var changed = [];
            function recurse(input)
            {
                var parent_id = input[0].parent_id;
                angular.forEach(input,function(cur){
                    if(cur.parent_id != parent_id ) {
                        changed.push({id: cur.id, parent_id: parent_id});
                     //   console.log(cur.id + ":" + parent_id);
                    }

                    cur.parent_id= parent_id;

                    for(var i=0;i<cur.children.length;i++) {
                        if(cur.children[i].parent_id != cur.id) {
                            changed.push({id: cur.children[i].id, parent_id: cur.id});
                            //console.log(cur.children[i].id + ":" + cur.id);
                        }
                        cur.children[i].parent_id=cur.id;
                        recurse([cur.children[i]]);
                    }
                });

            }
            recurse(json);
            return changed;
        }

        $scope.serialize = function(json){
            var items = [];
            function recurse(input)
            {
                var parent_id = input[0].parent_id;
                angular.forEach(input,function(cur){
                    var newObj = {
                        id: cur.id,
                        code: cur.code,
                        name: cur.name,
                        parent_id: parent_id
                    };
                    items.push(newObj);
                    for(var i=0;i<cur.children.length;i++) {
                        cur.children[i].parent_id=cur.id;
                        recurse([cur.children[i]]);
                    }
                });

            }
            recurse(json);
            return items;
        }
        //save units
        $scope.saveUnits = function(units){
            //units is [0] array we're saving it
            //first serialize nested array
            //call save units
            //pre order
            var saveData =$scope.serialize(units);
            $scope.savedata = saveData;
            console.log('serialized data');
            console.log(saveData);
            
            angular.forEach(saveData, function(unit){
                OrgUnit.updateunit(unit.id, { code : unit.code, name: unit.name, parent_id: unit.parent_id})
                    .then(function(data){
                        console.log('saved');
                    }).catch(function(data){
                        console.log('error');
                    }).finally(function(data){

                    });
            });
        }
    }])
    .controller('UnitUpdateCtrl', ['$scope','$rootScope','$stateParams','$location','$state','$http','OrgUnit','Alert',
        function($scope, $rootScope, $stateParams, $location, $state,$http, OrgUnit, Alert){

            $scope.allUnits = [];
            $scope.allUsers = [];
            var urlBase = '/api/user?include=unit_name';


            var onResourceCompleteUsers = function(response) {
                $scope.allUsers = response.data.data;
                console.log('doxia');
                console.log($scope.allUsers);
            };

            var onErrorUsers = function(reason) {
                $scope.error = "Could not fetch data";
            };

            $http.get(urlBase)
                .then(onResourceCompleteUsers, onErrorUsers);

            //Retrieve All Units
            var urlBaseUnit = '/api/organizational-unit?nested=0';

            var onResourceCompleteUnit = function(response) {
                $scope.allUnits = response.data.data;
                console.log($scope.allUnits);
            };

            var onErrorUnit = function(reason) {
                $scope.error = "Could not fetch data";
            };

            $http.get(urlBaseUnit)
                .then(onResourceCompleteUnit, onErrorUnit);

            var urlBase = '/api/organizational-unit/';

            urlBase += $stateParams.id +'?include=leader';

            var onResourceComplete = function(response) {
                $scope.unitinfo = response.data;
                if(typeof $scope.unitinfo.leader=='undefined')
                    $scope.unitinfo.leader={id:null};
            };

            var onError = function(reason) {
                $scope.error = "Could not fetch data";
            };

            $http.get(urlBase)
                .then(onResourceComplete, onError);

            //update unit data
            $scope.updateunit = function(userForm) {
                if(!userForm.$valid) return;

                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get unitinfo
                var unitinfo = $scope.unitinfo;

                var data = {
                    name      : unitinfo.name,
                    code      : unitinfo.code,
                    parent_id  : unitinfo.parent_id,
                    users    : response.data.users.data,
                    courses  : response.data.courses.data
                 /*   leader_id    : unitinfo.leader.id*/

                };
                if(unitinfo.leader != null && typeof unitinfo.leader !='undefined')
                    data.leader_id = unitinfo.leader.id;


                OrgUnit.updateunit(unitinfo.id,data).
                    then(function(unit){
                        alert('unit updated' + unitinfo.id);
                        console.log(unit);
                        $scope.unitinfo = unit;
                        $state.go("app.organization.units");
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });;
            };

            $scope.deleteunit = function() {

                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get unitinfo
                var unit_id = $stateParams.id;
                if(unit_id == null ) return;
                OrgUnit.deleteunit(unit_id).
                    then(function(unit){
                        alert('unit deleted' +unit_id);
                        console.log(unit);
                        $state.go("app.organization.units");
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });;
            };
            //delete unit

        }])
    .controller('UnitCourseUpdateCtrl', ['$scope','$rootScope','$stateParams','$location','$state','$http','OrgUnit','Alert',
            function($scope, $rootScope, $stateParams, $location, $state,$http,  OrgUnit, Alert){
                console.log($stateParams);
                var urlBase = '/api/organizational-unit/'+$stateParams.id+'?include=courses';
    
                console.log(urlBase);
                $scope.OrgUnitinfo = {};
    
                //this is for adding users on modal
                $scope.coursemodal = {
                    allCourseSelect : false,
                    selected: { courses:{}}
                };
                //this is for removing courses
                $scope.allCourseSelect = false;
                //buck select management
                $scope.selected = {
                    courses: {},
                };
    
                //preprocess image info
    
    
                var onResourceComplete = function(response) {
                    $scope.OrgUnitinfo = {
                        id       : response.data.id,
                        name     : response.data.name,
                        description  : response.data.description,
                        courses  : response.data.courses.data
                    };
                    console.log('OrgUnit info');
                    console.log(response);
    
                    //all users
                    //possible courses to add
                    var urlBaseAll = '/api/course/all';
                    $http.get(urlBaseAll)
                        .then(onResourceCompleteAll, onErrorAll);
                };
    
                var onError = function(reason) {
                    $scope.error = "Could not fetch data";
                };
    
                $http.get(urlBase)
                    .then(onResourceComplete, onError);
    
    
                onResourceCompleteAll = function(response) {
                    $scope.coursemodal.allCourses = response.data.data;
    
                    console.log('all courses');
                    console.log($scope.coursemodal.allCourses);
    
                    angular.forEach($scope.OrgUnitinfo.courses, function(course){
                        var found = _.find($scope.coursemodal.allCourses,{id: course.id});
                        $scope.coursemodal.allCourses.splice($scope.coursemodal.allCourses.indexOf(found),1);
                    });
                    //remove courses already added
                };
    
                onErrorAll = function(reason) {
                    $scope.error = "Could not fetch data";
                };
    
    
                //change all course select
                $scope.changeAllCourseSelect = function() {
                    $scope.allCourseSelect = !$scope.allCourseSelect;
                    if(!$scope.allCourseSelect)
                    {
                        $scope.selected.courses = [];
    
                    }else{
                        $scope.selected.courses = angular.copy($scope.OrgUnitinfo.courses);
                    }
    
                }
    
    
                //change all course select
                $scope.changeAllAddCourseSelect = function() {
                    //$scope.allCourseSelect = !$scope.allCourseSelect;
                    if(!$scope.coursemodal.allCourseSelect)
                    {
                        $scope.coursemodal.selected.courses = [];
    
                    }else{
                        $scope.coursemodal.selected.courses = angular.copy($scope.coursemodal.allCourses);
                    }
                }
    
    
    
                //parent funct
                $scope.parentUpdate = function(newCourses){
                    console.log('modal updated');
                    console.log(newCourses);
                    angular.forEach(newCourses,function(newCourse){
                        $scope.OrgUnitinfo.courses.push(newCourse);
                    });
    
                    console.log($scope.OrgUnitinfo.courses);
                }
    
    
                //bulk action
                $scope.applyCourseBulkAction = function (action)
                {
                    $rootScope.isLoading = '';
                    jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                    //populate OrgUnit ids
                    var course_ids = {course_ids: []};
                    angular.forEach($scope.OrgUnitinfo.courses, function(course){
                        course_ids.course_ids.push(course.id);
                    });
    
                    //call action
                    if(action=='delete')
                    {
                        //remove courses
                        angular.forEach($scope.selected.courses, function(course){
                            var index = course_ids.course_ids.indexOf(course.id);
                            //remove from current course base
                            course_ids.course_ids.splice(index,1);
                        });
    /*
                        OrgUnit.deletecourses($scope.OrgUnitinfo.id,course_ids)
                            .then(function(data){
                                console.log('success');
                                for(var i=0; i< $scope.selected.courses.length; i++) {
                                    // var found = $filter('filter')($scope.OrgUnitinfo.courses, {id:$scope.selected.courses[i].id}, true);
                                    var found = _.find($scope.OrgUnitinfo.courses,{id:$scope.selected.courses[i].id});
                                    $scope.OrgUnitinfo.courses.splice($scope.OrgUnitinfo.courses.indexOf(found),1);
                                }
                                $scope.selected.courses = [];
                                $rootScope.isLoading = 'loaded';
                            }).catch(function(data){
                                console.log('error');
                                $rootScope.isLoading = 'loaded';
                            }).finally(function(data){
                                $rootScope.isLoading = 'loaded';
                            });*/
                    }else if(action =='add')
                    {
                        //remove courses
                        angular.forEach($scope.coursemodal.selected.courses, function(course){
                            course_ids.course_ids.push(course.id);
                        });
    
                        console.log('adding courses');
                        console.log(course_ids);
    
                        OrgUnit.addcourses($scope.OrgUnitinfo.id,course_ids)
                            .then(function(data){
                                console.log('success');
                                //add existing
                                for(var i=0; i< $scope.coursemodal.selected.courses.length; i++) {
                                    //add there
                                    var index = _.find($scope.coursemodal.allCourses, {id: $scope.coursemodal.selected.courses[i].id});
                                    $scope.coursemodal.allCourses.splice($scope.coursemodal.allCourses.indexOf(index), 1);
                                }
                                var returnCourses = angular.copy($scope.coursemodal.selected.courses);
                                $scope.coursemodal.selected.courses = [];
                                //remove from pool
    
                                //reload
                                $rootScope.isLoading = 'loaded';
                                $rootScope.currentModal.close();
                                $state.go('app.organization.units.edit',{id : $stateParams.id },{reload : true})
                                //$scope.parentUpdate(returnCourses);
                                // $scope.$emit('modalupdate',returnCourses);
                            }).catch(function(data){
                                console.log('error');
    
                            }).finally(function(data){
                                $rootScope.isLoading = 'loaded';
                            });
                    }
                }
            }])
    .controller('UnitCreateCtrl', ['$scope','$rootScope','$http','$state','$stateParams','$location','OrgUnit','Alert',
        function($scope, $rootScope, $http,$state,$stateParams, $location, OrgUnit, Alert){

            //init Alert
            Alert.init($rootScope);

            $scope.allUnits = [];
            $scope.allUsers = [];
            var urlBase = '/api/user?include=unit_name';


            var onResourceComplete = function(response) {
                $scope.allUsers = response.data.data;
                console.log($scope.allUsers);
            };

            var onError = function(reason) {
                $scope.error = "Could not fetch data";
            };

            $http.get(urlBase)
                .then(onResourceComplete, onError);

            //Retrieve All Units
            var urlBaseUnit = '/api/organizational-unit?nested=0';

            var onResourceCompleteUnit = function(response) {
                $scope.allUnits = response.data.data;
                console.log($scope.allUnits);
            };

            var onErrorUnit = function(reason) {
                $scope.error = "Could not fetch data";
            };

            $http.get(urlBaseUnit)
                .then(onResourceCompleteUnit, onErrorUnit);

            $scope.unitinfo = {};

            $scope.createunit = function(userForm) {
                if(!userForm.$valid) return;

                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');
                //get userinfo
                var unitinfo = $scope.unitinfo;

                var data = {
                    name      : unitinfo.name,
                    code      : unitinfo.code,
                    parent_id    : unitinfo.parent_id,
                    leader_id     : unitinfo.leader_id

                };
                if(data.parent_id=='undefined')
                    data.parent_id = null;
                if(data.leader_id =='undefined')
                    data.leader_id = null;

                OrgUnit.createunit(data).
                    then(function(unit){
                        alert('New unit Created' + unit.id);
                        console.log(unit);
                        //go back to units
                        $state.go("app.organization.units");
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });
            };

            //signup and refresh
            $scope.createunitandrefresh = function(userForm) {
                if(!userForm.$valid) return;
                $rootScope.isLoading = '';
                jQuery('.page-loading-overlay').removeClass('loaded').css('opacity','0.5');

                //get unitinfo
                var unitinfo = $scope.unitinfo;

                var data = {
                    name      : unitinfo.name,
                    code      : unitinfo.code,
                    company_id    : unitinfo.company_id,
                    leader_id     : unitinfo.leader_id

                };

                if(data.parent_id=='undefined')
                    data.parent_id = null;
                if(data.leader_id =='undefined')
                    data.leader_id = null;

                OrgUnit.createunit(data).
                    then(function(unit){
                        alert('New unit Created and refreshing' + unit.id);
                        console.log(unit);
                        //reset form
                        $scope.unitinfo = {};
                        $scope.userForm.$setPristine();
                    })
                    .catch(function(errors){
                        $scope.errors = errors;
                    }).finally(function(data){
                        $rootScope.isLoading = 'loaded';
                    });;
            };
        }]);
