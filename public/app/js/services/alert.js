/**
 * Created by Venelin Ivanov on 1/26/15.
 */
'use strict'
angular.module('xenon.services')
    .service('Alert',function($timeout){

        var self = this;

        //init
        this.init = function(scope){
            self.scope = scope;
            self.clear();
        };

        //success
        this.success = function(text, $scope){
            if(!text) return;
            self.scope.message = {
                type: 'success',
                text: text
            };
            $timeout(function() { self.clear();}, 3000);
        }

        //info
        this.info = function(text ,$scope){
            if(!text) return;

            self.scope.message = {
                type: 'info',
                text: text
            };

            $timeout(function(){self.clear();}, 3000);
        }

        //warning
        this.warning = function(text, $scope){
            if (!text) return;
            self.scope.message = {
                type: 'warning',
                text: text
            };

            $timeout(function(){self.clear();}, 3000);
        }

        //danger
        this.danger = function(text, $scope) {
            if (!text) return;

            self.scope.message = {
                type : 'danger',
                text: text
            };

            $timeout(function(){self.clear();}, 3000);
        }

        //clear
        this.clear = function () {
            self.scope.message  = null;
        }
    });