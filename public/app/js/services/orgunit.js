/**
 * Created by Venelin Ivanov on 1/28/15.
 */
'use strict'
angular.module('xenon.services')
    .service('OrgUnit',['$rootScope','$location','$cookieStore','$http','$q',function($rootScope, $location, $cookieStore, $http, $q) {
        var self = this;
        var currentUnit= null;

        //delete unit
        self.deleteunit = function(unit_id) {
            var deferred = $q.defer();
            //talk with API
            //$http.post('/api/organizational-unit/multi_delete',unit_ids)
            /*$http.post('/api/organizational-unit/multi_delete',unit_ids)*/
            $http.delete('/api/organizational-unit/'+unit_id)
                .success(function(data){
                    deferred.resolve();
                })
                .error(function(data){
                    deferred.reject(data);
                });
            return deferred.promise;
        }
        //create unit
        self.createunit = function(unit){
            var deferred = $q.defer();

            var payload = unit;
            $http.post('/api/organizational-unit', payload )
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    deferred.reject(data);
                })
            return deferred.promise;
        };
        //edit unit
        self.updateunit = function(unit_id,unit){

            var deferred = $q.defer();
            //we also accept null for parent_id
            /*if(unit.parent_id ==null)
            {
                deferred.reject();
                return deferred.promise;
            }*/
            var payload = unit;
            $http({ method: 'PUT', url: '/api/organizational-unit/'+unit_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //save units
        self.saveunits = function(units){
            var deferred = $q.defer();

            var payload ={ units : units};
            $http({ method: 'PUT', url: '/api/organizational-unit', data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    deferred.reject(data);
                })
            return deferred.promise;
        };
        
        //delete users
        self.deleteusers = function(unit_id,user_ids){
            var deferred = $q.defer();

            var payload = user_ids;
            $http({ method: 'PATCH', url: '/api/organizational-unit/'+unit_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //add users
        self.addusers = function(unit_id,user_ids){
            var deferred = $q.defer();

            var payload = user_ids;
            $http({ method: 'PUT', url: '/api/organizational-unit/'+unit_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        };
        
        //add courses
        self.addcourses = function(unit_id,course_ids){
            var deferred = $q.defer();

            var payload = course_ids;
            $http({ method: 'PUT', url: '/api/organizational-unit/'+unit_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        }
        //remove courses
        self.deletecourses = function(course_ids){

        }


        //end of service
    }]);
