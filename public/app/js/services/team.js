/**
 * Created by Venelin Ivanov on 1/28/15.
 */
'use strict'
angular.module('xenon.services')
    .service('Team',['$rootScope','$location','$cookieStore','$http','$q',function($rootScope, $location, $cookieStore, $http, $q) {
        var self = this;
        var currentTeam= null;

        //delete team
        self.deleteteam = function(team_ids) {
            var deferred = $q.defer();
            //talk with API
            $http.post('/api/team/multi_delete',team_ids)
                .success(function(data){
                    deferred.resolve();
                })
                .error(function(data){
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        //delete users
        self.deleteusers = function(team_id,user_ids){
            var deferred = $q.defer();

            var payload = user_ids;
            $http({ method: 'PATCH', url: '/api/team/'+team_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //add users
        self.addusers = function(team_id,user_ids){
            var deferred = $q.defer();

            var payload = user_ids;
            $http({ method: 'PUT', url: '/api/team/'+team_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //create team
        self.createteam = function(team){
            var deferred = $q.defer();
            /* var payload = {
             first_name      : user.first_name,
             last_name       : user.last_name,
             email           : user.email,
             title           : user.title,

             };*/
            var payload = team;
            $http.post('/api/team', payload )
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    deferred.reject(data);
                })
            return deferred.promise;
        };
        //edit team
        self.updateteam = function(team_id,team){
            var deferred = $q.defer();

            var payload = team;
            $http({ method: 'PATCH', url: '/api/team/'+team_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //add courses
        self.addcourses = function(team_id,course_ids){
            var deferred = $q.defer();

            var payload = course_ids;
            $http({ method: 'PUT', url: '/api/team/'+team_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        }
        //remove courses
        self.deletecourses = function(course_ids){

        }

        //delete team in bulk
        self.multidelete = function(team_ids) {
            var deferred = $q.defer();
            var payload = team_ids;
            var baseURL = '/api/team';

            $http({ method: 'DELETE', url: baseURL, data: payload})
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        };
        //end of service
    }]);
