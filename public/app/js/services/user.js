/**
 * Created by Venelin Ivanov on 1/26/15.
 */
'use strict'
angular.module('xenon.services')
    .service('User',['$rootScope','$location','$cookieStore','$http','$q',function($rootScope, $location, $cookieStore, $http, $q) {
        var self = this;
        var currentUser= null;

        //change user
        function changeUser(user) {
            _.extend(currentUser,user);
        };

        //auth user
        self.auth = function(user){
            $cookieStore.put('user',user);
        };

        //is logged
        self.isLogged = function() {
            return !angular.isUndefined($cookieStore.get('user'));
        };

        //clean
        self.clean = function () {
            $cookieStore.remove('user');
        };

        //get by name
        self.get = function(name) {
            var user = $cookieStore.get('user');
            return (!_.isUndefined(user)) ? user[name] : null;
        };

        //myself
        self.me = function () {
            var deferred = $q.defer();

            $http.get('/api/company/me')
                .succes(function(data){
                       $cookieStore.put('self',data);
                })
                .error(function(data){
                    deferred.reject(data.message);
                });
            return deferred.promise;
        };

        //sign in
        self.signin = function(user) {
            var deferred = $q.defer();
            $http.post('/api/sessions',user)
                .success(function(data){
                    //assumes on success data has user information from API
                    self.auth(data);
                    /*self.me();*/
                    //deferred.resolve(data.user, data.redirect);
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    //meta stores error information
                    deferred.reject(data.meta);
                });
            return deferred.promise;
        };
        //sign out
        self.signout = function() {
            var deferred = $q.defer();
            //talk with API
            $http.delete('/api/sessions')
                .success(function(){
                    self.clean();
                    deferred.resolve();
                });
                return deferred.promise;
        }
        //logged in
        self.loggedin = function() {
            var deferred = $q.defer();
            $http.post('/api/loggedin',{})
                .success(function(data){
                    deferred.resolve(data.user);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data.message);
                });
            return deferred.promise;
        };

        //sign up
        self.signup = function(user){
           var deferred = $q.defer();
            /* var payload = {
             first_name      : user.first_name,
             last_name       : user.last_name,
             email           : user.email,
             title           : user.title,

             };*/
            var payload = user;
            $http.post('/api/user', payload )
                .success(function(data, status, headers, config) {
                    // self.auth(data.user);
                    // deferred.resolve(data.user);
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //sign out

        //edit user
        self.update = function(user_id,user){
            var deferred = $q.defer();

            var payload = user;
            $http({ method: 'PATCH', url: '/api/user/'+user_id, data: payload})
            /*$http.patch('api/user/'+user_id, payload )*/
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        };
        //deactivate users
        self.deactivatebulk= function(user_ids) {
            var deferred = $q.defer();

            var payload = user_ids;
            var baseURL = '/api/user/deactivate';
            $http.post(baseURL, payload)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    self.clean();
                    deferred.reject(data);
                });
            return deferred.promise;
        };

        //deactivate user
        self.deactivate = function(user_id,data) {
            var deferred = $q.defer();
            var payload = data;
            var baseURL = '/api/user/' + user_id + '/deactivate';
            $http.post(baseURL, payload)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    self.clean();
                    deferred.reject(data);
                });
            return deferred.promise;
        };
        //activate user in bulk
        self.activatebulk = function(user_ids) {
            var deferred = $q.defer();

            var payload = user_ids;
            var baseURL = '/api/user/request_activation';
            $http.post(baseURL, payload)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    self.clean();
                    deferred.reject(data);
                });
            return deferred.promise;
        };

        //activate user
        self.activate = function(user_id,data) {
            var deferred = $q.defer();
            var payload = data;
            var baseURL = '/api/user/' + user_id + '/activate';
            $http.post(baseURL, payload)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    self.clean();
                    deferred.reject(data);
                });
            return deferred.promise;
        };

        //reset password
        self.resetpassword = function(user_id,data) {
            var deferred = $q.defer();
            var payload = data;
            var baseURL = '/api/user/' + user_id + '/reset_password';
            $http.post(baseURL, payload)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        };

        //request reset password
        self.requestresetpassword = function(data) {
            var deferred = $q.defer();
            var baseURL = '/api/user/forgotpassword';
            var payload = data;
            $http.post(baseURL,data)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        };

        //delete user
        self.deleteuser = function(user_id) {
            var deferred = $q.defer();
            //talk with API
            $http.delete('/api/user/'+user_id)
                .success(function(){
                    self.clean();
                    deferred.resolve();
                })
                .error(function(data){
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        //delete multiple user
        self.deleteusers = function(user_ids) {
            var deferred = $q.defer();
            //talk with API
            var payload = user_ids;
            $http.post('/api/user/multi_delete',payload)
                .success(function(){
                    deferred.resolve();
                })
                .error(function(data){
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        //import
        self.import = function(fileinfo){
            var deferred = $q.defer();
            var payload = fileinfo;
            //$http.post('api/user/import', payload )
            $http({ method: 'POST', url: 'api/user/import', data: payload, headers: {'Content-Type': 'application/json'}})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //end of service
    }]);
