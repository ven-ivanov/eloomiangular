/**
 * Created by Venelin Ivanov on 1/28/15.
 */
'use strict'
angular.module('xenon.services')
    .service('Company',['$rootScope','$location','$cookieStore','$http','$q',function($rootScope, $location, $cookieStore, $http, $q) {
        var self = this;
        var currentCompany= null;

        //delete company
        self.deletecompany = function(company_ids) {
            var deferred = $q.defer();
            //talk with API
            $http.post('/api/company/multi_delete',company_ids)
                .success(function(data){
                    deferred.resolve();
                })
                .error(function(data){
                    deferred.reject(data);
                });
            return deferred.promise;
        }

        //delete users
        self.deleteusers = function(company_id,user_ids){
            var deferred = $q.defer();

            var payload = user_ids;
            $http({ method: 'PATCH', url: '/api/company/'+company_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //add users
        self.addusers = function(company_id,user_ids){
            var deferred = $q.defer();

            var payload = user_ids;
            $http({ method: 'PUT', url: '/api/company/'+company_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    self.clean();
                    deferred.reject(data);
                })
            return deferred.promise;
        };

        //create company
        self.createcompany = function(company){
            var deferred = $q.defer();

            var payload = company;
            $http.post('/api/company', payload )
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    deferred.reject(data);
                })
            return deferred.promise;
        };
        //edit company
        self.updatecompany = function(company_id,company){
            var deferred = $q.defer();

            var payload = company;
            $http({ method: 'PATCH', url: '/api/company/'+company_id, data: payload})
                .success(function(data, status, headers, config) {
                    deferred.resolve(data);
                })
                .error(function(data){
                    deferred.reject(data);
                })
            return deferred.promise;
        };


        //delete company in bulk
        self.multidelete = function(company_ids) {
            var deferred = $q.defer();
            var payload = company_ids;
            var baseURL = '/api/company/multi_delete';
            $http.post(baseURL, payload)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        };
        //end of service
    }]);
