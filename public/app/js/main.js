jQuery('#top-notification-close').click( function () {
	notificationWrapper.removeClass('active');
	
	setTimeout(function() {
		
		notificationWrapper.addClass('active');
		notificationDiv.html(notificationIcon+notificationText);
		notificationBtn.attr('href',btnLink);
		notificationBtn.text(btnText);
		
	}, 8000);
	
});

$(function () {
	function dashboardAutochangeGauge(){
		var activeGage = jQuery('#results li.active');
		
		if( activeGage.is(':last-child') ){
			var nextGage = jQuery('#results li:first-child');
		}else{
			var nextGage = activeGage.next();
		}
		
		
		if( !nextGage.hasClass('active') ){
		
		  	var gageTitle = nextGage.attr('data-title');
		  	var gageValue = nextGage.attr('data-val');
		  	var gageMax = nextGage.attr('data-max');
		  	jQuery('div.marker').remove();
		  	
		  	activeGage.removeClass('active');
		  	nextGage.addClass('active');
		  	nextGage.append('<div class="marker"></div>')
		  		
		  	var percentage = 100*gageValue/gageMax;
		  	var Color = '#000000';
		  	var	tickMark = 25*gageMax/100;
		  		
		  	if( percentage < 10 ){
		  		Color = '#e60000';
		  	}else if( percentage < 20 ){
		  		Color = '#e64100';
		  	}else if( percentage < 30 ){
		  		Color = '#ec6800';
		  	}else if( percentage < 40 ){
		  		Color = '#ecae00';
		  	}else if( percentage < 50 ){
		  		Color = '#ecf300';
		  	}else if( percentage < 60 ){
		  		Color = '#c6ff08';
		  	}else if( percentage < 70 ){
		  		Color = '#9dff4a';
		  	}else if( percentage < 80 ){
		  		Color = '#6be900';
		  	}else if( percentage < 90 ){
		  		Color = '#59c300';
		  	}else {
		  		Color = '#2dad00';
		  	}
		  	jQuery('#big-gauge').attr('class','').empty();	
		  	jQuery('#big-gauge-info').empty();
		  	jQuery('#big-gauge-info').html('<h4>'+gageTitle+'</h4><h2>'+gageValue+'</h2>');	
		  	
		  	$('.BigGaugeDashboad').dxCircularGauge({
		  		scale: {
		  			startValue: 0,
		  			endValue: gageMax,
		  			majorTick: {
		  				tickInterval: tickMark,
		  	          color: '#41444b',
		  	          length: 10
		  			},
		  			useRangeColors: true
		  		},	
		  		rangeContainer: {
		  	        width: 10,       
		  			ranges: [
		  	          { startValue: 0, endValue: gageMax, color: '#dcdcdc' },
		  			]
		  		},
		  		value: gageValue,
		  		valueIndicator: {
		  			type: 'rangebar',
		  			color: Color,
		  	        offset: -10
		  		}
		  	});
		  	
		  }
	}
	setInterval(dashboardAutochangeGauge, 8000);
	
	//COURSE - PROGRESS BAR
	var totalSteps = parseInt( jQuery('#eloomi-progress-wrapper').attr('data-steps') );
	var pbFill = jQuery('#eloomi-progress');
	var pbDistance = 100 / ( totalSteps-1 );
	var pbPosition = 0;
	
	for( var i = 0; i < totalSteps; i++ ){
		jQuery('#eloomi-progress-steps').append('<a id="pB-step-' + i + '" href="modul-' + i + '" style="left:' + pbPosition + '%;"></a>');
		pbPosition += pbDistance;
	}
	var zadsada = 0;
	pbPosition = 0;

	function demoSteps(){
		
//		for( var i = 0; i < totalSteps; i++ ){
//			if( i == zadsada ){
//				pbFill.css('width', pbPosition + '%');
//				pbPosition += pbDistance;
//			}
//		}
//		
//		zadsada++;
//		console.log(zadsada);
		
		jQuery('#modul-nav').addClass('btn-available');
		jQuery('#modul-nav i').removeAttr('class').addClass('fa-unlock');
		
	}
	setInterval(demoSteps, 6000);
	
	jQuery('#modul-nav').click( function () {
		if( jQuery(this).hasClass('btn-available') ){
			pbPosition += pbDistance;
			pbFill.css('width', pbPosition + '%');
			jQuery(this).removeClass('btn-available');
			jQuery('#modul-nav i').removeAttr('class').addClass('fa-lock');
		}
	});
});

$(document).ready(function(){
	$('body').on('click', '#results li.gauge-item', function(){
		var nextGage = jQuery(this);
	
		if( !nextGage.hasClass('active') ){
		  	var activeGage = jQuery('li.gauge-item.active');
		  	var gageTitle = nextGage.attr('data-title');
		  	var gageValue = nextGage.attr('data-val');
		  	var gageMax = nextGage.attr('data-max');
		  	jQuery('div.marker').remove();
		  	
		  	activeGage.removeClass('active');
		  	nextGage.addClass('active');
		  	nextGage.append('<div class="marker"></div>')
		  		
		  	var percentage = 100*gageValue/gageMax;
		  	var Color = '#000000';
		  	var	tickMark = 25*gageMax/100;
		  		
		  	if( percentage < 10 ){
		  		Color = '#e60000';
		  	}else if( percentage < 20 ){
		  		Color = '#e64100';
		  	}else if( percentage < 30 ){
		  		Color = '#ec6800';
		  	}else if( percentage < 40 ){
		  		Color = '#ecae00';
		  	}else if( percentage < 50 ){
		  		Color = '#ecf300';
		  	}else if( percentage < 60 ){
		  		Color = '#c6ff08';
		  	}else if( percentage < 70 ){
		  		Color = '#9dff4a';
		  	}else if( percentage < 80 ){
		  		Color = '#6be900';
		  	}else if( percentage < 90 ){
		  		Color = '#59c300';
		  	}else {
		  		Color = '#2dad00';
		  	}
		  	jQuery('#big-gauge').attr('class','').empty();	
		  	jQuery('#big-gauge-info').empty();
		  	jQuery('#big-gauge-info').html('<h4>'+gageTitle+'</h4><h2>'+gageValue+'</h2>');
		  	
		  	jQuery('.BigGaugeDashboad').dxCircularGauge({
		  		scale: {
		  			startValue: 0,
		  			endValue: gageMax,
		  			majorTick: {
		  				tickInterval: tickMark,
		  	          color: '#41444b',
		  	          length: 10
		  			},
		  			useRangeColors: true
		  		},	
		  		rangeContainer: {
		  	        width: 10,       
		  			ranges: [
		  	          { startValue: 0, endValue: gageMax, color: '#dcdcdc' }
		  			]
		  		},
		  		value: gageValue,
		  		valueIndicator: {
		  			type: 'rangebar',
		  			color: Color,
		  	        offset: -10
		  		}
		  	});
		  	
		  }
		
	});
});


