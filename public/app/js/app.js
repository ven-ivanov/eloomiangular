'use strict';

var app = angular.module('xenon-app', [
	'ngCookies',
	'ngAnimate',
   /* 'ngRoute',*/
	'ui.router',
	'ui.bootstrap',
	'oc.lazyLoad',
	'xenon.controllers',
	'xenon.directives',
	'xenon.factory',
	'xenon.services',
	// Added in v1.3
	'FBAngular',
	// Added by PurpleCow [27-01-2015]
	'flow',
    // Added by Ven for breadcrumb
    'ncy-angular-breadcrumb',
    // auto active menu
    'autoActive',
    //bulk select
    'checklist-model',
	//ui tree
	'ui.tree'
]);

app.run(function(){
	// Page Loading Overlay
	public_vars.$pageLoadingOverlay = jQuery('.page-loading-overlay');

	jQuery(window).load(function()
	{
		public_vars.$pageLoadingOverlay.addClass('loaded');
	})
});

app.config(function($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
        template: 'bootstrap2'
    });
});
app.config(function($rootScopeProvider) {

	$rootScopeProvider.digestTtl(100);
});
app.config(function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, ASSETS){

	$urlRouterProvider.otherwise('/app/forside');

	$stateProvider.
		// Main Layout Structure
		state('app', {
			abstract: true,
			url: '/app',
			templateUrl: appHelper.templatePath('layout/app-body'),
			controller: function($rootScope, $scope){
				$rootScope.isLoginPage        = false;
				$rootScope.isLightLoginPage   = false;
				$rootScope.isLockscreenPage   = false;
				$rootScope.isMainPage         = true;

				$scope.users = [
		            {name : "Du mangler at fuldføre kurset: \"Gør din strategi flexibel\"", email : "anup.vasudeva2009@gmail.com", desc : "Description about Anup Vasudeva"},
		            {name : "Amit Vasudeva", email : "amit.vasudeva2009@gmail.com", desc : "Description about Amit Vasudeva"},
		            {name : "Vijay Kumar", email : "vijay.kumar@gmail.com", desc : "Description about Vijay Kumar"}
		        ];
			}
		}).

		// Dashboards
		state('app.forside', {
			url: '/forside',
			templateUrl: appHelper.templatePath('main/forside'),
            ncyBreadcrumb: {
                label: 'Forside',
                skip: true
            },
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
						ASSETS.extra.toastr,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxCharts,
					]);
				}
			}
		}).
		state('app.organization', {
			url: '/organization',
			templateUrl: appHelper.templatePath('main/organization'),
            ncyBreadcrumb: {
                label: 'Organisation'
            },
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.charts.dxGlobalize,
					]);
				},
				dxCharts: function($ocLazyLoad){
					return $ocLazyLoad.load([
						//ASSETS.charts.,
						// ASSETS.uikit.base,
						ASSETS.uikit.nestable,
					]);
				}
			}
		}).
			state('app.organization.units', {
				url: '/units',
				templateUrl: appHelper.templatePath('organization/units'),
            	abstract: false,
            	ncyBreadcrumb: {
            	    label: 'Afdelinger'
            	},
				controller: 'OrganizationCtrl'
			}).
				state('app.organization.units.edit', {
					ncyBreadcrumb: {
					    label: 'Edit {{unitinfo.name}} '
					},
					controller: 'UnitUpdateCtrl',
					url: '/edit-:id',
					templateUrl: appHelper.templatePath('organization/units/edit'),
					resolve: {
						jqui: function($ocLazyLoad){
							return $ocLazyLoad.load({
								files: ASSETS.core.jQueryUI
							});
						},
						selectboxit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.selectboxit,
							]);
						}
					}
				}).
				state('app.organization.units.new', {
					ncyBreadcrumb: {
					    label: 'Opret Afdeling'
					},
					url: '/new',
					templateUrl: appHelper.templatePath('organization/units/new'),
					resolve: {
						jqui: function($ocLazyLoad){
							return $ocLazyLoad.load({
								files: ASSETS.core.jQueryUI
							});
						},
						selectboxit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.selectboxit,
							]);
						},
						fwDependencies: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.datepicker,
							]);
						}
					}
				}).
			state('app.organization.teams', {
				url: '/teams',
				templateUrl: appHelper.templatePath('organization/teams'),
                ncyBreadcrumb: {
                    label: 'Teams'
                },
				controller: 'TeamsCtrl',
				resolve: {
					deps: function($ocLazyLoad){
						return $ocLazyLoad.load([
							ASSETS.tables.datatables,
						])
					},
				}
			}).
				state('app.organization.teams.team', {
					ncyBreadcrumb: {
					    label: '{{teaminfo.name}}'
					},
					url: '/team-:id',
					templateUrl: appHelper.templatePath('organization/teams/team'),
					controller: 'TeamUpdateCtrl'
				}).
				state('app.organization.teams.new', {
					ncyBreadcrumb: {
					    label: 'Opret Team'
					},
					url: '/new',
					templateUrl: appHelper.templatePath('organization/teams/new')
				}).
			state('app.organization.users', {
				url: '/users',
                templateUrl: appHelper.templatePath('organization/users'),
                controller: 'UsersCtrl',
                ncyBreadcrumb: {
                    label: 'Users'
                },
                resolve: {
					jqui: function($ocLazyLoad){
						return $ocLazyLoad.load({
							files: ASSETS.core.jQueryUI
						});
					},
					selectboxit: function($ocLazyLoad){
						return $ocLazyLoad.load([
							ASSETS.forms.selectboxit,
						]);
					},
					fwDependencies: function($ocLazyLoad){
						return $ocLazyLoad.load([
							ASSETS.forms.datepicker,
						]);
					}
				}
			}).
				state('app.organization.users.edit', {
					ncyBreadcrumb: {
					    label: 'Edit {{userinfo.first_name}} {{userinfo.last_name}}'
					},
					url: '/user-:id',
					templateUrl: appHelper.templatePath('organization/users/edit'),
                    controller: "UserUpdateCtrl"
				}).
				state('app.organization.users.new', {
					ncyBreadcrumb: {
					    label: 'Opret Bruger'
					},
					url: '/new',
					templateUrl: appHelper.templatePath('organization/users/new'),
                    controller: "UserSignUpCtrl"
				}).
				state('app.organization.users.import', {
					ncyBreadcrumb: {
					    label: 'Importer .CSV'
					},
					url: '/import',
					templateUrl: appHelper.templatePath('organization/users/import')
				}).
			state('app.organization.coaches', {
				ncyBreadcrumb: {
				    label: 'Coaches'
				},
				url: '/coaches',
				templateUrl: appHelper.templatePath('organization/coaches'),
				resolve: {
					deps: function($ocLazyLoad){
						return $ocLazyLoad.load([
							ASSETS.tables.datatables,
						]);
					}
				}
			}).
				state('app.organization.coaches.edit', {
					ncyBreadcrumb: {
					    label: 'Coach Name'
					},
					url: '/edit-:id',
					templateUrl: appHelper.templatePath('organization/coaches/edit')
				}).
				state('app.organization.coaches.new', {
					ncyBreadcrumb: {
					    label: 'Opret Coach'
					},
					url: '/new',
					templateUrl: appHelper.templatePath('organization/coaches/new'),
					resolve: {
						datepicker: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.select2,
							]);
						}
					}
				}).
		state('app.learning', {
			ncyBreadcrumb: {
			    label: 'Læring'
			},
			url: '/learning',
			templateUrl: appHelper.templatePath('main/learning')
		}).
			state('app.learning.courses', {
				ncyBreadcrumb: {
				    label: 'Kursus'
				},
				url: '/courses',
				templateUrl: appHelper.templatePath('learning/courses')
			}).
				state('app.learning.courses.view', {
					ncyBreadcrumb: {
					    label: 'Course Name'
					},
					url: '/view-:id',
					templateUrl: appHelper.templatePath('learning/view')
				}).
					state('app.learning.courses.view.module', {
						ncyBreadcrumb: {
						    label: 'Module Name'
						},
						url: '/module',
						templateUrl: appHelper.templatePath('learning/module')
					}).
			state('app.learning.courseslist', {
				ncyBreadcrumb: {
				    label: 'Kursus'
				},
				url: '/courseslist',
				templateUrl: appHelper.templatePath('learning/courseslist'),
				resolve: {
					jqui: function($ocLazyLoad){
						return $ocLazyLoad.load({
							files: ASSETS.core.jQueryUI
						});
					},
					deps: function($ocLazyLoad){
						return $ocLazyLoad.load([
							ASSETS.tables.datatables,
						]);
					}
				}
			}).
				state('app.learning.courseslist.new', {
					ncyBreadcrumb: {
					    label: 'Opret Kursus'
					},
					url: '/new',
					templateUrl: appHelper.templatePath('learning/course/new'),
					resolve: {
						jqui: function($ocLazyLoad){
							return $ocLazyLoad.load({
								files: ASSETS.core.jQueryUI
							});
						},
						selectboxit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.selectboxit,
							]);
						}
					}
				}).
				state('app.learning.courseslist.edit', {
					ncyBreadcrumb: {
					    label: 'Courses Name'
					},
					url: '/edit-:id',
					templateUrl: appHelper.templatePath('learning/course/edit'),
					controller: 'UIModalsCtrl',
//					controller: 'CourseListEditCtrl',
					resolve: {
						selectboxit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.selectboxit,
							]);
						}
					}
				}).
				state('app.learning.courseslist.edit.newmodule', {
					ncyBreadcrumb: {
					    label: 'Opret Modul'
					},
					url: '/newmodule',
					templateUrl: appHelper.templatePath('learning/course/newmodule')
				}).
				state('app.learning.courseslist.edit.newmodule.newstd', {
					ncyBreadcrumb: {
					    label: 'Indhold'
					},
					url: '/new-std',
					templateUrl: appHelper.templatePath('learning/course/modules/new-standard'),
					resolve: {
						bootstrap: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.core.bootstrap,
							]);
						},
						bootstrapWysihtml5: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.bootstrapWysihtml5,
							]);
						},
						uikit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.uikit.base,
								ASSETS.uikit.codemirror,
								ASSETS.uikit.marked,
							]);
						},
						uikitHtmlEditor: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.uikit.htmleditor,
							]);
						},
					}
				}).
				state('app.learning.courseslist.editstd', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-std:id',
					templateUrl: appHelper.templatePath('learning/course/modules/edit-standard')
				}).
				state('app.learning.courseslist.edit.newmodule.newpp', {
					ncyBreadcrumb: {
					    label: 'Powerpoint'
					},
					url: '/new-pp',
					templateUrl: appHelper.templatePath('learning/course/modules/new-pp')
				}).
				state('app.learning.courseslist.editpp', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-pp:id',
					templateUrl: appHelper.templatePath('learning/course/modules/edit-pp')
				}).
				state('app.learning.courseslist.edit.newmodule.newscorm', {
					ncyBreadcrumb: {
					    label: 'SCORM'
					},
					url: '/new-scorm',
					templateUrl: appHelper.templatePath('learning/course/modules/new-scorm')
				}).
				state('app.learning.courseslist.editscorm', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-scorm:id',
					templateUrl: appHelper.templatePath('learning/course/modules/edit-scorm')
				}).
				state('app.learning.courseslist.edit.newmodule.newpoll', {
					ncyBreadcrumb: {
					    label: 'Survey'
					},
					url: '/new-poll',
					templateUrl: appHelper.templatePath('learning/course/modules/new-poll'),
					controller: 'UIModalsCtrl'
				}).
				state('app.learning.courseslist.editpoll', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-poll:id',
					templateUrl: appHelper.templatePath('learning/course/modules/edit-poll'),
					controller: 'UIModalsCtrl'
				}).
				state('app.learning.courseslist.edit.newmodule.newvideo', {
					ncyBreadcrumb: {
					    label: 'Video'
					},
					url: '/new-video',
					templateUrl: appHelper.templatePath('learning/course/modules/new-video')
				}).
				state('app.learning.courseslist.editvideo', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-video:id',
					templateUrl: appHelper.templatePath('learning/course/modules/edit-video')
				}).
			state('app.learning.shop', {
				ncyBreadcrumb: {
				    label: 'Kursus Shop'
				},
				url: '/shop',
				templateUrl: appHelper.templatePath('learning/shop'),
				controller: 'UIModalsCtrl'
			}).
			state('app.learning.badges', {
				ncyBreadcrumb: {
				    label: 'Badges'
				},
				url: '/badges',
				templateUrl: appHelper.templatePath('learning/badges')
			}).
			state('app.learning.certificates', {
				ncyBreadcrumb: {
				    label: 'Certifikater'
				},
				url: '/certificates',
				templateUrl: appHelper.templatePath('learning/certificates')
			}).
		state('app.goalscoaching', {
			ncyBreadcrumb: {
			    label: 'Mål & Coaching'
			},
			url: '/goalscoaching',
			templateUrl: appHelper.templatePath('main/goalscoaching')
		}).
		state('app.raport', {
			ncyBreadcrumb: {
			    label: 'Rapporter'
			},
			url: '/raport',
			templateUrl: appHelper.templatePath('main/raport')
		}).
		state('app.profile', {
			ncyBreadcrumb: {
			    label: 'Min Profil'
			},
			url: '/profile',
			templateUrl: appHelper.templatePath('main/profile')
		}).
		state('app.settings', {
			ncyBreadcrumb: {
			    label: 'Indstillinger'
			},
			url: '/settings',
			templateUrl: appHelper.templatePath('main/settings')
		}).
		state('app.system', {
			ncyBreadcrumb: {
			    label: 'Systemet er udviklet af eloomi'
			},
			url: '/system',
			templateUrl: appHelper.templatePath('main/system')
		}).
		state('app.terms', {
			ncyBreadcrumb: {
			    label: 'Vilkår & Betlingelser'
			},
			url: '/terms',
			templateUrl: appHelper.templatePath('main/terms')
		}).
		state('app.policy', {
			ncyBreadcrumb: {
			    label: 'Privacy Policy'
			},
			url: '/policy',
			templateUrl: appHelper.templatePath('main/policy')
		}).
		state('app.help', {
			ncyBreadcrumb: {
			    label: 'Hælp'
			},
			url: '/help',
			templateUrl: appHelper.templatePath('main/help')
		}).
		
		//SaleForce1
		state('app.saleforce1', {
			ncyBreadcrumb: {
			    label: 'SaleForce1'
			},
			url: '/saleforce1',
			templateUrl: appHelper.templatePath('main/saleforce1')
		}).
			state('app.saleforce1.companies', {
				ncyBreadcrumb: {
				    label: 'Firmaer'
				},
				url: '/companies',
				templateUrl: appHelper.templatePath('saleforce1/companies')
			}).
				state('app.saleforce1.companies.new', {
					ncyBreadcrumb: {
					    label: 'Ny Firma'
					},
					url: '/new',
					templateUrl: appHelper.templatePath('saleforce1/company-new'),
					resolve: {
						jqui: function($ocLazyLoad){
							return $ocLazyLoad.load({
								files: ASSETS.core.jQueryUI
							});
						},
						selectboxit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.selectboxit,
							]);
						},
						colorpicker: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.colorpicker,
							]);
						}
					}
				}).
				state('app.saleforce1.companies.edit', {
					ncyBreadcrumb: {
					    label: '{{companyinfo.name}}'
					},
					url: '/edit-:id',
					templateUrl: appHelper.templatePath('saleforce1/company-edit'),
					controller:"CompanyUpdateCtrl",
					resolve: {
						jqui: function($ocLazyLoad){
							return $ocLazyLoad.load({
								files: ASSETS.core.jQueryUI
							});
						},
						selectboxit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.selectboxit,
							]);
						},
						colorpicker: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.colorpicker,
							]);
						}
					}
				}).
			state('app.saleforce1.courseslist', {
				ncyBreadcrumb: {
				    label: 'SF1 Kurser'
				},
				url: '/courseslist',
				templateUrl: appHelper.templatePath('saleforce1/courseslist'),
				resolve: {
					jqui: function($ocLazyLoad){
						return $ocLazyLoad.load({
							files: ASSETS.core.jQueryUI
						});
					},
					deps: function($ocLazyLoad){
						return $ocLazyLoad.load([
							ASSETS.tables.datatables,
						]);
					}
				}
			}).
				state('app.saleforce1.courseslist.new', {
					ncyBreadcrumb: {
					    label: 'Opret Kursus'
					},
					url: '/new',
					templateUrl: appHelper.templatePath('saleforce1/course/new'),
					resolve: {
						jqui: function($ocLazyLoad){
							return $ocLazyLoad.load({
								files: ASSETS.core.jQueryUI
							});
						},
						selectboxit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.selectboxit,
							]);
						}
					}
				}).
				state('app.saleforce1.courseslist.edit', {
					ncyBreadcrumb: {
					    label: 'Courses Name'
					},
					url: '/edit-:id',
					templateUrl: appHelper.templatePath('saleforce1/course/edit'),
					resolve: {
						selectboxit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.selectboxit,
							]);
						}
					}
				}).
				state('app.saleforce1.courseslist.edit.newmodule', {
					ncyBreadcrumb: {
					    label: 'Opret Modul'
					},
					url: '/newmodule',
					templateUrl: appHelper.templatePath('saleforce1/course/newmodule')
				}).
				state('app.saleforce1.courseslist.edit.newmodule.newstd', {
					ncyBreadcrumb: {
					    label: 'Indhold'
					},
					url: '/new-std',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/new-standard'),
					resolve: {
						bootstrap: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.core.bootstrap,
							]);
						},
						bootstrapWysihtml5: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.forms.bootstrapWysihtml5,
							]);
						},
						uikit: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.uikit.base,
								ASSETS.uikit.codemirror,
								ASSETS.uikit.marked,
							]);
						},
						uikitHtmlEditor: function($ocLazyLoad){
							return $ocLazyLoad.load([
								ASSETS.uikit.htmleditor,
							]);
						},
					}
				}).
				state('app.saleforce1.courseslist.editstd', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-std:id',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/edit-standard')
				}).
				state('app.saleforce1.courseslist.edit.newmodule.newpp', {
					ncyBreadcrumb: {
					    label: 'Powerpoint'
					},
					url: '/new-pp',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/new-pp')
				}).
				state('app.saleforce1.courseslist.editpp', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-pp:id',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/edit-pp')
				}).
				state('app.saleforce1.courseslist.edit.newmodule.newscorm', {
					ncyBreadcrumb: {
					    label: 'SCORM'
					},
					url: '/new-scorm',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/new-scorm')
				}).
				state('app.saleforce1.courseslist.editscorm', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-scorm:id',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/edit-scorm')
				}).
				state('app.saleforce1.courseslist.edit.newmodule.newpoll', {
					ncyBreadcrumb: {
					    label: 'Survey'
					},
					url: '/new-poll',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/new-poll'),
					controller: 'UIModalsCtrl'
				}).
				state('app.saleforce1.courseslist.editpoll', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-poll:id',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/edit-poll'),
					controller: 'UIModalsCtrl'
				}).
				state('app.saleforce1.courseslist.edit.newmodule.newvideo', {
					ncyBreadcrumb: {
					    label: 'Video'
					},
					url: '/new-video',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/new-video')
				}).
				state('app.saleforce1.courseslist.editvideo', {
					ncyBreadcrumb: {
					    label: 'Module Name'
					},
					url: '/edit-video:id',
					templateUrl: appHelper.templatePath('saleforce1/course/modules/edit-video')
				}).	
				
		// Logins and Lockscreen
		state('login', {
			url: '/login',
			templateUrl: appHelper.templatePath('login'),
			controller: 'LoginCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
			}
		}).
        state('activate', {
            url: '/user/:user/activation/:activation_code',
            templateUrl: appHelper.templatePath('activation'),
            controller: 'UserActivateCtrl',
            resolve: {
                resources: function($ocLazyLoad){
                    return $ocLazyLoad.load([
                        ASSETS.forms.jQueryValidate,
                        ASSETS.extra.toastr,
                    ]);
                },
            }
        }).
        state('forgotpassword', {
            url: '/user/forgotpassword',
            templateUrl: appHelper.templatePath('forgotpassword'),
			controller: 'UserResetPasswordController'
        }).
        state('resetpassword', {
            url: '/user/:user/resetpassword/:reset_code',
            templateUrl: appHelper.templatePath('resetpassword'),
			controller: 'UserResetPasswordController'
        }).
		state('lockscreen', {
			url: '/lockscreen',
			templateUrl: appHelper.templatePath('lockscreen'),
			controller: 'LockscreenCtrl',
			resolve: {
				resources: function($ocLazyLoad){
					return $ocLazyLoad.load([
						ASSETS.forms.jQueryValidate,
						ASSETS.extra.toastr,
					]);
				},
			}
		});
});

app.config(['flowFactoryProvider', function (flowFactoryProvider) {
  flowFactoryProvider.defaults = {
    target: '/api/file',
      testChunks:false,
    permanentErrors: [404, 500, 501],
    maxChunkRetries: 1,
    chunkRetryInterval: 5000,
    simultaneousUploads: 4,
    singleFile: true
  };
  flowFactoryProvider.on('catchAll', function (event) {
    console.log('catchAll', arguments);
  });
  
}]);

app.config(['$httpProvider' , function($httpProvider) {
		$httpProvider.interceptors.push(['$q', '$injector',
							function($q,  $injector) {
								return {
									request: function(config) {
										//console.log(config.url);
									var loggedin = $injector.get('User').isLogged();
										if(!loggedin) {
											if(config.url !="app/tpls/login.html" && config.url != "app/tpls/activation.html" && config.url != "app/tpls/forgotpassword.html" && config.url!= "app/tpls/resetpassword.html")
												$injector.get('$state').go('login');
										}
										return config || $q.when(config);

									},
									responseError: function(response) {
										//just in  case
										var Redirect = $injector.get('URLs');
										if(response.status === 403) {
											// redirect to URLs.login
											$injector.get('$state').go('login');
										}
									return $q.reject(response);
					}
				};
			}
		]);
	}]);
	app.factory('URLs', ['$http','$state', function($http ,$state) {
		var URLs;
		URLs = $state.href('login',{},{absolute : true});
		return URLs;
	}]);

app.constant('ASSETS', {
	'core': {
		'bootstrap': appHelper.assetPath('js/bootstrap.min.js'), // Some plugins which do not support angular needs this

		'jQueryUI': [
			appHelper.assetPath('js/jquery-ui/jquery-ui.min.js'),
			appHelper.assetPath('js/jquery-ui/jquery-ui.structure.min.css'),
		],

		'moment': appHelper.assetPath('js/moment.min.js'),

		'googleMapsLoader': appHelper.assetPath('app/js/angular-google-maps/load-google-maps.js')
	},

	'charts': {

		'dxGlobalize': appHelper.assetPath('js/devexpress-web-14.1/js/globalize.min.js'),
		'dxCharts': appHelper.assetPath('js/devexpress-web-14.1/js/dx.chartjs.js'),
		'dxVMWorld': appHelper.assetPath('js/devexpress-web-14.1/js/vectormap-data/world.js'),
	},

	'xenonLib': {
		notes: appHelper.assetPath('js/xenon-notes.js'),
	},

	'maps': {

		'vectorMaps': [
			appHelper.assetPath('js/jvectormap/jquery-jvectormap-1.2.2.min.js'),
			appHelper.assetPath('js/jvectormap/regions/jquery-jvectormap-world-mill-en.js'),
			appHelper.assetPath('js/jvectormap/regions/jquery-jvectormap-it-mill-en.js'),
		],
	},

	'icons': {
		'meteocons': appHelper.assetPath('css/fonts/meteocons/css/meteocons.css'),
		'elusive': appHelper.assetPath('css/fonts/elusive/css/elusive.css'),
	},

	'tables': {
		'rwd': appHelper.assetPath('js/rwd-table/js/rwd-table.min.js'),

		'datatables': [
			appHelper.assetPath('js/datatables/dataTables.bootstrap.css'),
			appHelper.assetPath('js/datatables/datatables-angular.js'),
		],

	},

	'forms': {

		'select2': [
			appHelper.assetPath('js/select2/select2.css'),
			appHelper.assetPath('js/select2/select2-bootstrap.css'),

			appHelper.assetPath('js/select2/select2.min.js'),
		],

		'daterangepicker': [
			appHelper.assetPath('js/daterangepicker/daterangepicker-bs3.css'),
			appHelper.assetPath('js/daterangepicker/daterangepicker.js'),
		],

		'colorpicker': appHelper.assetPath('js/colorpicker/bootstrap-colorpicker.min.js'),

		'selectboxit': appHelper.assetPath('js/selectboxit/jquery.selectBoxIt.js'),

		'tagsinput': appHelper.assetPath('js/tagsinput/bootstrap-tagsinput.min.js'),

		'datepicker': appHelper.assetPath('js/datepicker/bootstrap-datepicker.js'),

		'timepicker': appHelper.assetPath('js/timepicker/bootstrap-timepicker.min.js'),

		'inputmask': appHelper.assetPath('js/inputmask/jquery.inputmask.bundle.js'),

		'formWizard': appHelper.assetPath('js/formwizard/jquery.bootstrap.wizard.min.js'),

		'jQueryValidate': appHelper.assetPath('js/jquery-validate/jquery.validate.min.js'),

            'dropzone': [
    appHelper.assetPath('js/dropzone/css/dropzone.css'),
    appHelper.assetPath('js/dropzone/dropzone.min.js'),
],

    'typeahead': [
    appHelper.assetPath('js/typeahead.bundle.js'),
    appHelper.assetPath('js/handlebars.min.js'),
],

    'multiSelect': [
    appHelper.assetPath('js/multiselect/css/multi-select.css'),
    appHelper.assetPath('js/multiselect/js/jquery.multi-select.js'),
],

    'icheck': [
			appHelper.assetPath('js/icheck/skins/all.css'),
			appHelper.assetPath('js/icheck/icheck.min.js'),
		],

		'bootstrapWysihtml5': [
			appHelper.assetPath('js/wysihtml5/src/bootstrap-wysihtml5.css'),
			appHelper.assetPath('js/wysihtml5/wysihtml5-angular.js')
		],
	},

	'uikit': {
		'base': [
			appHelper.assetPath('js/uikit/uikit.css'),
			appHelper.assetPath('js/uikit/css/addons/uikit.almost-flat.addons.min.css'),
			appHelper.assetPath('js/uikit/js/uikit.min.js'),
		],

		'codemirror': [
			appHelper.assetPath('js/uikit/vendor/codemirror/codemirror.js'),
			appHelper.assetPath('js/uikit/vendor/codemirror/codemirror.css'),
		],

		'marked': appHelper.assetPath('js/uikit/vendor/marked.js'),
		'htmleditor': appHelper.assetPath('js/uikit/js/addons/htmleditor.min.js'),
		'nestable': appHelper.assetPath('js/uikit/js/addons/nestable.min.js'),
	},

	'extra': {
		'tocify': appHelper.assetPath('js/tocify/jquery.tocify.min.js'),

		'toastr': appHelper.assetPath('js/toastr/toastr.min.js'),

		'fullCalendar': [
			appHelper.assetPath('js/fullcalendar/fullcalendar.min.css'),
			appHelper.assetPath('js/fullcalendar/fullcalendar.min.js'),
		],

		'cropper': [
			appHelper.assetPath('js/cropper/cropper.min.js'),
			appHelper.assetPath('js/cropper/cropper.min.css'),
		]
	}
});