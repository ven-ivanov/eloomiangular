/**
 * Created by Joannes Noel on 2/07/15.
 *
 */
angular.module('xenon.controllers')

    .controller('CourseListCtrl', function ($scope, CourseSrvc) {
		$scope.courses = CourseSrvc.courses;
		CourseSrvc.all();
    })

	.controller('CourseListNewCtrl', function ($scope, $state, $http, CourseSrvc) {
	
		$http.get('/api/misc/timespans').
		success(function(res, status, headers, config) {
			for(var i in res.data)
				$("select#valid-for").data("selectBox-selectBoxIt").add({ value: res.data[i].id, text: res.data[i].string });
		});
		
		$scope.courseImageUploaded = function($file, $message){
			var response = JSON.parse($message);
			if(response.images.length > 0)
				$scope.course.image_id = response.images[0].id;
		}

		$scope.submit = function(create_another){
			CourseSrvc.create($scope.certificateFile, $scope.course, function(){
				if(!create_another){
					$state.go('app.learning.courseslist');
				}
				for (var k in $scope.course) {
					if ($scope.course.hasOwnProperty(k) && $scope.course[k]) {
					   delete $scope.course[k];
					}
				}
			});
		}

    })

    .controller('CourseListEditCtrl', function ($scope, $http, $stateParams, CourseSrvc) {

		$scope.course_settings = {};
		
		$http.get('/api/misc/timespans').
		success(function(res, status, headers, config) {
			for(var i in res.data)
				$("select#valid-for").data("selectBox-selectBoxIt").add({ value: res.data[i].id, text: res.data[i].string });
			
			CourseSrvc.get($stateParams.id, function(res){
				res.valid_for = res.valid_for.id;
				angular.copy(res, $scope.course_settings);
				$("select#valid-for").data("selectBox-selectBoxIt").selectOption($scope.course_settings.valid_for);
			});
		});
		
		$scope.updateSettings = CourseSrvc.update();
    });
