/**
 * Created by Joannes Noel on 2/07/15.
 */
'use strict'
angular.module('xenon.services')
    .service('CourseSrvc', ['$http','$state', function ($http) {
		return {
			courses: [],
			create: function(cert, data, callback){
				var fd = new FormData();
				if(cert)
					fd.append('certificate_file', cert);
				for (var k in data) {
					if (data.hasOwnProperty(k) && data[k]) {
					   fd.append(k, data[k]);
					}
				}
				$http.post('/api/course', fd, {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				})
				.success(function(res){
					callback();
				})
				.error(function(){
				});
			},
			all: function(){
				var srvc = this;
				$http.get('/api/course/all', {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				})
				.success(function(res){
					srvc.courses.length = 0;
					for(var i in res.data){
						srvc.courses.push(res.data[i]);
					}
				})
				.error(function(){
				});
			},
			get: function($id, callback){
				$http.get('/api/course/'+$id+'?include=users,image,valid_for', {
					transformRequest: angular.identity,
					headers: {'Content-Type': undefined}
				})
				.success(function(res){
					callback(res);
				})
				.error(function(){
				});
			},
			update: function($id, data, callback){
			
			}
		}
	}]);
