<?php

use Eloomi\Models\User;

class UserRepositoryTest extends BaseTest {

	protected $users;

	public function setUp(){

		parent::setUp();
		$this->users = App::make('Eloomi\Repositories\EloquentUserRepository');
		User::flushEventListeners();
		User::boot();
	}

	public function test_user_creation_with_all_info()
	{
		$company = $this->createCompany();
		TenantScope::addTenant('company_id', $company->id);

		$info = $this->getUserData();
		$user = $this->users->create($info);

		$this->assertEquals($company->id, $user->company->id);
		$this->assertNotEquals($info['password'], $user->password);
	}

	// Should not be possible
	public function test_change_user_to_another_company()
	{
		$real_company = $this->createCompany();
		TenantScope::addTenant('company_id', $real_company->id);

		$user = $this->users->create($this->getUserData());
		$wrong_company = $this->createCompany();

		$user->fill( ['company_id' => $wrong_company] );

		$this->assertEquals(
			$real_company->id,
			$user->company->id
		);
	}

	public function test_wrong_password_confirmation(){
		$real_company = $this->createCompany();
		TenantScope::addTenant('company_id', $real_company->id);

		$info = $this->getUserData();
		$info['password'] = '12345678';
		$info['password_confirmation'] = 'fdsafdsa';

		$this->setExpectedException('Eloomi\Exceptions\UserExceptions\ValidationException');
		$this->users->create($info);
	}

}
