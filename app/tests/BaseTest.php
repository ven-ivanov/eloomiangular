<?php

use Illuminate\Foundation\Artisan;
//use Faker;
use Eloomi\Models\Company;
use Eloomi\Models\OrganizationalUnit;
use Eloomi\Models\User;
use Eloomi\Models\Group;
use Eloomi\Models\Language;

abstract class BaseTest extends TestCase {

	protected $faker;
	static $genders = ['F', 'M'];

	public function setUp(){

		parent::setUp();

		$this->faker = Faker\Factory::create();
		\Artisan::call('migrate:all');
	}

	protected function getUserData($preset = []){
		$password = $this->faker->word;
		$info = [
			'email' => $this->faker->unique()->safeEmail,
			'password' => $password,
			'password_confirmation' => $password,
			'first_name' => $this->faker->firstName,
			'last_name' => $this->faker->lastName,
			'birthday' => $this->faker->dateTimeBetween('-60 years', "-18 years")->format('y-m-d'),
			'gender' => $this->faker->optional(0.7)->randomElement(self::$genders),
			'title' => $this->faker->optional(0.7)->sentence(4),
			'phone' => $this->faker->optional(0.7)->phoneNumber,
			'timezone' => $this->faker->optional(0.7)->timezone,
		];

		return array_merge($info, $preset);
	}

	protected function createOU($parent = null, $name = null)
	{
		$OU = OrganizationalUnit::create([
				'name' => $name?: $this->faker->unique()->sentence(2),
		]);

		if ($parent){
			$OU->makeChildOf($parent);
		}

		return $OU;
	}

	protected function createUser($preset = [], $active = null)
	{
		$info = $this->getUserData($preset);
		$active = !is_null($active) ? $active : $this->faker->boolean();

		return Sentry::register($info, $active);
	}

	protected function createCompany($preset = [])
	{
		$info = [
			'name'  => $this->faker->unique()->word,
			'domain'   => $this->faker->unique()->domainName
		];

		$info = array_merge($info, $preset);
		return Company::create($info);
	}

	protected function createLanguage()
	{
		return Language::create([
			'name' => $this->faker->unique()->word,
			'code' => $this->faker->unique()->word
		]);
	}

}
