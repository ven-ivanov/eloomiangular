<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoachTraineeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('coach_trainee', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('coach_id')->unsigned()->index();
			$table->foreign('coach_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('trainee_id')->unsigned()->index();
			$table->foreign('trainee_id')->references('id')->on('users')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('coach_trainee');
	}

}
