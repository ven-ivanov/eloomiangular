<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSoftdeleteCompanyOrganizationalUnitToUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
            $table->string('reference')->nullable();
			$table->integer('image_id')->nullable()->unsigned();
			$table->foreign('image_id')->references('id')->on('files');
            $table->integer('company_id')->nullable()->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
			$table->softDeletes();

			$table->unique( array('company_id','reference') );
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
            $table->dropSoftDeletes();
            $table->dropForeign('users_company_id_foreign');
            $table->dropForeign('users_organizational_unit_id_foreign');
            $table->dropColumn('company_id');
            $table->dropColumn('organizational_unit_id');
		});
	}

}
