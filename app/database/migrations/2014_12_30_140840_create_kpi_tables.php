<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKpiTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goal', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->integer('min')->default(0);
			$table->integer('max');
			$table->boolean('is_overridable')->default(true);
			$table->morphs('for_entity');
			$table->timestamps();
		});

		Schema::create('goal_measurements', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('kpi_id');
			$table->integer('user_id');
			$table->integer('value');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goal');
		Schema::drop('goal_measurements');
	}

}
