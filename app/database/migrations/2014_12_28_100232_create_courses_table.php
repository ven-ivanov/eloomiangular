<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('code')->nullable();
			$table->text('description');
			$table->boolean('active')->default(true);
			$table->boolean('in_order')->default(true);
			$table->date('due_date')->nullable();

			$table->integer('points');


			$table->integer('certificate_id')->nullable()->unsigned();
			$table->foreign('certificate_id')->references('id')->on('certificates');

			$table->integer('valid_for')->nullable()->unsigned();
			$table->foreign('valid_for')->references('id')->on('timespans');

			$table->integer('image_id')->nullable()->unsigned();
			$table->foreign('image_id')->references('id')->on('files');

			$table->integer('company_id')->nullable()->unsigned();
			$table->foreign('company_id')->references('id')->on('companies');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses');
	}

}
