<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlideshareTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slideshare_presentations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('slideshare_id')->nullable();

//			$table->integer('company_id')->nullable()->unsigned();
//			$table->foreign('company_id')->references('id')->on('companies');

			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slideshare_presentations');
	}

}
