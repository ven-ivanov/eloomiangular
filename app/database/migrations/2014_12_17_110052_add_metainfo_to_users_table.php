<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMetainfoToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->date('birthday')->nullable();
			$table->enum('gender', ['F','M'])->nullable();
			$table->string('title')->nullable();
			$table->string('phone')->nullable();
			$table->string('timezone')->nullable();
			$table->integer('language_id')->unsigned()->nullable();
			$table->foreign('language_id')->references('id')->on('languages');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropForeign('users_language_id_foreign');
			$table->dropColumn('language_id');
			$table->dropColumn('timezone');
			$table->dropColumn('phone');
			$table->dropColumn('title');
			$table->dropColumn('gender');
			$table->dropColumn('birthday');
		});
	}

}
