<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddLeaderIdToOrganizationalUnitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('organizational_units', function(Blueprint $table)
		{
            $table->integer('leader_id')->unsigned()->nullable();
            $table->foreign('leader_id')->references('id')->on('users');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('organizational_units', function(Blueprint $table)
		{
			$table->dropForeign('organizational_unit_leader_id_foreign');
			$table->dropColumn('leader_id');
		});
	}

}
