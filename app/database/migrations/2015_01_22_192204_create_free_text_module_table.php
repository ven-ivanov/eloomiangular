<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFreeTextModuleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('free_text_modules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('text');
			$table->integer('image_id')->nullable()->unsigned();
			$table->foreign('image_id')->references('id')->on('files');

//			$table->integer('company_id')->nullable()->unsigned();
//			$table->foreign('company_id')->references('id')->on('companies');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('free_text_modules');
	}

}
