<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizationalUnitUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organizational_unit_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('organizational_unit_id')->unsigned()->index();
			$table->foreign('organizational_unit_id')->references('id')->on('organizational_units')->onDelete('cascade');
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organizational_unit_user');
	}

}
