<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('domain')->unique();
			$table->integer('language_id')->nullable()->unsigned();
			$table->string('color')->nullable();
			$table->integer('image_id')->unsigned()->nullable();
			$table->string('address')->nullable();
			$table->string('zip')->nullable();
			$table->string('city')->nullable();
			$table->string('cvr')->nullable();
			$table->string('phone')->nullable();
			$table->string('homepage')->nullable();
//			$table->string('type')->nullable();
			$table->string('contact_name')->nullable();

			$table->softDeletes();
			$table->timestamps();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
