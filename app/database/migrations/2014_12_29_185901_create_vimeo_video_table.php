<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVimeoVideoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vimeo_videos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('path')->nullable();
			$table->boolean('ready')->default(false);

//			$table->integer('company_id')->nullable()->unsigned();
//			$table->foreign('company_id')->references('id')->on('companies');

			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vimeo_videos');
	}

}
