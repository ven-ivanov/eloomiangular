<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSurveyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('surveys', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('pass_percentage')->default(100);

//			$table->integer('company_id')->nullable()->unsigned();
//			$table->foreign('company_id')->references('id')->on('companies');
			$table->timestamps();
		});

		Schema::create('questions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order');
			$table->text('question')->nullable();
			$table->morphs('questionable');

			$table->integer('survey_id')->nullable()->unsigned();
			$table->foreign('survey_id')->references('id')->on('surveys');

			$table->integer('company_id')->nullable()->unsigned();
			$table->foreign('company_id')->references('id')->on('companies');
			$table->timestamps();
		});

		Schema::create('survey_multiple_choice', function(Blueprint $table)
		{
			$table->increments('id');

//			$table->integer('company_id')->nullable()->unsigned();
//			$table->foreign('company_id')->references('id')->on('companies');
			$table->timestamps();
		});

		Schema::create('survey_multiple_choice_answers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('answer')->nullable();
			$table->boolean('is_correct')->default(false);

			$table->integer('question_id')->unsigned()->nullable();
			$table->foreign('question_id')->references('id')->on('survey_multiple_choice');

			$table->integer('company_id')->nullable()->unsigned();
			$table->foreign('company_id')->references('id')->on('companies');
			$table->timestamps();
		});

		Schema::create('survey_rating', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('min')->nullable();
			$table->string('max')->nullable();

//			$table->integer('company_id')->nullable()->unsigned();
//			$table->foreign('company_id')->references('id')->on('companies');
			$table->timestamps();
		});

		Schema::create('survey_free_text', function(Blueprint $table)
		{
			$table->increments('id');
//			$table->integer('company_id')->nullable()->unsigned();
//			$table->foreign('company_id')->references('id')->on('companies');
			$table->timestamps();
		});



	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('surveys');
		Schema::drop('questionable_survey');
		Schema::drop('multiple_choice');
		Schema::drop('multiple_choice_choices');
		Schema::drop('multiple_choice_choices');
	}

}
