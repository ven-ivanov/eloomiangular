<?php
use Eloomi\Data\Permissions;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class GroupsTableSeeder extends Seeder {

    static $count = 10;

    public function run(){

        Sentry::createGroup([
            'name'        => Permissions::CROSS_COMPANY_ADMIN,
            'permissions' => ['superuser' => 1]
        ]);

        Sentry::createGroup([
            'name'        => Permissions::COMPANY_ADMIN,
            'permissions' => [ ]
        ]);

        Sentry::createGroup([
            'name'        => Permissions::COMPANY_USER,
            'permissions' => [ ]
        ]);

    }
}