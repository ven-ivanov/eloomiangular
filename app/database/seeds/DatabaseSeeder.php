<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call('AppPartsTableSeeder');
		$this->call('LanguagesTableSeeder');
        $this->call('CompaniesTableSeeder');
        $this->call('OrganizationalUnitsTableSeeder');
        $this->call('GroupsTableSeeder');
        $this->call('UsersTableSeeder');
		$this->call('FilesTableSeeder');
		$this->call('TimespanTableSeeder');
		$this->call('TeamsTableSeeder');
		$this->call('CourseTableSeeder');

	}

}
