<?php
use Eloomi\Models\Timespan;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class TimespanTableSeeder extends Seeder {

    public function run(){

        $timespans = [
            "1 week" 	=> "1 week",
            "2 weeks" 	=> "2 weeks",
            "3 weeks" 	=> "3 weeks",
            "4 weeks" 	=> "4 weeks",
            "1 month" 	=> "1 month",
            "2 months" 	=> "2 months",
            "3 months" 	=> "3 months",
            "4 months" 	=> "4 months",
            "5 months" 	=> "5 months",
            "6 months" 	=> "6 months",
            "1 year" 	=> "1 year",
            "2 years" 	=> "2 years",
            "3 years" 	=> "3 years",
            "4 years" 	=> "4 years",
            "5 years" 	=> "5 years"
        ];

        \Eloquent::unguard();
        foreach($timespans as $human_string => $carbon_string){
            Timespan::create([
                'human_string'  => $human_string,
                'carbon_string' => $carbon_string
            ]);
         }
        \Eloquent::reguard();

    }
}