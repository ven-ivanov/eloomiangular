<?php
use Eloomi\Models\Company;
use Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class OrganizationalUnitsTableSeeder extends Seeder
{

    static $count = 15;
    /**
     * @var
     */
    private $units;
    /**
     * @var CompanyRepositoryInterface
     */
    private $companies;

    public function __construct(
        OrganizationalUnitRepositoryInterface $units,
        CompanyRepositoryInterface $companies
    ){

        $this->units = $units;
        $this->companies = $companies;
    }

    public function run()
    {

        $companies = $this->companies->index();
        foreach($companies as $company){
            $faker = Faker\Factory::create();
            TenantScope::addTenant('company_id', $company->id);
            $nodes = [
                $this->units->create([
                    'name' => $faker->word(2),
                    'code' => $faker->unique()->word(),
                ])
            ];

            for($i = 0; $i < self::$count - 1; $i++){
                $child = $this->units->create([
                    'name' => $faker->word(2),
                    'code' => $faker->unique()->word(),
                ]);
                if ($faker->numberBetween(1,5) != 5){
                    $parent = $faker->randomElement($nodes);
                    $this->units->moveToNewParent($child, $parent);
                }
                $nodes[] = $child;
            }
        }
    }
}