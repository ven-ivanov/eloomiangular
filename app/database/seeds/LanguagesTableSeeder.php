<?php
use Eloomi\Models\Language;
use Eloomi\Repositories\Interfaces\LanguageRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class LanguagesTableSeeder extends Seeder {

    /**
     * @var LanguageRepositoryInterface
     */
    private $languages;

    public function __construct(
        LanguageRepositoryInterface $languages
    ){
        $this->languages = $languages;
    }

    public function run(){

        $this->languages->create([
            'name' => 'danish',
            'code' => 'da'
        ]);

        $this->languages->create([
            'name' => 'english',
            'code' => 'en'
        ]);
    }
}