<?php
use Eloomi\Exceptions\UserExceptions\WrongAnswersException;
use Eloomi\Models\Company;
use Eloomi\Models\Timespan;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\ProgressRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\QuestionRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\TeamRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Repositories\Interfaces\Statistics\CourseStatisticsRepositoryInterface;
use Eloomi\Repositories\LMS\MultipleChoiceAnswerRepository;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class CourseTableSeeder extends Seeder {

    /**
     * @var CourseRepositoryInterface
     */
    private $courses;

    /**
     * @var UserRepositoryInterface
     */
    private $users;
    /**
     * @var TeamRepositoryInterface
     */
    private $teams;
    /**
     * @var OrganizationalUnitRepositoryInterface
     */
    private $units;
    /**
     * @var ModuleRepositoryInterface
     */
    private $modules;
    /**
     * @var MultipleChoiceAnswerRepository
     */
    private $answers;
    /**
     * @var QuestionRepositoryInterface
     */
    private $questions;
    /**
     * @var FileRepositoryInterface
     */
    private $files;
    /**
     * @var ProgressRepositoryInterface
     */
    private $progress;
    /**
     * @var CourseStatisticsRepositoryInterface
     */
    private $statistics;

    public function __construct(
        CourseRepositoryInterface $courses,
        UserRepositoryInterface $users,
        TeamRepositoryInterface $teams,
        OrganizationalUnitRepositoryInterface $units,
        ModuleRepositoryInterface $modules,
        MultipleChoiceAnswerRepository $answers,
        QuestionRepositoryInterface $questions,
        FileRepositoryInterface $files,
        ProgressRepositoryInterface $progress,
        CourseStatisticsRepositoryInterface $statistics
    ){
        $this->timespans = Timespan::all()->lists('id');
        $this->courses = $courses;
        $this->users = $users;
        $this->teams = $teams;
        $this->units = $units;
        $this->modules = $modules;
        $this->answers = $answers;
        $this->questions = $questions;
        $this->files = $files;
        $this->progress = $progress;
        $this->statistics = $statistics;
    }

    public function run()
    {

        $companies = Company::all();

        foreach ($companies as $company) {

            TenantScope::addTenant('company_id', $company->id);
            $user_ids = array_pluck($this->users->index(), 'id');
            $team_ids = array_pluck($this->teams->index(), 'id');
            $unit_ids = array_pluck($this->units->index(), 'id');
            $image_ids = $this->files->index()->lists('id');

            $faker = Faker\Factory::create();

            for ($i = 0; $i < 10; $i++)
            {
                $course = $this->courses->create([
                    'name' => $faker->unique()->sentence(4),
                    'code' => $faker->unique()->sentence(2),
                    'description' => $faker->paragraph(),
                    'in_order' => $faker->boolean(),
                    'due_date' => $faker->dateTimeBetween('+1 month', "+2 years")->format('Y-m-d'),
                    'price'    => $faker->optional(0.1)->randomElement([5000, 10000, 15000, 20000]),
                    'valid_for' => $faker->optional()->randomElement($this->timespans),
                    'points'    => $faker->randomElement([10, 50, 100, 200])
                ]);

                $course = $this->courses->update($course, [
                    'active' => $faker->boolean(),
                    'user_ids' => $faker->randomElements($user_ids, $faker->numberBetween(0, 2)),
                    'team_ids' => $faker->randomElements($team_ids, $faker->numberBetween(0, 2)),
                    'unit_ids' => $faker->randomElements($unit_ids, $faker->numberBetween(0, 2)),
                    'image_id' => $faker->optional()->randomElement($image_ids)
                ]);

                $users = $this->statistics->users_assigned($course)->load('module_progress')->all();
                for($moduleNumber = 0; $moduleNumber<$faker->numberBetween(1, 4); $moduleNumber++){
                    $module = $this->createModule($faker, $course->id);

                    $users = $faker->randomElements($users, $faker->numberBetween(0, count($users)));

                    $current_users = $users;
                    foreach($current_users as $user){
                        $this->progress->startModule($user, $module);
                    }

                    foreach($current_users as $user){
                        try {
                            $this->progress->updateProgress(
                                $user,
                                [
                                    'module_id' => $module->id,
                                    'answers'   => []
                                ]
                            );
                        } catch(WrongAnswersException $e){
                            $users = [];
                        }
                    }
                }
            }
        }
    }

    protected function createModule($faker, $course_id){

        $moduleType = $faker->randomElement(['survey', 'free_text', 'vimeo', 'slideshare']);
        $mainModule = $this->modules->create([
            'course_id'    => $course_id,
            'name'         => $faker->unique()->sentence(4),
            'description'  => $faker->paragraph(),
            'is_skippable' => $faker->boolean(),
            'module_type'  => $moduleType
        ]);
        $mainModule->save();
        switch($moduleType){
            case "survey";
                for($questionNumber=0; $questionNumber< $faker->numberBetween(4,6); $questionNumber++) {
                    $info = [
                        'type' => $faker->randomElement(['free_text_question', 'multiple_choice', 'rating']),
                        'module_id' => $mainModule->id
                    ];
                    $question = $this->questions->create($info);
                    $this->questions->update($question, ['question' => $faker->paragraph()]);
                    switch ($info['type']) {
                        case "multiple_choice":
                            for ($i = 0; $i < $faker->numberBetween(4, 6); $i++) {
                                $this->answers->create([
                                    'multiple_choice_question_id' => $question->questionable_id,
                                    'is_correct' => $faker->boolean(),
                                    'answer' => $faker->sentence(5)
                                ]);
                            }
                            break;
                        case "rating";
                            $this->questions->update($question, [
                                'min' => 1,
                                'max' => $faker->randomElement([5, 10, 100])
                            ]);
                            break;
                    }
                }

                break;
            case "free_text":
                $mainModule = $this->modules->update($mainModule,[
                    'text' => $faker->paragraph(5)
                ]);
                break;
//            case "slideshare":
//                $module = $this->modules->update($module,[
//                    'slideshare_id' => $faker->paragraph(5)
//                ]);
//                break;
            case "vimeo":
                $mainModule->load("teachable");
                $module = $mainModule->teachable;
                $module->path = '/video/'. $faker->numberBetween(1000000, 10000000);
                $module->save();
                break;
        }

        $mainModule->save();
        return $mainModule;
    }
}