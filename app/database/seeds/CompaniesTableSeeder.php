<?php
use Eloomi\Models\AppPart;
use Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface;
use Eloomi\Repositories\Interfaces\LanguageRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class CompaniesTableSeeder extends Seeder {

    private $faker;
    private $langauges;
    private $app_parts;
    /**
     * @var CompanyRepositoryInterface
     */
    private $companies;

    public function __construct(
        LanguageRepositoryInterface $languageRepositoryInterface,
        CompanyRepositoryInterface $companies,
        AppPart $appPart
    ){
        $this->faker = Faker\Factory::create();
        $this->faker->addProvider(new Faker\Provider\da_DK\Company($this->faker));
        $this->langauges = $languageRepositoryInterface->index()->map(function($language){return $language->id;});
        $this->langauges = $this->langauges->all();
        $this->companies = $companies;
        $this->app_parts = $appPart->all()->map(function ($part){ return $part->id; });
        $this->app_parts = $this->app_parts->all();
    }

    static $count = 10;
    public function run(){
        $this->companies->create(
            $this->createInfo([
                'domain' => 'tdc.eloomi.purpledigital.dk',
            ])
        );

        $this->companies->create(
            $this->createInfo([
                'domain' => 'telia.eloomi.purpledigital.dk',
            ])
        );

        for($i = 0; $i < self::$count-2; $i++){
            $this->companies->create(
                $this->createInfo()
            );
        }
    }

    private function createLigtColor(){
        return $this->createLightColorPart().
               $this->createLightColorPart().
               $this->createLightColorPart();
    }

    private function createLightColorPart(){
        return str_pad(dechex(mt_rand(0, 122)), 2, '0', STR_PAD_LEFT);
    }

    private function createInfo($preset = [])
    {
        $info = [
            'name'         => $this->faker->unique()->word,
            'domain'       => $this->faker->unique()->domainWord . '.eloomi.purpledigital.dk',
            'language_id'  => $this->faker->optional()->randomElement($this->langauges),
            'color'        => mt_rand(1,2) == 2 ? $this->createLigtColor() : null,
            'address'      => $this->faker->optional()->address,
            'zip'          => $this->faker->optional()->postcode,
            'city'         => $this->faker->optional()->city,
            'cvr'          => $this->faker->optional()->cvr,
            'phone'        => $this->faker->optional()->phoneNumber,
            'homepage'     => $this->faker->optional()->domainName,
            'contact_name' => $this->faker->optional()->name,
            'app_part_ids' => $this->faker->randomElements($this->app_parts, $this->faker->numberBetween(1, count($this->app_parts)))
        ];

        return array_merge($info, $preset);
    }
}