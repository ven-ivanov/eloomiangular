<?php
use Eloomi\Repositories\Interfaces\AppPartRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class AppPartsTableSeeder extends Seeder {

    /**
     * @var AppPartRepositoryInterface
     */
    private $apps;

    public function __construct(
        AppPartRepositoryInterface $apps
    ){
        $this->apps = $apps;
    }

    public function run(){

        $parts = ['LMS', "Coaching", "Reporting", "SalesForce1"];
        foreach($parts as $part){
            $this->apps->create(['name' => $part]);
        }
    }
}