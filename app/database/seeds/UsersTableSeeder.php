<?php
use Eloomi\Models\Group;
use Eloomi\Models\Language;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class UsersTableSeeder extends Seeder {

    static $count = 15;
    static $genders = ['F', 'M'];
    static $admin_info = [
        ['email' => 'rick@purplecow.dk', 'password' => 'secret'],
        ['email' => 'klaus@purplecow.dk', 'password' => 'secret'],
        ['email' => 'razvan@purplecow.dk', 'password' => 'secret'],
        ['email' => 'kenneth@purplecow.dk', 'password' => 'secret'],
        ['email' => 'frederik@purplecow.dk', 'password' => 'secret'],
        ['email' => 'joannesn1988@outlook.com', 'password' => 'secret'],
//        ['email' => 'ven@outlook.com', 'password' => 'secret'],
    ];

    protected $faker;
    protected $language_ids;
    /**
     * @var UserRepositoryInterface
     */
    private $users;
    /**
     * @var OrganizationalUnitRepositoryInterface
     */
    private $units;
    /**
     * @var CompanyRepositoryInterface
     */
    private $companies;

    private $organizational_unit_ids;

    public function __construct(
        UserRepositoryInterface $users,
        OrganizationalUnitRepositoryInterface $units,
        CompanyRepositoryInterface $companies
    ){
        $this->faker = Faker\Factory::create();
        $this->language_ids = Language::all()->lists('id');
        $this->users = $users;
        $this->units = $units;
        $this->companies = $companies;
    }

    public function run()
    {

        $companies = $this->companies->index();

        foreach ($companies as $company) {
            TenantScope::addTenant('company_id', $company->id);
            $this->organizational_unit_ids = $this->units->index()->lists('id');

            foreach(self::$admin_info as $admin_info){
                $info = $this->createUserInfo($admin_info);
                $user = $this->users->create($info);
                $this->activate($user, $info);
            }

            for ($i = 0; $i < self::$count - count(self::$admin_info); $i++) {
                $info = $this->createUserInfo();
                $this->users->create($info);
            }

            $users = $this->users->index();
            $user_ids = $users->lists('id');
            foreach($users as $user){
                $candidate_ids = array_filter($user_ids, function($element) use ($user){return $element != $user->id;});
                $coach_ids = $this->faker->randomElements($candidate_ids, $this->faker->numberBetween(1, count($candidate_ids)));
                $user->coaches()->sync($coach_ids);
            }
        }
    }

    protected function createUserInfo($presets = []){
        $info =[
            'email' => $this->faker->unique()->safeEmail,
            'password' => $this->faker->word,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'birthday' => $this->faker->dateTimeBetween('-60 years', "-18 years")->format('Y-m-d'),
            'gender' => $this->faker->optional(0.7)->randomElement(self::$genders),
            'title' => $this->faker->optional(0.7)->sentence(4),
            'phone' => $this->faker->optional(0.7)->phoneNumber,
            'timezone' => $this->faker->optional(0.7)->timezone,
            'language_id' => $this->faker->optional(0.7)->randomElement($this->language_ids),
            'organizational_unit_id' => $this->faker->randomElement($this->organizational_unit_ids)
        ];

        return array_merge($info, $presets);
    }

    protected function activate($user, $info){
        $this->users->activateUser(
            $user,
            $user->getActivationCode(),
            ['password' => $info['password'], 'password_confirmation' => $info['password']]
        );
    }

    protected function attachRandomGroup($user){
        $user->groups()->attach(
            $this->faker->numberBetween(Group::min('id'), Group::max('id'))
        );
    }
}