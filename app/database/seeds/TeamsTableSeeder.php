<?php
use Eloomi\Models\Company;
use Eloomi\Repositories\Interfaces\Organization\TeamRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;

/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 02/12/14
 * Time: 19.26
 */

class TeamsTableSeeder extends Seeder {

    /**
     * @var UserRepositoryInterface
     */
    private $users;
    /**
     * @var TeamRepositoryInterface
     */
    private $teams;

    public function __construct(
        UserRepositoryInterface $users,
        TeamRepositoryInterface $teams
    ){
        $this->users = $users;
        $this->teams = $teams;
    }

    public function run()
    {

        $companies = Company::all();

        $faker = Faker\Factory::create();
        foreach ($companies as $company) {
            TenantScope::addTenant('company_id', $company->id);
            $user_ids = array_pluck($this->users->index(), 'id');

            for($i = 0; $i < 10; $i++) {

                $member_ids = $faker->randomElements($user_ids, $faker->numberBetween(1, count($user_ids)));

                $team = $this->teams->create([
                    'name'              => $faker->unique()->sentence(4),
                    'description'       => $faker->paragraph(),
                    'leader_id'         => $faker->randomElement($member_ids),
                ]);

                $this->teams->update($team, [
                    'user_ids'          => $member_ids
                ]);

            }

        }
    }
}