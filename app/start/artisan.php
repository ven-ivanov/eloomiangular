<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/


Artisan::add(new MigrateAll);
Artisan::add(new ClearBeanstalkdQueueCommand);
Artisan::add(new DeleteExpiredCourseCompletions(
    \App::make('Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface'),
    \App::make('Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface')
));
Artisan::add(new EloomiTestCommand(
    \App::make('Eloomi\Repositories\Interfaces\Statistics\CourseStatisticsRepositoryInterface'),
    \App::make('Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface')
));

Artisan::add(new RemoveUnusedfiles(
    \App::make('Eloomi\Repositories\Interfaces\FileRepositoryInterface')
));

Artisan::add(new RebuildOrganizationTrees(
    \App::make('Eloomi\Models\OrganizationalUnit')
));