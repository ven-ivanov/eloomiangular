<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',

));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});


// TODO: DRY it up
App::error(function(AccessDeniedHttpException $exception, $code)
{
	Log::warning($exception);

	$resource = App::make('Item', [
		'data' => [],
		'transformer' => function($data){ return []; }
	]);

	$resource->setMetaValue('message', 'You are not logged in');
	$resource->setMetaValue('status', 'error');
	$resource->setMetaValue('code', '403');

	$data = with(App::make('League\Fractal\Manager'))->createData($resource)->toJson();
	$headers = [
		'Content-Type' => 'application/json',
		'Access-Control-Allow-Origin' => '*'
	];

	return Symfony\Component\HttpFoundation\Response::create($data, 403, $headers);
});

/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/



require app_path().'/filters.php';

/*
|--------------------------------------------------------------------------
| Monitor logging
|--------------------------------------------------------------------------
|
| Monitor the log so admins can be alerted in suitable fashion if needed
|
*/

Log::listen(function($level, $message, $context)
{
	if (!in_array($level,['error', 'critical', 'alert'])){
		return;
	}

//	Mail::queue('emails.problem', ['message' => $message, 'context' => $context], function($message) use($level)
//	{
//		$message
//			->to(Config::get('notification.email'))
//			->subject($level . ': Problems on Eloomi!');
//	});
});