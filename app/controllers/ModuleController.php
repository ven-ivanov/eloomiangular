<?php

use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Eloomi\Transformers\ModuleTransformer;

use Eloomi\Transformers\ArrayTransformer;
use League\Fractal\Manager;

class ModuleController extends CrudController {

	/**
	 * @var ModuleRepositoryInterface
	 */
	protected $modules;
	/**
	 * @var ModuleTransformer
	 */
	protected $moduleTransformer;

	public function __construct(
		ModuleRepositoryInterface $modules,
		ModuleTransformer $moduleTransformer,
		Manager $manager,
		ArrayTransformer $nullTransformer

	){
		parent::__construct($manager, $nullTransformer, $modules, $moduleTransformer);
		$this->modules = $modules;
		$this->moduleTransformer = $moduleTransformer;
	}

	/**
	 * Display a listing of the resource.
	 * GET /module
	 *
	 * @param int $course_id
	 * @return Response
	 */
	public function courseModules($course_id)
	{
		$modules = $this->modules->getModulesByCourseId($course_id);
		$resource = $this->createCollection($modules, $this->moduleTransformer, 'modules');

		return $this->respondWithSuccess($resource);
	}

	public function create()
	{
		echo '<form method="POST" action="/api/module/" enctype="multipart/form-data"/>';
		echo <<<HTML
<input type="hidden" name="module_type" value="vimeo" />
<input type="hidden" name="name" value="TestMovie" />
<input type="hidden" name="description" value="This is awesome" />
<input type="file" name="video" />
<input type="submit" />
</form>
HTML;

	}

}