<?php

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Models\Company;
use Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;
use Eloomi\Transformers\ArrayTransformer;
use Symfony\Component\HttpFoundation\Response;
use Eloomi\Transformers\OrganizationalUnitTransformer;
use League\Fractal\Manager;

class OrganizationalUnitController extends CrudController {

	protected $organizational_units;
	/**
	 * @var \Eloomi\Transformers\UserTransformer
	 */
	protected $ou_transformer;
	protected $array_transformer;
	/**
	 * @var CompanyRepositoryInterface
	 */
	protected $company;

	public function __construct(
		OrganizationalUnitRepositoryInterface $organizational_units,
		OrganizationalUnitTransformer $ou_transformer,
		ArrayTransformer $array_transformer,
		Company $company,
		Manager $manager
	){
		parent::__construct($manager, $array_transformer, $organizational_units,$ou_transformer);
		$this->organizational_units = $organizational_units;
		$this->ou_transformer = $ou_transformer;
		$this->company = $company;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$root = Input::get('root_id');
		if (!is_null($root)){
			$root = $this->organizational_units->getById($root);
		}

		$depth = Input::get('depth', null);
		$units = $this->organizational_units->getAll($root, $depth);

		$resource = $this->createCollection($units, $this->ou_transformer);

		if (!Input::get('nested', true)){
			return $this->respondWithSuccess($resource);
		}

		$resource->setMetaValue("meta", ['status' => 'success']);
		foreach($this->metas as $key => $value){
			$resource->setMetaValue($key, $value);
		}

		$data = $this->manager->createData($resource)->toArray();
		usort($data['data'], function($a, $b){
			if ($a['depth'] == $b['depth']) { return 0; }

			return $a['depth'] > $b['depth'] ? 1 : 0;
		});

		$data['data'] = $this->reorderArray($data['data']);
		$headers = ['Content-Type' => 'application/json'];

		return Response::create(json_encode($data), 200, $headers);
	}

	private function reorderArray($array){
		$temp_parent_array = [];
		$roots = [];

		while($element = array_pop($array)){
			$current_parent = $element['parent_id'];
			$id = $element['id'];

			if (!isset($temp_parent_array[$id])){
				$element['children'] = [];
			} else {
				$children = $temp_parent_array[$id];
				usort($children, function($a, $b){ return strcasecmp($a['name'], $b['name']); });
				$element['children'] = $children;
			}

			if (is_null($current_parent)){
				$roots[] = $element;
				continue;
			}

			if (!isset($temp_parent_array[$current_parent])){
				$temp_parent_array[$current_parent] = [];
			}
			$temp_parent_array[$current_parent][] = $element;
		}

		return $roots;
	}

	public function update_structure(){
		$units = Input::get('units');

		$this->organizational_units->update_structure($units);

		$resource = $this->createEmptyResource();

		return $this->respondWithSuccess($resource);
	}

	/**
	 * Search the storage in the storage for the term.
	 *
	 * @param  string  $term
	 * @return Response
	 */
	public function search($term)
	{
		$order = Input::get('order', 'first_name');
		$start = Input::get('start', null);
		$stop  = Input::get('stop',  null);
		$users = $this->organizational_units->search($term, $start, $stop, $order);

		$resource = App::make('Collection', [
			'data' => $users,
			'transformer' => $this->user_transformer,
			'name' => 'users'
		]);

		return $this->respondWithSuccess($resource);
	}

	public function importUnits(){

		$file = Input::file('file');
		$type = Input::get('type', false);

		if(is_null($file)){
			return $this->respondBadRequest("File required to import");
		}

		if (!$file->isValid()){
			return $this->respondBadRequest("File not uploaded correctly");
		}

		if (!$type){
			return $this->respondBadRequest("The type has not been specified");
		}

		try {
			$this->organizational_units->importUnitsFromFile($file, $type);
		} catch (InvalidInputException $e){
			return $this->respondBadRequest($e->getMessage());
		} catch (IOException $e){
			return $this->respondWithServerError($e->getMessage());
		} catch (Exception $e){
			throw $e;
			return $this->respondWithError("An unexpected error occured");
		}

		return $this->index();
	}

	public function importUsers($id){

		$file = Input::file('file');
		$type = Input::get('type', false);
		$ou = $this->organizational_units->getById($id);

		if(!$file){
			return $this->respondBadRequest("File required to import");
		}

		if (!$file->isValid()){
			return $this->respondBadRequest("File not uploaded correctly");
		}

		if (!$type){
			return $this->respondBadRequest("The type has not been specified");
		}

		try {
			$ou = $this->organizational_units->importUsersFromFile($ou, $file, $type);
		}
		catch (InvalidInputException $e) {
			return $this->respondBadRequest($e->getMessage());
		}
		catch(UserNotFoundException $e) {
			return $this->respondBadRequest($e->getMessage());
		}
		catch(UserNotFoundException $e) {
			return $this->respondBadRequest($e->getMessage());
		}
		catch (Exception $e) {
			throw $e;
			return $this->respondWithError("An unexpected error occured");
		}

		$this->manager->parseIncludes('users');
		$resource = $this->createItem($ou, $this->ou_transformer);

		return $this->respondWithSuccess($resource);

	}


}
