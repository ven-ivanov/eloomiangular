<?php

use Eloomi\Repositories\Interfaces\LMS\QuestionRepositoryInterface;
use Eloomi\Repositories\MultipleChoiceAnswerRepository;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\MultipleChoiceAnswerTransformer;
use Eloomi\Transformers\QuestionTransformer;
use League\Fractal\Manager;

class MultipleChoiceAnswerController extends \CrudController {

	public function __construct(
		MultipleChoiceAnswerRepository $answers,
		MultipleChoiceAnswerTransformer $anwserTransformer,
		ArrayTransformer $null_transformer,
		Manager $manager
	){
		parent::__construct($manager, $null_transformer, $answers, $anwserTransformer);
	}
}