<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/12/14
 * Time: 15.05
 */

use Eloomi\Exceptions\EloomiException;
use Eloomi\Transformers\ArrayTransformer;
use League\Fractal\Resource\ResourceAbstract;
use League\Fractal\Serializer\ArraySerializer;
use League\Fractal\Manager;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends BaseController {

    protected $statusCode = Response::HTTP_OK;
    protected $data = [];
    protected $users;
    protected $manager;
    protected $resource;
    protected $array_transformer;
    protected $metas = [];

    public function __construct(
        Manager $manager,
        ArrayTransformer $array_transformer
    ){
        parent::__construct();
        $this->manager = $manager;
        $this->manager->setSerializer(new ArraySerializer());
        $this->manager->parseIncludes(Input::get('include', []));
        $this->array_transformer = $array_transformer;
    }

    protected function getStatusCode(){
        return $this->statusCode;
    }

    protected function addMeta($key, $value){
        $this->metas[$key] = $value;
        return $this;
    }

    protected function setStatusCode($code){
        $this->statusCode = $code;
        return $this;
    }

    protected function respondNotFound($message = "Not found"){
        return $this->setStatusCode(Response::HTTP_NOT_FOUND)
            ->respondWithError($message);
    }

    protected function respondBadRequest($message = "Bad request"){
        return $this->setStatusCode(Response::HTTP_BAD_REQUEST)
            ->respondWithError($message);
    }

    protected function respondNotAllowed($message = "Not allowed"){
        return $this->setStatusCode(Response::HTTP_FORBIDDEN)
            ->respondWithError($message);
    }

    protected function respondServerError($message = "An error occured"){
        return $this->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR)
            ->respondWithError($message);
    }

    protected function respondWithError($message){

        $resource = $this->createEmptyResource();

        $resource
            ->setMetaValue('status', 'error')
            ->setMetaValue('message', $message)
            ->setMetaValue('error_code', $this->getStatusCode());

        return $this->respond($resource);
    }

    protected function respondWithSuccess(ResourceAbstract $resource){

        $resource->setMetaValue('status', 'success');
        return $this->respond($resource);
    }

    protected function respond(ResourceAbstract $resource, $headers = []){

        foreach($this->metas as $key => $value){
            $resource->setMetaValue($key, $value);
        }

        $data = $this->manager->createData($resource)->toJson();
        $headers = array_merge([
            'Content-Type' => 'application/json',
//            'Access-Control-Allow-Origin' => '*'
        ], $headers);

        return Response::create($data, $this->getStatusCode(), $headers);
    }

    protected function respondWithEloomiException(EloomiException $exception){
        return $this->respondBadRequest($exception->getMessage());
    }

    protected static function createItem($data, $callback, $name = null){
        return self::createResource('Item', $data, $callback, $name);
    }

    protected static function createCollection($data, $callback, $name = null){
        return self::createResource('Collection', $data, $callback, $name);
    }

    protected function createEmptyResource(){
        return $this->createItem([], $this->array_transformer);
    }

    private static function createResource($type, $data, $callback, $name){
        $resource_array = [
            'data' => $data,
            'transformer' => $callback
        ];

        if ($name){
            $resource_array['name'] = $name;
        }

        return App::make($type, $resource_array);
    }

}
