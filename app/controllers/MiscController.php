<?php

use Cartalyst\Sentry\Groups\Eloquent\Group;
use Eloomi\Repositories\Interfaces\AppPartRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Repositories\Interfaces\SessionRepositoryInterface;
use Eloomi\Exceptions\SessionExceptions;
use Eloomi\Transformers\AppPartTransformer;
use Eloomi\Transformers\GroupTransformer;
use Eloomi\Transformers\LanguageTransformer;
use Eloomi\Transformers\TimespanTransformer;
use Eloomi\Transformers\ArrayTransformer;
use League\Fractal\Manager;
use Eloomi\Models\Language;
use Eloomi\Models\Timespan;

class MiscController extends ApiController {
    /**
     * @var LanguageTransformer
     */
    protected $languageTransformer;
    /**
     * @var TimespanTransformer
     */
    protected $timespanTransformer;
    /**
     * @var AppPartTransformer
     */
    private $appPartTransformer;
    /**
     * @var AppPartRepositoryInterface
     */
    private $apps;
    /**
     * @var GroupTransformer
     */
    private $groupTransformer;

    /**
     * @param ArrayTransformer $null_transformer
     * @param Manager $manager
     * @param LanguageTransformer $languageTransformer
     * @param TimespanTransformer $timespanTransformer
     * @param AppPartTransformer $appPartTransformer
     * @param GroupTransformer $groupTransformer
     * @param UserRepositoryInterface $users
     * @param AppPartRepositoryInterface $apps
     */

	public function __construct(
        ArrayTransformer $null_transformer,
        Manager $manager,
        LanguageTransformer $languageTransformer,
        TimespanTransformer $timespanTransformer,
        AppPartTransformer $appPartTransformer,
        GroupTransformer $groupTransformer,
        UserRepositoryInterface $users,
        AppPartRepositoryInterface $apps
    ){
        parent::__construct($manager, $null_transformer);
        $this->languageTransformer = $languageTransformer;
        $this->timespanTransformer = $timespanTransformer;
        $this->appPartTransformer = $appPartTransformer;
        $this->apps = $apps;
        $this->groupTransformer = $groupTransformer;
        $this->users = $users;
    }

    public function timezones(){
        $zones_array = array();
        $timestamp = time();
        foreach(timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $zones_array[$key]['name'] = $zone;
            $zones_array[$key]['gmt_diff'] = date('P', $timestamp);
        }
        usort($zones_array, function($a, $b){
            $a = (int) str_replace(':', '', $a['gmt_diff']);
            $b = (int) str_replace(':', '', $b['gmt_diff']);

            if ($a ==$b ) { return 0; }

            return $a < $b ? -1 : 1;
        });

        $resource = $this->createCollection($zones_array, function($data){
           return $data;
        });

        return $this->respondWithSuccess($resource);
    }

    public function languages(){
        $languages = Language::all();
        $resource = $this->createCollection($languages, function($data){
            return $data;
        });

        return $this->respondWithSuccess($resource);
    }

    public function timespans(){
        $timespans = Timespan::all();

        $resource = $this->createCollection($timespans, $this->timespanTransformer);

        return $this->respondWithSuccess($resource);
    }

    public function app_parts(){
        $parts = $this->apps->index();
        $resource = $this->createCollection($parts, $this->appPartTransformer);

        return $this->respondWithSuccess($resource);
    }

    public function groups(){
        $group = $this->users->getCurrentUser()->getGroups()->first();
        $groups = Group::where('id','>=', $group->id)->get();
        $resource = $this->createCollection($groups, $this->groupTransformer);

        return $this->respondWithSuccess($resource);
    }

}