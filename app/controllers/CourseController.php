<?php

use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\Statistics\CourseStatisticsRepositoryInterface;
use Eloomi\Transformers\CourseTransformer;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\ProgressTransformer;
use League\Fractal\Manager;
//use Input;

class CourseController extends \CrudController {

	protected $user_transformer;
	protected $array_transformer;
	protected $courses;
	/**
	 * @var CourseStatisticsRepositoryInterface
	 */
	private $courseStatistics;

	public function __construct(
		CourseRepositoryInterface $courses,
		CourseTransformer $course_transformer,
		ArrayTransformer $null_transformer,
		Manager $manager,
		CourseStatisticsRepositoryInterface $courseStatistics
	){
		parent::__construct($manager, $null_transformer, $courses, $course_transformer);
		$this->courses = $courses;
		$this->courseStatistics = $courseStatistics;
	}

	public function indexAll(){
		try{
			$models = $this->courses->index(true);
			$resource = $this->createCollection($models, $this->main_transformer);

			return $this->respondWithSuccess($resource);
		} catch (EloomiException $e){
			return $this->respondWithEloomiException($e);
		} catch (Exception $e){
			throw $e;
			return $this->respondServerError("An unexpected error occured");
		}
	}

	public function statistics(){
//		$this->course
	}

}
