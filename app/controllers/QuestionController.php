<?php

use Eloomi\Repositories\Interfaces\LMS\QuestionRepositoryInterface;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\QuestionTransformer;
use League\Fractal\Manager;

class QuestionController extends \CrudController {

	public function __construct(
		QuestionRepositoryInterface $questions,
		QuestionTransformer $questionTransformer,
		ArrayTransformer $null_transformer,
		Manager $manager
	){
		parent::__construct($manager, $null_transformer, $questions, $questionTransformer);
	}
}