<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/12/14
 * Time: 15.05
 */

use Eloomi\Exceptions\EloomiException;
use Eloomi\Transformers\ArrayTransformer;
use League\Fractal\Manager;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;
use League\Fractal\TransformerAbstract;

class CrudController extends ApiController {

    /**
     * @var CrudRepositoryInterface
     */
    protected $main_repository;
    /**
     * @var TransformerAbstract
     */
    protected $main_transformer;

    public function __construct(
        Manager $manager,
        ArrayTransformer $array_transformer,
        CrudRepositoryInterface $main_repository,
        TransformerAbstract $main_transformer
    ){
        parent::__construct($manager, $array_transformer);
        $this->main_repository = $main_repository;
        $this->main_transformer = $main_transformer;
    }

    /**
     * @return CrudRepositoryInterface
     */
    protected function getMainRepository(){
        return $this->main_repository;
    }

    /**
     * @return CrudRepositoryInterface
     */
    protected function getMainTransformer(){
        return $this->main_transformer;
    }

    public function index(){
        try{
            $models = $this->main_repository->index();
            $resource = $this->createCollection($models, $this->main_transformer);

            return $this->respondWithSuccess($resource);
        } catch (EloomiException $e){
            return $this->respondWithEloomiException($e);
        } catch (Exception $e){
            throw $e;
            return $this->respondServerError("An unexpected error occured");
        }
    }

    /**
     * Store a newly created resource in storage.
     * @return Response
     * @throws Exception
     */
    public function store()
    {
        try {
            $model = $this->main_repository->create(Input::all());
            return $this->show($model);
        } catch (EloomiException $e){
            return $this->respondWithEloomiException($e);
        } catch (Exception $e){
            throw $e;
            return $this->respondServerError("An unexpected error occured");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $model mixed
     * @return Response
     */
    public function show($model)
    {
        if (ctype_digit($model)){
            $model = $this->main_repository->getById($model);
        }

        $resource = $this->createItem($model, $this->main_transformer);
        return $this->respondWithSuccess($resource);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $model = $this->main_repository->getById($id);
        $this->main_repository->delete($model);

        $resource = $this->createEmptyResource();
        return $this->respondWithSuccess($resource);
    }

    public function multiDestroy(){
        $ids = Input::get('ids');
        if (!is_array($ids)) { $ids = explode(',', $ids); }

        $this->main_repository->deleteByIds($ids);
        $resource = $this->createEmptyResource();
        return $this->respondWithSuccess($resource);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $model = $this->main_repository->updateById($id, Input::all());
        $resource = $this->createItem($model, $this->main_transformer);

        return $this->respondWithSuccess($resource);
    }


}
