<?php

use Eloomi\Repositories\Interfaces\SessionRepositoryInterface;
use Eloomi\Exceptions\SessionExceptions;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\UserTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Eloomi\Models\Company;

class SessionController extends ApiController {

	protected $session;
    protected $user_transformer;

    /**
     * @param SessionRepositoryInterface $session
     * @param UserTransformer $user_transformer
     * @param ArrayTransformer $null_transformer
     * @param Manager $manager
     * @internal param UserTransformer $user_user_transformer
     * @internal param UserTransformer $user_transformer
     * @internal param SessionRepositoryInterface $null_transformer
     */

	public function __construct(
        SessionRepositoryInterface $session,
        UserTransformer $user_transformer,
        ArrayTransformer $null_transformer,
        Manager $manager
    ){
        parent::__construct($manager, $null_transformer);
		$this->session = $session;
        $this->user_transformer = $user_transformer;
    }


    // TODO: DELETE WHEN DONE TESTING
    public function create(){
        echo <<<HTML
<form method="post" action="/api/sessions">
	<input type="hidden" value="rick@purplecow.dk" name="email"/>
	<input type="hidden" value="secret" name="password"/>
	<input type="hidden" name="remember" value="1" />
	<input type="submit"/>
</form>
HTML;
    }

	/**
	 * Store a newly created resource in storage.
	 * POST /sessions
	 *
	 * @return Response
	 */
	public function store()
	{
        $credentials = Input::only('email', 'password');
        $rememberme  = Input::get('remember', false) !== false ;

        try {
            $user = $this->session->store($credentials, $rememberme);
        }
        catch(SessionExceptions\MissingInputException $e){
            return $this->respondBadRequest($e->getMessage());
        }
        catch(SessionExceptions\WrongInputException $e){
            return $this->respondBadRequest($e->getMessage());
        }
        catch(SessionExceptions\UserNotActivatedException $e){
            return $this->respondNotAllowed($e->getMessage());
        }
        catch(SessionExceptions\NotAllowedException $e){
            return $this->respondNotAllowed($e->getMessage());
        }


        $resource = $this->createItem($user, $this->user_transformer, 'name');
        return $this->respondWithSuccess($resource);

	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /sessions/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id = null)
	{
		$this->session->destroy();

        $resource =  $this->createEmptyResource();

        return $this->respondWithSuccess($resource);
	}

}