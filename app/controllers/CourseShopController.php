<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/12/14
 * Time: 15.05
 */

use Eloomi\Exceptions\EloomiException;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\CourseShopRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Repositories\LMS\CourseShopRepository;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\CourseTransformer;
use League\Fractal\Manager;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;
use League\Fractal\TransformerAbstract;

class CourseShopController extends ApiController {

    /**
     * @var CourseShopRepository
     */
    private $course_shop;
    /**
     * @var CourseTransformer
     */
    private $courseTransformer;
    /**
     * @var CourseRepositoryInterface
     */
    private $courses;

    public function __construct(
        CourseShopRepository $course_shop, // Using the interface causes an error - don't know why
        CourseRepositoryInterface $courses,
        ArrayTransformer $arrayTransformer,
        CourseTransformer $courseTransformer,
        Manager $manager,
        UserRepositoryInterface $users
    ){
        $this->course_shop = $course_shop;
        parent::__construct($manager, $arrayTransformer);
        $this->courseTransformer = $courseTransformer;
        $this->courses = $courses;
        $this->users = $users;
    }

    public function index(){

        try{
            $courses = $this->course_shop->listCourses();
            $resource = $this->createCollection($courses, $this->courseTransformer);

            return $this->respondWithSuccess($resource);
        } catch (EloomiException $e){
            return $this->respondWithEloomiException($e);
        } catch (Exception $e){
            throw $e;
            return $this->respondServerError("An unexpected error occured");
        }
    }

    public function buy(){
        try{
            $course_ids = \Input::get('course_ids');

            if (!$this->course_shop->isSellable($course_ids)){
                throw new \Eloomi\Exceptions\InvalidInputException("At least one of the selected courses are not available for purchase");
            }

            $courses = [];
            foreach($course_ids as $course_id){
                $courses[] = $this->course_shop->buy($course_id);
            }

            $resource = $this->createCollection($courses, $this->courseTransformer);
            return $this->respondWithSuccess($resource);
        } catch (EloomiException $e){
            return $this->respondWithEloomiException($e);
        } catch (Exception $e){
            throw $e;
            return $this->respondServerError("An unexpected error occured");
        }
    }

    public function assign(){
        try{
            $course_id  = \Input::get('course_id');
            $company_id = \Input::get('company_id');
            $price      = \Input::get('price', null);

            if (is_null($price)){
                throw new \Eloomi\Exceptions\InvalidInputException("price must be set");
            }

            if (!ctype_digit($price)){
                throw new \Eloomi\Exceptions\InvalidInputException("Price must be a whole positive number");
            }

            $course = $this->course_shop->assign($course_id, $company_id, $price);
            $resource = $this->createItem($course, $this->courseTransformer);
            return $this->respondWithSuccess($resource);

        } catch (EloomiException $e){
            return $this->respondWithEloomiException($e);
        } catch (Exception $e){
            throw $e;
            return $this->respondServerError("An unexpected error occured");
        }
    }

    public function request(){
        $course_id = Input::get('course_id');
        $user = $this->users->getCurrentUser();
        $this->course_shop->request($course_id, $user);

        $resource = $this->createEmptyResource();

        return $this->respondWithSuccess($resource);
    }


}
