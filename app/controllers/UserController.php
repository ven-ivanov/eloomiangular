<?php

use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\UserTransformer;
use League\Fractal\Manager;

class UserController extends \CrudController {

	/*
	 * @var UserRepositoryInterface
	 */
	protected $users;

	/*
	 * @var UserTransformer
	 */
	protected $user_transformer;

	public function __construct(
		UserRepositoryInterface $users,
		UserTransformer $user_transformer,
		ArrayTransformer $null_transformer,
		Manager $manager
	){
		parent::__construct($manager, $null_transformer, $users, $user_transformer);
		$this->users = $users;
		$this->user_transformer = $user_transformer;
	}

	public function activate($id){


		$code = Input::get('activation_code');
        $user = $this->users->getById($id);
		$user = $this->users->activateUser($user, $code, Input::except('activation_code'));

		$resource = $this->createItem($user, $this->user_transformer);
		return $this->respondWithSuccess($resource);
	}

	public function requestActivation(){

		$users = $this->users->requestActivations(Input::all());
		$resource = $this->createCollection($users, $this->user_transformer);
		return $this->respondWithSuccess($resource);
	}

	public function resetPassword($id){
		$user = $this->users->getById($id);
		$code = Input::get('password_reset_code');
		$this->users->resetPassword($user, $code, Input::all());

		$resource = $this->createItem($user, $this->user_transformer);
		return $this->respondWithSuccess($resource);
	}

	public function deactivate(){
		$users = $this->users->deactivateByIds(Input::all());
		$resource = $this->createCollection($users, $this->user_transformer);
		return $this->respondWithSuccess($resource);
	}

	public function requestPasswordReset(){
		$email = Input::get('email');
		$user = $this->users->getByEmail($email);
		$this->users->requestPasswordReset($user);

		$resource = $this->createEmptyResource();
		return $this->respondWithSuccess($resource);
	}

	public function importUsers(){

		$file = Input::get('file_id');
		$type = Input::get('type', false);

		if(!$file){
			return $this->respondBadRequest("File required to import");
		}

		if (!$type){
			return $this->respondBadRequest("The type has not been specified");
		}

		try {
			list($errors, $users) = $this->users->importUsersFromFile($file, $type);

		} catch (InvalidInputException $e){
			return $this->respondBadRequest('Data was missing or the file is formatted wrong');
		} catch (IOException $e){
			return $this->respondWithServerError("The server was unable to read the file");
		} catch (Exception $e){
			throw $e;
			return $this->respondWithError("An unexpected error occured");
		}

		$this->manager->parseIncludes('language');
		$resource = $this->createCollection($users, $this->user_transformer);
		if ($errors){
			$resource->setMetaValue('errors', $errors);
		}

		return $this->respondWithSuccess($resource);
	}

	/**
	 * Search the storage in the storage for the term.
	 *
	 * @param  string  $term
	 * @return Response
	 */
	public function search($term)
	{
		$order = Input::get('order', 'first_name');
		$start = Input::get('start', null);
		$stop  = Input::get('stop',  null);
		$users = $this->users->search($term, $start, $stop, $order);

		$resource = $this->createCollection($users, $this->user_transformer, 'users');
		return $this->respondWithSuccess($resource);
	}


}
