<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 16.00
 */

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Models\Company;
use Eloomi\Repositories\FileRepository;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\FileTransformer;
use League\Fractal\Manager;
use Symfony\Component\HttpFoundation\Response;

class FileController extends CrudController {

    /**
     * @var FileRepository
     */
    private $files;
    /**
     * @var Company
     */
    private $company;

    public function __construct(
        FileRepositoryInterface $files,
        Manager $manager,
        ArrayTransformer $array_transformer,
        FileTransformer $fileTransformer,
        Company $company
    ){
        parent::__construct($manager, $array_transformer, $files, $fileTransformer);
        $this->files = $files;
        $this->company = $company;
    }

    public function download($id){
        try {
            $file = $this->files->getFile($id);

            header('Cache-control: private');
            header('Content-Type: '   . $file->getMimeType());
            header('Content-Length: ' .filesize($file->getPathname()));

            @readfile($file->getPathname());
            exit();

        }
        catch(TenantModelNotFoundException $e){
            return Response::create('', Response::HTTP_NOT_FOUND);
        }
        catch(Exception $e){
            throw $e;
        }
    }

    public function show($model){
        TenanctScope::disable();
        if (ctype_digit($model)){
            $model = $this->main_repository->getById($model);
        }
        if ( !is_null($model->company_id) && $this->company->current_company()->id == $model->company_id ){
            throw new TenantModelNotFoundException('No query results for model [Eloomi\Models\File] when scoped by tenant.');
        }

        $resource = $this->createItem($model, $this->main_transformer);
        return $this->respondWithSuccess($resource);
    }

    public function store(){
        $files = Input::file();
        $info = [];
        foreach($files as $file){
            if (!$file->isValid()){
                throw new InvalidInputException("A file wasn't uploaded correctly");
            }
            $model = $this->files->create(['file' => $file]);
            $info[] = [
                'id' => $model->id,
                'path' => \FileController::getUrlForFile($model->id)
            ];
        }

        $resource = $this->createCollection($info, $this->array_transformer);
        return $this->respondWithSuccess($resource);
    }

    public static function getUrlForFile($id){
        if (is_null($id)){
            return null;
        }
        return URL::route(
            'file.download',
            ['file_id' => $id]
        );
    }

}