<?php

use Eloomi\Exceptions\EloomiException;
use Eloomi\Repositories\Interfaces\LMS\ProgressRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\ModuleTransformer;
use League\Fractal\Manager;

class ProgressController extends ApiController {

	/**
	 * @var ProgressRepositoryInterface
	 */
	private $progress;
	/**
	 * @var ModuleTransformer
	 */
	private $moduleTransformer;

	public function __construct(
		ArrayTransformer $null_transformer,
		Manager $manager,
		ProgressRepositoryInterface $progress,
		ModuleTransformer $moduleTransformer,
		UserRepositoryInterface $users
	){
		parent::__construct($manager, $null_transformer);
		$this->progress = $progress;

		$this->moduleTransformer = $moduleTransformer;
		$this->users = $users;
	}

	public function userCompletedModules()
	{
		try {
			$user = $this->users->getCurrentUser();
			$nextModule = $this->progress->updateProgress($user, Input::all());

			if (is_null($nextModule)){
				$resource = $this->createEmptyResource();
			} else {
				$resource = $this->createItem($nextModule, $this->moduleTransformer);
			}

			return $this->respondWithSuccess($resource);
		} catch (EloomiException $e){
			return $this->respondWithEloomiException($e);
		} catch (Exception $e){
			throw $e;
			return $this->respondServerError("An unexpected error occured");
		}
	}

}
