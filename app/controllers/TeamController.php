<?php

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Functionality\Filereaders\CsvIOInterface;
use Eloomi\Repositories\Interfaces\Organization\TeamRepositoryInterface;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\TeamTransformer;
use League\Fractal\Manager;
use Symfony\Component\Filesystem\Exception\IOException;

class TeamController extends \CrudController {

	protected $teams;
	protected $team_transformer;

	public function __construct(
		TeamRepositoryInterface $teams,
		TeamTransformer $team_transformer,
		ArrayTransformer $array_transformer,
		Manager $manager
	){
		parent::__construct($manager, $array_transformer, $teams, $team_transformer);
		$this->teams = $teams;
		$this->team_transformer = $team_transformer;
	}

	/**
	 * Search the storage in the storage for the term.
	 *
	 * @param  string  $term
	 * @return Response
	 */
	public function search($term)
	{
		$order = Input::get('order', 'first_name');
		$start = Input::get('start', null);
		$stop  = Input::get('stop',  null);
		$users = $this->users->search($term, $start, $stop, $order);

		$resource = App::make('Collection', [
			'data' => $users,
			'transformer' => $this->user_transformer,
			'name' => 'users'
		]);

		return $this->respondWithSuccess($resource);
	}

	public function importTeams(){

		$file = Input::file('file');
		$type = Input::get('type', false);

		if(!$file){
			return $this->respondBadRequest("File required to import");
		}

		if (!$file->isValid()){
			return $this->respondBadRequest("File not uploaded correctly");
		}

		if (!$type){
			return $this->respondBadRequest("The type has not been specified");
		}

		try {
			list($errors, $teams) = $this->teams->importTeamsFromFile($file, $type);

		} catch (InvalidInputException $e){
			return $this->respondBadRequest('Data was missing or the file is formatted wrong');
		} catch (IOException $e){
			return $this->respondWithServerError("The server was unable to read the file");
		} catch (Exception $e){
			return $this->respondWithError("An unexpected error occured");
		}

		$this->manager->parseIncludes('users,leader');
		$resource = $this->createCollection($teams, $this->team_transformer);
		if ($errors){
			$resource->setMetaValue('errors', $errors);
		}

		return $this->respondWithSuccess($resource);
	}

	public function importUsers($id){

		$file = Input::file('file');
		$type = Input::get('type', false);
		$team = $this->teams->getById($id);

		if(!$file){
			return $this->respondBadRequest("File required to import");
		}

		if (!$file->isValid()){
			return $this->respondBadRequest("File not uploaded correctly");
		}

		if (!$type){
			return $this->respondBadRequest("The type has not been specified");
		}

		try {
			$team = $this->teams->importUsersFromFile($team, $file, $type);
		}
		catch (InvalidInputException $e) {
			return $this->respondBadRequest('Data was missing or the file is formatted wrong');
		}
		catch (IOException $e) {
			return $this->respondWithServerError("The server was unable to read the file");
		}
		catch(UserNotFoundException $e) {
			return $this->respondBadRequest($e->getMessage());
		}
		catch (Exception $e) {
			return $this->respondWithError("An unexpected error occured");
		}

		$this->manager->parseIncludes('users,leader');
		$resource = $this->createItem($team, $this->team_transformer);

		return $this->respondWithSuccess($resource);

	}


}
