<?php

use Eloomi\Models\Company;
use Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface;
use Eloomi\Transformers\ArrayTransformer;
use Eloomi\Transformers\CompanyTransformer;
use League\Fractal\Manager;

class CompanyController extends \CrudController {

	/**
	 * @var Company
	 */
	private $company;

	public function __construct(
		CompanyRepositoryInterface $companies,
		CompanyTransformer $companyTransformer,
		Manager $manager,
		ArrayTransformer $nullTransformer,
		Company $company
	){
		parent::__construct($manager, $nullTransformer, $companies, $companyTransformer);
		$this->company = $company;
	}

	public function current_company(){
		$company = $this->company->current_company();
		return $this->show($company);
	}
}