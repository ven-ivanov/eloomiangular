<?php

return array(

    'api_key'       => getenv('slideshare_api_key'),
    'shared_secret' => getenv('slideshare_shared_secret'),
    'username'      => getenv('slideshare_username'),
    'password'      => getenv('slideshare_password'),

);
