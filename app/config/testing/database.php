<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 23/12/14
 * Time: 14.04
 */

return array(

    'default' => 'sqlite',

    'connections' => array(
        'sqlite' => array(
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => ''
        ),
    )
);