<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Client Identifier
	|--------------------------------------------------------------------------
	|
	| Your applications client identifier. Used when generating authentication
	| tokens and to receive your authorization code. The client identifier
	|
	*/

	'client_id'     => '1418cc01517ce291b34d2c9f60afe8fef1aa9eae',

	/*
	|--------------------------------------------------------------------------
	| Client Secret
	|--------------------------------------------------------------------------
	|
	| Your applications client secret. Used when generating authentication
	| tokens and to receive your authorization code.
	|
	*/

    'client_secret' => '6c07f9c69f2cdc5cb283d9bcfa957490af385868',
];