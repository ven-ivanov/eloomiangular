<?php

use Carbon\Carbon;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;

class DeleteExpiredCourseCompletions extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'eloomi:expired-courses';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description =
		"Deletes and notifies user about expired courses they have participated in";
	/**
	 * @var UserRepositoryInterface
	 */
	private $users;
	/**
	 * @var CourseRepositoryInterface
	 */
	private $courses;

	/**
	 * Create a new command instance.
	 *
	 * @param UserRepositoryInterface $users
	 * @param CourseRepositoryInterface $courses
	 */
	public function __construct(
		UserRepositoryInterface $users,
		CourseRepositoryInterface $courses
	)
	{
		parent::__construct();
		$this->users = $users;
		$this->courses = $courses;
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler|Schedulable $scheduler
	 * @return Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->daily()->hours(0)->minutes(0);
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$users   = $this->users->index(true);
		$courses = $this->courses->index(true)->load('lasts_for');
		$courses = $courses->filter(function($element){
			return !is_null($element->lasts_for);
		});

		$courses = $courses->map(function($element){
			$now = Carbon::now();
			$valid_for = $element->lasts_for->carbon_string;
			$now->modify('-' . $valid_for);

			return ['id' => $element->id, 'last_valid_at' => $now];
		});
		$courses = $courses->keyBy('id');

		$revokedUsers = [];
		foreach($users as $user){
			$finished_modules = $user->finished_modules()->withPivot('created_at')->get();
			$expiredModules = $finished_modules->filter(function($module) use($courses){
				$course = $courses->get($module->course_id);
				return !is_null($course) &&
				 	   $module->pivot->created_at->diffInDays($course['last_valid_at'], false) > 0 ;
			});

			$expiredCourses = $expiredModules->map(function($element){
				return $element->course_id;
			});

			$expiredCourses = array_unique($expiredCourses);
			$expiredInfo = $expiredCourses->map(function($course_id) use ($courses){
				$course = $courses[$course_id];
				return $course->name;
			});

			Mail::queue('emails.courses.expired', $expiredInfo, function($message) use ($user)
			{
				$message
					->to($user->email, $user->first_name)
					->subject(trans('courses.expired.subject'));
			});

		}
	}

}
