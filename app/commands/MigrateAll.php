<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MigrateAll extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'migrate:all';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Migrates Sentry migrations and then own migrations';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$tables = [];
		DB::statement( 'SET FOREIGN_KEY_CHECKS=0' );
		foreach (DB::select('SHOW TABLES') as $k => $v) {
			$tables[] = array_values((array)$v)[0];
		}

		foreach($tables as $table) {
			Schema::drop($table);
		}

		Artisan::call('migrate', array('--package' => 'cartalyst/sentry'));
		Artisan::call('migrate');

		if ($this->option('seed')){
			Artisan::call('db:seed');
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
//	protected function getArguments()
//	{
//		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
//		);
//	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('seed', null, InputOption::VALUE_NONE, 'Should the database be seeded', null),
		);
	}

}
