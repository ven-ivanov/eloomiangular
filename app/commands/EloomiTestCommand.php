<?php

use Eloomi\Models\OrganizationalUnit;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\Statistics\CourseStatisticsRepositoryInterface;
use Eloomi\Repositories\LMS\CourseShopRepository;
use Eloomi\Repositories\Statistics\CourseStatisticsRepository;
use Illuminate\Console\Command;

class EloomiTestCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'eloomi:test';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'A way to test something fairly easy';
	/**
	 * @var CourseStatisticsRepository
	 */
	private $statistics;
	/**
	 * @var CourseRepositoryInterface
	 */
	private $courses;

	/**
	 * @param CourseStatisticsRepositoryInterface|CourseStatisticsRepository $statistics
	 * @param CourseRepositoryInterface $courses
	 */
	public function __construct(
		CourseStatisticsRepositoryInterface $statistics,
		CourseRepositoryInterface $courses
	){
		parent::__construct();
		$this->statistics = $statistics;
		$this->courses = $courses;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		\TenantScope::addTenant('company_id',1);
		$course = $this->courses->getById(8);
		dd($this->statistics->average_completion($course));
	}

}
