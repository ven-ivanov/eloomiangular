<?php

use Carbon\Carbon;
use Eloomi\Models\OrganizationalUnit;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;

class RebuildOrganizationTrees extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'eloomi:rebuild-units';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description =
		"Rebuilds the organizational trees";

	/**
	 * @var OrganizationalUnit
	 */
	private $organizationalUnit;

	/**
	 * Create a new command instance.
	 *
	 * @param OrganizationalUnit $organizationalUnit
	 */
	public function __construct(
		OrganizationalUnit $organizationalUnit
	)
	{
		parent::__construct();
		$this->organizationalUnit = $organizationalUnit;
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler|Schedulable $scheduler
	 * @return Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->daily()->hours(1)->minutes(0);
	}

	const SIX_HOURS = 12600;

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		\TenantScope::disable();
		$this->organizationalUnit->rebuild();
		\TenantScope::enable();
	}

}