<?php

use Carbon\Carbon;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Indatus\Dispatcher\Scheduling\ScheduledCommand;
use Indatus\Dispatcher\Scheduling\Schedulable;
use Indatus\Dispatcher\Drivers\Cron\Scheduler;

class RemoveUnusedfiles extends ScheduledCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'eloomi:clean-files';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description =
		"Deletes all files not used for anything";
	/**
	 * @var FileRepositoryInterface
	 */
	private $files;

	/**
	 * Create a new command instance.
	 *
	 * @param FileRepositoryInterface $files
	 */
	public function __construct(
		FileRepositoryInterface $files
	)
	{
		parent::__construct();
		$this->files = $files;
	}

	/**
	 * When a command should run
	 *
	 * @param Scheduler|Schedulable $scheduler
	 * @return Schedulable
	 */
	public function schedule(Schedulable $scheduler)
	{
		return $scheduler->daily()->hours(0)->minutes(30);
	}

	const SIX_HOURS = 12600;

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		\TenantScope::disable();
		$files = $this->files->index();
		$now = time();
		$files = $files->filter(function($element) use ($now) {
			return $now - self::SIX_HOURS > $element->created_at->timestamp;
		});

		$files->load([
			'certificates',
			'companies',
			'courses',
			'free_texts',
			'users'
		]);

		foreach($files as $file){
			if ($file->certificates()->count()){
				continue;
			}
			if ($file->companies()->count()){
				continue;
			}
			if ($file->courses()->count()){
				continue;
			}
			if ($file->free_texts()->count()){
				continue;
			}
			var_dump($file->users()->count());
			if ($file->users()->count()){
				continue;
			}

			$this->files->delete($file);
		}
	}

}