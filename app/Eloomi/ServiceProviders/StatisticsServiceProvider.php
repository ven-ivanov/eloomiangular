<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class StatisticsServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\Statistics\CourseStatisticsRepositoryInterface', 'Eloomi\Repositories\Statistics\CourseStatisticsRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\Statistics\StatisticsRepositoryInterface', 'Eloomi\Repositories\Statistics\StatisticsRepository');
	}
}