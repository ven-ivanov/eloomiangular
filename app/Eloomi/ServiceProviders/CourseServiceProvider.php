<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class CourseServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface', 'Eloomi\Repositories\LMS\CourseRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\CourseShopRepositoryInterface', 'Eloomi\Repositories\LMS\CourseShopRepository');
	}
}