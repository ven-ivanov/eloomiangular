<?php namespace Eloomi\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class ApiServiceProvider extends ServiceProvider {

    public function register(){
        $this->app->bind('Collection', function($app, $parameters)
        {
            $data = $parameters['data'];
            $transformer = $parameters['transformer'];
            if (!array_key_exists('name',$parameters)){
                return new Collection($data, $transformer);
            }

            $name = $parameters['name'];
            return new Collection($data, $transformer, $name);
        });

        $this->app->bind('Item', function($app, $parameters)
        {
            $data = $parameters['data'];
            $transformer = $parameters['transformer'];
            if (!array_key_exists('name',$parameters)){
                return new Item($data, $transformer);
            }

            $name = $parameters['name'];
            return new Item($data, $transformer, $name);
        });


    }
}