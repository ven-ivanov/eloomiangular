<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class TeamServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\Organization\TeamRepositoryInterface', 'Eloomi\Repositories\Organization\TeamRepository');
	}
}