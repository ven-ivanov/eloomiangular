<?php namespace Eloomi\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class SessionServiceProvider extends ServiceProvider {

	public function register(){
		$this->app->singleton('Eloomi\Repositories\Interfaces\SessionRepositoryInterface','Eloomi\Repositories\EloquentSessionRepository');
	}
}