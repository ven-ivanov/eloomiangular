<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class TimespanServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\TimespanRepositoryInterface', 'Eloomi\Repositories\TimespanRepository');

	}
}