<?php namespace Eloomi\ServiceProviders;

use Illuminate\Support\ServiceProvider;

class CompanyServiceProvider extends ServiceProvider {

    public function register(){

        $this->app->singleton('Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface', 'Eloomi\Repositories\Organization\CompanyRepository');
    }
}