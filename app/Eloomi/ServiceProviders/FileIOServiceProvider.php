<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class FileIOServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Filereaders\Interfaces\CsvIOInterface', 'Eloomi\Functionality\Filereaders\CsvIO');
	}
}