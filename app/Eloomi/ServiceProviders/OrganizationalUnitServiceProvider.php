<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class OrganizationalUnitServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface', 'Eloomi\Repositories\Organization\OrganizationalUnitRepository');
	}
}