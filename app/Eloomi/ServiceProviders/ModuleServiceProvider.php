<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface', 'Eloomi\Repositories\LMS\ModuleRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\VideoRepositoryInterface', 'Eloomi\Repositories\LMS\VimeoRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\PowerPointRepositoryInterface', 'Eloomi\Repositories\LMS\SlideshareRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\FreeTextRepositoryInterface', 'Eloomi\Repositories\LMS\FreeTextRepository');

		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\SurveyRepositoryInterface', 'Eloomi\Repositories\LMS\SurveyRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\QuestionRepositoryInterface', 'Eloomi\Repositories\LMS\QuestionRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\FreeTextQuestionRepositoryInterface', 'Eloomi\Repositories\LMS\FreeTextQuestionRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\RatingQuestionRepositoryInterface', 'Eloomi\Repositories\LMS\RatingQuestionRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\MultipleChoiceQuestionRepositoryInterface', 'Eloomi\Repositories\LMS\MultipleChoiceQuestionRepository');
		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\MultipleChoiceAnswerRepositoryInterface', 'Eloomi\Repositories\LMS\MultipleChoiceAnswerRepository');
	}
}