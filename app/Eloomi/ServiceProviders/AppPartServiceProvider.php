<?php namespace Eloomi\ServiceProviders;

use Illuminate\Support\ServiceProvider;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class AppPartServiceProvider extends ServiceProvider {

    public function register(){

        $this->app->singleton('Eloomi\Repositories\Interfaces\AppPartRepositoryInterface', 'Eloomi\Repositories\AppPartRepository');

    }
}