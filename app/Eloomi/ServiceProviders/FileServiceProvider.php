<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\FileRepositoryInterface', 'Eloomi\Repositories\FileRepository');

	}
}