<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface', 'Eloomi\Repositories\Organization\UserRepository');
	}
}