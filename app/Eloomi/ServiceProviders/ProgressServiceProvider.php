<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class ProgressServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\ProgressRepositoryInterface', 'Eloomi\Repositories\LMS\ProgressRepository');
	}
}