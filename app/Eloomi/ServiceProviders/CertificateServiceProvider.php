<?php namespace Eloomi\ServiceProviders;

use Eloomi\Models\User;
use Illuminate\Support\ServiceProvider;

class CertificateServiceProvider extends ServiceProvider {

	public function register(){

		$this->app->singleton('Eloomi\Repositories\Interfaces\LMS\CertificateRepositoryInterface', 'Eloomi\Repositories\LMS\CertificateRepository');

	}
}