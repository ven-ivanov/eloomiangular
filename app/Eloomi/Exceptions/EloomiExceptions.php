<?php namespace Eloomi\Exceptions;
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/12/14
 * Time: 14.16
 */
class EloomiException extends \Exception { }

class AccessViolationException extends EloomiException {}
class MissingInputException extends EloomiException {}
class InvalidInputException extends EloomiException {}

class FileReaderException extends EloomiException{

    protected $line_content;

    public function __construct($msg, $line_content){
        parent::__construct($msg);
        $this->line_content = $line_content;
    }

    public function getLineContent(){
        return $this->line_content;
    }
}