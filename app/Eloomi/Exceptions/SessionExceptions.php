<?php namespace Eloomi\Exceptions\SessionExceptions;

use Eloomi\Exceptions\EloomiException;
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/12/14
 * Time: 14.16
 */

class SessionException extends EloomiException {}
class MissingInputException extends SessionException { }
class WrongInputException extends SessionException { }
class UserNotActivatedException extends SessionException { }
class NotAllowedException extends SessionException { }
