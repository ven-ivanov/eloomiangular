<?php namespace Eloomi\Exceptions\UserExceptions;


use Eloomi\Exceptions\EloomiException;
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/12/14
 * Time: 14.16
 */
class UserException extends EloomiException {}


class ValidationException extends UserException {

    protected $errors;

    public function __construct($msg, Array $errors){
        parent::__construct($msg);
        $this->errors = $errors;
    }

    public function getErrors(){
        return $this->errors;
    }
}


class MissingInputException  extends UserException { }
class DoNotExistException    extends UserException { }
class AlreadyExistsException extends UserException { }
