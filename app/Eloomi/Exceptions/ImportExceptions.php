<?php namespace Eloomi\Exceptions\ImportExceptions;


use Eloomi\Exceptions\EloomiException;
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/12/14
 * Time: 14.16
 */
class ImportException extends EloomiException {}