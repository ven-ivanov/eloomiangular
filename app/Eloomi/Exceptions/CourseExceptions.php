<?php namespace Eloomi\Exceptions\UserExceptions;


use Eloomi\Exceptions\EloomiException;
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/12/14
 * Time: 14.16
 */
class CourseException extends EloomiException {}


class WrongAnswersException  extends CourseException { }
class NotQualifiedException  extends CourseException { }
