<?php namespace Eloomi\Data;

/**
* Permission
*/

class Permissions {

    // Group names
    const CROSS_COMPANY_ADMIN = "SalesForce1";
    const COMPANY_ADMIN = "Company administrator";
    const COMPANY_USER  = "User";


    // Company permissions
    CONST CREATE_COMPANY = "company.create";
    CONST READ_COMPANY   = "company.read";
    CONST UPDATE_COMPANY = "company.update";
    CONST DELETE_COMPANY = "company.delete";

    // User permissions
    CONST CREATE_USER = "user.create";
    CONST READ_USER   = "user.read";
    CONST UPDATE_USER = "user.update";
    CONST UPDATE_USER_CRITICAL = "user.update.critical";
    CONST DELETE_USER = "user.delete";

    // Organizational Unit permissions
    CONST CREATE_UNIT = "unit.create";
    CONST READ_UNIT   = "unit.read";
    CONST UPDATE_UNIT = "unit.update";
    CONST DELETE_UNIT = "unit.delete";

    // Team permissions
    CONST CREATE_TEAM = "team.create";
    CONST READ_TEAM   = "team.read";
    CONST UPDATE_TEAM = "team.update";
    CONST DELETE_TEAM = "team.delete";

    // Course permissions
    CONST CREATE_COURSE = "course.create";
    CONST READ_COURSE   = "course.read";
    CONST UPDATE_COURSE = "course.update";
    CONST DELETE_COURSE = "course.delete";

}