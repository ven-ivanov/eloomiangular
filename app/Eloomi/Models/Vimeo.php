<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloomi\Models\Interfaces\ModuleInterface;
use Eloquent;
use App;

class Vimeo extends Eloquent implements ModuleInterface {

//    use TenantScopedModelTrait;

    protected $table = 'vimeo_videos';

    protected $fillable = [''];

    public function module(){
        return $this->morphOne('Eloomi\Models\Module', 'teachable');
    }

    public static function getType(){
        return "vimeo";
    }

    public function getTypeSpecifics()
    {
        return ['path' => $this->path];
    }

    public function getTransformer(){
        return App::make('Eloomi\Transformers\VimeoTransformer');
    }

    public function isReady(){
        return $this->ready;
    }

    public function company(){
        return $this->belongsTo('Eloomi\Models\Company', 'company_id');
    }

}