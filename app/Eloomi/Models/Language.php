<?php namespace Eloomi\Models;

use Eloquent;
/**
* Language
*/
class Language extends Eloquent {

    public $timestamps = false;
    public $fillable = ['name', 'code'];

}
