<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloquent;
use App;

class Module extends Eloquent {

    use TenantScopedModelTrait;

    protected $fillable = [
        'name',
        'description',
        'is_skippable'
    ];

    public function getType()
    {
        $teachable = $this->teachable;
        return $teachable->getType();
    }

    public function user_progress(){
        return $this->belongsToMany('Eloomi\Models\User')->withTimestamps();
    }

    public function SetIsSkippableAttribute($value){
        $this->attributes['is_skippable'] = (boolean) $value;
    }

    public function teachable()
    {
        return $this->morphTo("teachable");
    }

    public function course(){
        return $this->belongsTo('Eloomi\Models\Course');
    }

    public static function getById($id)
    {
        return self::findOrFail($id);
    }

}