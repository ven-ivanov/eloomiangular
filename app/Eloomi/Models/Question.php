<?php namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloquent;

class Question extends Eloquent {

    use TenantScopedModelTrait;

    protected $fillable = ['question'];

    public function questionable()
    {
        return $this->morphTo('questionable');
    }

    public function survey(){
        return $this->belongsTo('Eloomi\Models\Survey');
    }

    public function getTypeSpecifics()
    {
        return $this->questionable->getTypeSpecifics();
    }

    public function getType()
    {
        return $this->questionable->getType();
    }
}