<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloomi\Models\Interfaces\SurveyQuestionInterface;
use Eloquent;
use App;

class MultipleChoiceQuestion extends Eloquent implements SurveyQuestionInterface {

//    use TenantScopedModelTrait;

    protected $table = 'survey_multiple_choice';
    protected $fillable = [''];

    public function module(){
        return $this->morphOne('Eloomi\Models\Survey', 'questionable');
    }

    public static function getType(){
        return "multiple_choice";
    }

    public function getTypeSpecifics()
    {
        $answers = $this->answers->toArray();
        $answers = array_map(function($answer){
            return [
                'id'      => $answer->id,
                 'answer' => $answer->answer
            ];
        }, $answers);

        return [
            'answers'  => $answers
        ];
    }

    public function getTransformer(){
        return \App::make('Eloomi\Transformers\MultipleChoiceQuestionTransformer');
    }

    public function answers(){
        return $this->hasMany('Eloomi\Models\MultipleChoiceAnswer','question_id');
    }

    public function company(){
        return $this->belongsTo('Eloomi\Models\Company', 'company_id');
    }

    public static function getById($id)
    {
        return self::findOrFail($id);
    }

//    public static function boot()
//    {
//        parent::boot();
//        self::bootTenantScopedModelTrait();
//    }
}