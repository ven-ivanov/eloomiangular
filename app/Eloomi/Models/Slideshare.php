<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloomi\Models\Interfaces\ModuleInterface;
use Eloquent;
use App;

class Slideshare extends Eloquent implements ModuleInterface {

    protected $table = 'slideshare_presentations';

    protected $fillable = [''];

    public function module(){
        return $this->morphOne('Eloomi\Models\Module', 'teachable');
    }

    public static function getType(){
        return "slideshare";
    }

    public function getTypeSpecifics()
    {
        return ['path' => $this->path];
    }

    public function getTransformer(){
        return App::make('Eloomi\Transformers\SlideshareTransformer');
    }

    public function company(){
        return $this->belongsTo('Eloomi\Models\Company', 'company_id');
    }

}