<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloomi\Models\Interfaces\ModuleInterface;
use Eloquent;
use App;

class FreeText extends Eloquent implements ModuleInterface {

//    use TenantScopedModelTrait;

    protected $table = 'free_text_modules';

    protected $fillable = ['text'];

    public function module(){
        return $this->morphOne('Eloomi\Models\Module', 'teachable');
    }

    public static function getType(){
        return "free_text";
    }

    public function getTypeSpecifics()
    {
        return [
            'text' => $this->text,
            'image' => \FileController::getUrlForFile($this->image_id)

        ];
    }

    public function getTransformer(){
        return App::make('Eloomi\Transformers\FreeTextTransformer');
    }

    public function isReady(){
        return true;
    }

    public function company(){
        return $this->belongsTo('Eloomi\Models\Company', 'company_id');
    }

    public function image(){
        return $this->belongsTo('Eloomi\Models\File');
    }

}