<?php namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Cartalyst\Sentry\Hashing\NativeHasher;
use Eloomi\Traits\Getter;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Cartalyst\Sentry\Users\Eloquent\User as SentryUserModel;
use Eloquent;
use \App;
use Illuminate\Database\Eloquent\ModelNotFoundException;

//use Cartalyst\Sentry\Hashing\NativeHasher;


class User extends SentryUserModel {

	use TenantScopedModelTrait, SoftDeletingTrait, Getter;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	protected $fillable = ['first_name', 'last_name', 'email', 'birthday', 'gender', 'title', 'phone', 'timezone', 'password', 'reference'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token', 'confirmation_code', 'remember_token');

	public function company(){
		return $this->belongsTo('Eloomi\Models\Company', 'company_id');
	}

    public function organizational_unit() {
		$this->load('organizational_units');
        return $this->organizational_units()->first();
    }

	public function organizational_units() {
		return $this->belongsToMany('Eloomi\Models\OrganizationalUnit');
	}

	public function trainees() {
		return $this->belongsToMany('Eloomi\Models\User','coach_trainee', 'trainee_id','coach_id');
	}

	public function coaches() {
		return $this->belongsToMany('Eloomi\Models\User','coach_trainee','coach_id', 'trainee_id');
	}

	public function activeState(){
		if ($this->isActivated()) { return "active"; }

		if ( is_null($this->activation_code) ) { return "inactive"; }

		return "pending";
	}


	public function language(){
		return $this->belongsTo('Eloomi\Models\Language');
	}

	public function name(){
		return $this->first_name . ' ' . $this->last_name;
	}

	public function teams()
	{
		return $this->belongsToMany('Eloomi\Models\Team');
	}

	public function courses()
	{
		return $this->morphToMany('Eloomi\Models\Course', 'participant');
	}

	public function image(){
		return $this->belongsTo('Eloomi\Models\File');
	}

	public function module_progress(){
		return $this->belongsToMany('Eloomi\Models\Module')->withTimestamps();
	}

	public function getDates(){
		return [
			'created_at',
			'updated_at',
			'deleted_at',
			'birthday'
		];
	}

	public static function getById($id)
	{
		return static::getOneByAttribute('id', $id);
	}

	public static function getByReference($reference){
		return static::getOneByAttribute('reference', $reference);
	}

	public function getByIds(array $ids)
	{
		return self::getManyByAttribute('id', $ids);
	}

	public static function getByEmail($email)
	{
		return self::getOneByAttribute('email', $email);
	}

	public static function getByEmails(array $emails){
		return self::getManyByAttribute('email', $emails);
	}

	public static function getByReferences(array $references){
		return self::getManyByAttribute('reference', $references);
	}

    public static function boot()
    {
		parent::boot();

		static::bootTenantScopedModelTrait();
		static::setHasher(new NativeHasher);
		User::observe(App::make('Eloomi\Observers\UserObserver'));
    }
}
