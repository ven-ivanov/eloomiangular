<?php namespace Eloomi\Models;

use Eloomi\Exceptions\InvalidInputException;
use TenantScope;
use Eloquent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Config;
/**
* Company
*/
class Company extends Eloquent {

    private static $cache = null;
    private static $with_cache  = [];

    use SoftDeletingTrait;
    /**
    * Table name.
    *
    * @var string
    */
    protected $table = 'companies';
    protected $fillable = [
        'name',
        'domain',
        'color',
        'address',
        'zip',
        'city',
        'cvr',
        'phone',
        'homepage',
        'contact_name'
    ];

    public function users(){
      return $this->hasMany('Eloomi\Models\User');
    }

    public function organizational_unit(){
      return $this->belongsTo('Eloomi\Models\OrganizationalUnit');
    }

    public function bought_courses(){
        return $this->belongsToMany('Eloomi\Models\Course', 'bought_courses', 'id', 'original_id');
    }


    public function language(){
        return $this->belongsTo('Eloomi\Models\Language');
    }

    public function logo(){
        return $this->belongsTo('Eloomi\Models\File', 'image_id');
    }

    public function app_parts(){
        return $this->belongsToMany('Eloomi\Models\AppPart')->withTimestamps();
    }

    public function setDomainAttribute($subdomain){
        $base_domain = "." . Config::get('eloomi.base_domain');
        $subdomain = str_replace($base_domain, '', $subdomain); // removes all parts of the base domain, if present
        if (substr_count($subdomain, ".")){
            throw new InvalidInputException("domain must not nest domain more than 1 after");
        }
        $this->attributes['domain'] = $subdomain . $base_domain;
    }

    public static function current_company($with = []){

        if (is_null(static::$cache)){
            $company_id = TenantScope::getTenantId('company_id');
            self::$cache = static::find($company_id);
        }

        $missing = array_diff($with, self::$with_cache);
        self::$cache->load($missing);
        self::$with_cache = array_merge(self::$with_cache, $missing);

        return self::$cache;
    }

}
