<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloquent;
use App;

class AppPart extends Eloquent {

    protected $table = 'app_parts';
    public $timestamps = false;


    protected $fillable = ['name'];

    public function course(){
        return $this->hasOne('Eloomi\Models\Course');
    }

    public function companies(){
        return $this->belongsToMany('Eloomi\Models\Company');
    }

}