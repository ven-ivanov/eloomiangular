<?php namespace Eloomi\Models;

use Eloquent;
use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;

/**
* File
*/
class File extends Eloquent {

    use TenantScopedModelTrait;
    protected $fillable = [''];

    public function certificates(){
        return $this->hasMany('Eloomi\Models\Certificate', 'file_id');
    }

    public function companies(){
        return $this->hasMany('Eloomi\Models\Company', 'image_id');
    }

    public function courses(){
        return $this->hasMany('Eloomi\Models\Course', 'image_id');
    }

    public function free_texts(){
        return $this->hasMany('Eloomi\Models\FreeText', 'image_id');
    }

    public function users(){
        return $this->hasMany('Eloomi\Models\User', 'image_id');
    }

}
