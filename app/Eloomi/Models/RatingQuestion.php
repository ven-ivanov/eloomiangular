<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloomi\Models\Interfaces\SurveyQuestionInterface;
use Eloquent;
use App;

class RatingQuestion extends Eloquent implements SurveyQuestionInterface {

    protected $table = 'survey_rating';

    protected $fillable = ['min', 'max'];

    public function module(){
        return $this->morphOne('Eloomi\Models\Survey', 'questionable');
    }

    public static function getType(){
        return "rating";
    }

    public function getTypeSpecifics()
    {
        return [
            'min' => $this->min,
            'max' => $this->max
        ];
    }

    public function company(){
        return $this->belongsTo('Eloomi\Models\Company', 'company_id');
    }

    public function getTransformer(){
        return \App::make('Eloomi\Transformers\RatingTransformer');
    }

}