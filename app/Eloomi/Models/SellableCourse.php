<?php namespace Eloomi\Models;

use Eloquent;
use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;

/**
* File
*/
class SellableCourse extends Eloquent {

    protected $fillable = [
        'price',
    ];

    public function course()
    {
        return $this->belongsTo('Eloomi\Models\Course');
    }
}
