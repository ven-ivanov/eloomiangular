<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloomi\Models\Interfaces\SurveyQuestionInterface;
use Eloquent;
use App;

class FreeTextQuestion extends Eloquent implements SurveyQuestionInterface {

//    use TenantScopedModelTrait;

    protected $table = 'survey_free_text';

    protected $fillable = [''];

    public function survey(){
        return $this->morphOne('Eloomi\Models\Survey', 'questionable');
    }

    public static function getType(){
        return "free_text_question";
    }

    public function getTypeSpecifics()
    {
        return [];
    }

    public function getTransformer(){
        return \App::make('Eloomi\Transformers\FreeTextQuestionTransformer');
    }

    public function company(){
        return $this->belongsTo('Eloomi\Models\Company', 'company_id');
    }

//    public static function boot()
//    {
//        parent::boot();
//        self::bootTenantScopedModelTrait();
//    }
}