<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloquent;
use App;

class Certificate extends Eloquent {

    use TenantScopedModelTrait;

    protected $fillable = ['description'];

    public function course(){
        return $this->hasOne('Eloomi\Models\Course');
    }

    public function company(){
        return $this->belongsTo('Eloomi\Models\Company', 'company_id');
    }

    public function file(){
        return $this->belongsTo('Eloomi\Models\File', 'file_id');
    }

}