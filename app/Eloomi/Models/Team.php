<?php namespace Eloomi\Models;

use Eloquent;
use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
/**
* Team
*/
class Team extends Eloquent {

    use TenantScopedModelTrait, SoftDeletingTrait;

    protected $fillable = ['name', 'description'];

    public function users()
    {
        return $this->belongsToMany('Eloomi\Models\User');
    }

    public function leader()
    {
        return $this->belongsTo('Eloomi\Models\User');
    }

    public function courses()
    {
        return $this->morphToMany('Eloomi\Models\Course', 'participant');
    }
}
