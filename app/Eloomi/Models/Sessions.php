<?php namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;

class Session extends Eloquent {

	use TenantScopedModelTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'sessions';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public function users(){
		return $this->hasMany('Eloomi\Models\User');
	}
}