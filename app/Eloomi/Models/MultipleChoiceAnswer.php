<?php

namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloquent;
use App;

class MultipleChoiceAnswer extends Eloquent {

    use TenantScopedModelTrait;

    protected $table = 'survey_multiple_choice_answers';
    protected $fillable = ['answer','is_correct'];

    public function question(){
        return $this->belongsTo('Eloomi\Models\MultipleChoiceQuestion', 'question_id');
    }

    public function company(){
        return $this->belongsTo('Eloomi\Models\Company', 'company_id');
    }

    public static function getById($id)
    {
        return static::findOrFail($id);
    }

}