<?php namespace Eloomi\Models;

use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;
use Eloomi\Models\Interfaces\ModuleInterface;
use Eloquent;

class Survey extends Eloquent implements ModuleInterface {

//	use TenantScopedModelTrait;

	protected $table = 'surveys';

	protected $fillable = ['pass_percentage'];

	public function module(){
		return $this->morphOne('Eloomi\Models\Module', 'teachable');
	}

	public static function getType(){
		return "survey";
	}

	public function getTypeSpecifics()
	{
		$this->load('questions');
		$questions = $this->questions->each(
			function($element){ return $element->getTypeSpecifics(); }
		);

		return [
			'pass_percentage' => $this->pass_percentage,
			'questions'    => $questions
		];
	}

	public function getTransformer(){
		return \App::make('Eloomi\Transformers\SurveyTransformer');
	}

	public function isReady(){
		return count($this->questions()) > 0;
	}

	public function questions(){
		return $this->hasMany('Eloomi\Models\Question');
	}

}