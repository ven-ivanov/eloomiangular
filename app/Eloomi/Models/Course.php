<?php namespace Eloomi\Models;

use Eloquent;
use AuraIsHere\LaravelMultiTenant\Traits\TenantScopedModelTrait;

/**
* File
*/
class Course extends Eloquent {

    use TenantScopedModelTrait;

    protected $fillable = [
        'name',
        'code',
        'description',
        'active',
        'in_order',
        'due_date',
        'points',
    ];

    public function users()
    {
        return $this->morphedByMany('Eloomi\Models\User', 'participant');
    }

    public function image(){
        return $this->belongsTo('Eloomi\Models\File', 'image_id');
    }

    public function copied_from(){
        return $this->belongsToMany('Eloomi\Models\Course', 'bought_courses', 'copy_id', 'original_id')->withTimestamps();
    }

    public function copies(){
        return $this->belongsToMany('Eloomi\Models\Course', 'bought_courses', 'original_id', 'copy_id')->withTimestamps();
    }

    public function organizational_units()
    {
        return $this->morphedByMany('Eloomi\Models\OrganizationalUnit', 'participant');
    }

    public function companies(){
        return $this->belongsToMany('Eloomi\Models\Company', 'bought_courses', 'company_id');
    }

    public function teams()
    {
        return $this->morphedByMany('Eloomi\Models\Team', 'participant');
    }

    public function lasts_for()
    {
        return $this->belongsTo('Eloomi\Models\Timespan', 'valid_for');
    }

    public function getDates()
    {
        return array('created_at', 'updated_at', 'deleted_at', 'due_date');
    }

    public function modules(){
        return $this->hasMany('Eloomi\Models\Module');
    }

    public function certificate(){
        return $this->belongsTo('Eloomi\Models\Certificate');
    }

    public static function getById($id)
    {
        return self::findOrFail($id);
    }

}
