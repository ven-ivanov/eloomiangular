<?php namespace Eloomi\Models;

use Eloquent;
/**
* Timespan
*/
class Timespan extends Eloquent {

    public $timestamps = false;
}
