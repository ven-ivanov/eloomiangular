<?php namespace Eloomi\Models\Interfaces;

interface TypeInterface {

    public function getTypeSpecifics();

    public static function getType();
}