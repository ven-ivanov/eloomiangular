<?php namespace Eloomi\Models\Interfaces;

interface SurveyQuestionInterface extends TypeInterface {

    public function getTransformer();
}