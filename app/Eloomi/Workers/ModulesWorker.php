<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 12/01/15
 * Time: 13.23
 */

namespace Eloomi\Workers;

use TenantScope;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Vimeo as VimeoFacade;
use Queue;
use Mail;


class ModulesWorker {

    /**
     * @var UserRepositoryInterface
     */
    protected $users;
    /**
     * @var ModuleRepositoryInterface
     */
    protected $modules;

    public function __construct(
        UserRepositoryInterface $users,
        ModuleRepositoryInterface $modules
    ){
        $this->users = $users;
        $this->$modules = $modules;
    }

    public function retakeModule($job, $data){
        TenantScope::addTenant('company_id', $data['company_id']);

        $module = $this->modules->getById($data['module_id']);

        // TODO: Notify all users who has completed the module

        // TODO: Invalidate all users who has taken the course

        $job->delete();
    }

}