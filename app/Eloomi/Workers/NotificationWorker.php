<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 12/01/15
 * Time: 13.23
 */

namespace Eloomi\Workers;

use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Vimeo as VimeoFacade;


class NotificationWorker {

    /**
     * @var ModuleRepositoryInterface
     */
    private $modules;
    /**
     * @var UserRepositoryInterface
     */
    private $users;

    public function __construct(
        ModuleRepositoryInterface $modules,
        UserRepositoryInterface $users
    ){

        $this->modules = $modules;
        $this->users = $users;
    }

    public function courseUpdated($job, $data){
        $module_name = $this->modules->getById($data['module_id'])->name;
        $users = $this->users->getByIds($data['user_ids']);

        $data = ['module_name' => $module_name];
        foreach( $users as $user ) {
            Mail::queue('emails.course.invalidated', array_merge($data, ['first_name' => $user->first_name]), function ($message) use ($user) {
                $message
                    ->to($user->email, $user->name())
                    ->subject(trans('email.courses.invalid'));
            });
        }

        $job->delete();
    }
}