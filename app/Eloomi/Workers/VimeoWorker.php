<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 12/01/15
 * Time: 13.23
 */

namespace Eloomi\Workers;

use Eloomi\Models\Company;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use TenantScope;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\VideoRepositoryInterface;
use Vimeo as VimeoFacade;
use Eloomi\Models\Vimeo as VimeoModel;
use Carbon\Carbon;
use Queue;
use Mail;
use Config;


class VimeoWorker {

    /**
     * @var UserRepositoryInterface
     */
    protected $users;
    /**
     * @var VimeoRepositoryInterface
     */
    protected $vimeo;
    /**
     * @var FileRepositoryInterface
     */
    private $company;

    public function __construct(
        UserRepositoryInterface $users,
        VideoRepositoryInterface $vimeo,
        FileRepositoryInterface $files
    ){
        VimeoFacade::setToken(Config::get('vimeo.token'));
        $this->users = $users;
        $this->vimeo = $vimeo;
        $this->files = $files;
    }

    const NOW = 0;
    const MINUTE = 60;
    const TWELVE_HOURS = 43200;
    //const TEN_MINUTES = 600;           //      10 * 60
    const TEN_MINUTES = 30;           //      10 * 60
    const RUNS_IN_TWELVE_HOURS = 72;   // 12 * 60 * 60 / ( 10 * 60 )

    public function upload($job, $data){
        TenantScope::addTenant('company_id', $data['company_id']);

        $local_path = $data['video_path'];

        // The whole reason we are doing this async
        \Log::info("Now we are about to upload", ['jobid' =>$job->getJobId(), 'path' => $local_path] );
        $vimeoPath = VimeoFacade::upload($local_path);

        $video_model_id = $data['model_id'];
        $model = $this->vimeo->getById($video_model_id);

        $model->path = $vimeoPath;
        $model->save();

        // TODO: Check if path exists, and if so check if the video on vimeo should be deleted.

        $this->resetVideoInfo($model);

        $date = Carbon::now()->addSeconds(self::TEN_MINUTES);
        \Log::info("Queueing stuff for statuscheck", ['jobid' =>$job->getJobId()] );
        Queue::later($date, 'Eloomi\Workers\VimeoWorker@checkStatus', $data);

        $job->delete();
    }

    public function changeVideo($job, $data){
        TenantScope::addTenant('company_id', $data['company_id']);


        $model_id = $data['model_id'];
        $model = $this->vimeo->getById($model_id);

        $local_path = $data['video_path'];

        VimeoFacade::replace($model->path, $local_path);
        unlink($local_path);

        $date = Carbon::now()->addSeconds(self::TEN_MINUTES);
        Queue::later($date, 'Eloomi\Workers\VimeoWorker@checkStatus', $data);

        $job->delete();
    }

    protected function resetVideoInfo(VimeoModel $videoModel){

        VimeoFacade::request($videoModel->path, [
            'privacy' => [
                'view' => 'disable',
                'embed' => 'whitelist'
            ],
            'review_link' => false
        ], 'PATCH');

        $this->resetEmbedDomains($videoModel);
        $this->addEmbedDomain($videoModel);
    }

    /**
     * @param VimeoModel $video
     * @param array $domains
     */
    protected function addEmbedDomain(VimeoModel $video){
        $embed_url = $video->path . '/privacy/domains/eloomi.com';
        VimeoFacade::request($embed_url, [], 'PUT');
    }

    protected function resetEmbedDomains(VimeoModel $video){
        $fetchUrl = $this->createDomainPrivacyUrl($video);
        while($fetchUrl){
            $response = VimeoFacade::request($fetchUrl, [], 'GET');
            $response = $response['body'];
            foreach($response['data'] as $domain){
                VimeoFacade::request($domain['uri'], [], 'DELETE');
            }

            $fetchUrl = $response['paging']['next'];
        }
    }

    protected function createDomainPrivacyUrl(VimeoModel $videoModel, $domain = null){
        $url = $videoModel->path . "/privacy/domains";
        if ($domain){
            $url .= '/' . $domain;
        }
        return $url;
    }

    public function checkStatus($job, $data)
    {
        TenantScope::addTenant('company_id', $data['company_id']);

        $model_id = $data['model_id'];
        $model = $this->vimeo->getById($model_id);
        if (!$this->IsVideoReady($model)){
            \Log::info("Video isn't ready yet", ['jobid' =>$job->getJobId()] );

            if ($job->attempts() > self::RUNS_IN_TWELVE_HOURS){
                Log::error('Video not ready after 12 hours', $data);
            } else {
                \Log::info("Releasing the job onto the queue", ['jobid' =>$job->getJobId()] );
                $job->release(self::TEN_MINUTES);
                \Log::info("We just released it onto the queue, but we keep going", ['jobid' =>$job->getJobId()] );
            }
            return;
        }

        $this->vimeo->markAsReady($model);

        $user_id = $data['user_id'];
        $user = $this->users->getById($user_id);

        $email = $user->email;
        $name  = $user->first_name;

        Mail::queue('emails.video.ready', $data, function($message) use ($email, $name)
        {
            $message
                ->to($email, $name)
                ->subject(trans('video.ready.subject'));
        });

        $job->delete();
    }

    public function delete($job, $data)
    {
        TenantScope::addTenant('company_id', $data['company_id']);
        $vimeo_path = $data['video_path'];
        VimeoFacade::request($vimeo_path, [], 'DELETE');
        $job->delete();
    }

    protected function isVideoReady(VimeoModel $video){
        $response = VimeoFacade::request($video->path, [], 'GET');
        if (!isset($response['body']['status'])){
            throw new \Exception('Asking for video that does not exist');
        }

        return $response['body']['status'] == 'available';
    }

}