<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 12/01/15
 * Time: 13.23
 */

namespace Eloomi\Workers;

use Eloomi\Functionality\SlideShare;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\PowerpointRepositoryInterface;
use TenantScope;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Vimeo as VimeoFacade;
use Queue;
use Mail;


class SlideshareWorker {

    protected $users;
    protected $slide;
    /**
     * @var SlideShare
     */
    private $api;
    /**
     * @var PowerPointRepositoryInterface
     */
    private $model;
    /**
     * @var FileRepositoryInterface
     */
    private $files;
    /**
     * @var SlideshareRepository
     */
    private $slideshare;

    public function __construct(
        SlideShare $api,
        UserRepositoryInterface $users,
        PowerPointRepositoryInterface $slideshare,
        FileRepositoryInterface $files
    ){
        $this->api = $api;
        $this->users = $users;
        $this->files = $files;
        $this->slideshare = $slideshare;
    }

    public function upload($job, $data){
        TenantScope::addTenant('company_id', $data['company_id']);
        $file = $this->files->getById($data['presentation_id']);
        $local_path = $this->files->getFilePath($file);

        // The whole reason we are doing this async
        \Log::info("Now we are about to upload", ['jobid' =>$job->getJobId(), 'path' => $local_path] );
        $slideshare_id = $this->api->upload($local_path);
        $this->files->delete($file);

        $model = $this->slideshare->getById($data['model_id']);
        $model->slideshare_id = $slideshare_id;

        $model->save();

        $job->delete();
    }


    public function delete($job, $data)
    {
        $this->api->delete($data['slideshare_id']);
        $job->delete();
    }

}