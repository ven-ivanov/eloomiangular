<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 11/02/15
 * Time: 10.56
 */

namespace Eloomi\Traits;


trait Getter {

    protected static function getManyByAttribute($attribute, array $values){
        $users = static::whereIn($attribute, $values)->get();
        if (count($values) != count($users)){
            throw new TenantModelNotFoundException("One or more {$attribute}s are not in the system");
        }

        return $users;
    }

    protected static function getOneByAttribute($attribute, $value)
    {
        $user = User::where($attribute, $value)->first();
        if (!$user){
            throw new TenantModelNotFoundException("No user was found with the $attribute ". $value);
        }

        return $user;
    }
}