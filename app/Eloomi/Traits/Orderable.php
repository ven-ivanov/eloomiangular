<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 22/12/14
 * Time: 12.21
 */

namespace Eloomi\Traits;

trait Orderable {

    protected function changeOrder($model, $newOrder){
        $currentOrder = $model->order;
        if ($currentOrder < $newOrder){
            $this->getModel()->whereBetween('order', [$currentOrder+1, $newOrder])->decrement('order');
        } else {
            $this->getModel()->whereBetween('order', [$newOrder, $currentOrder-1])->increment('order');
        }

        $model->order = $newOrder;
        return $model;
    }
}