<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 24/01/15
 * Time: 12.37
 */

namespace Eloomi\Traits;


use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Repositories\Interfaces\TypeInterface;
use ErrorException;

trait MultiRepository {

    /*
     * @var TypeInterface[]
     */
    protected $repositories = [];

    /**
     * @param string $type
     * @return mixed
     * @throws InvalidInputException
     * @throws ErrorException
     */
    protected function getRepository($type)
    {
        if (!isset($this->repositories) || !is_array($this->repositories) || count($this->repositories) == 0){
            throw new ErrorException('repositories are not set or set incorrectly');
        }

        foreach($this->repositories as $repo){
            if ($repo->getType() == $type){
                return $repo;
            }
        }

        throw new InvalidInputException('The given type is not supported');
    }
}