<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 22/12/14
 * Time: 12.21
 */

namespace Eloomi\Traits;

use Eloomi\Exceptions\InvalidInputException;

trait FileReader {

    protected function matchFileReaderType($type){
        foreach($this->fileReaders as $fileReader){
            if($fileReader->getType() == $type){
                return $fileReader;
            }
        }

        throw new InvalidInputException('Type is not supported');
    }
}