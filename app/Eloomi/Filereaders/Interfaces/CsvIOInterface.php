<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 17/01/15
 * Time: 14.04
 */

namespace Eloomi\Filereaders\Interfaces;


interface CsvIOInterface extends FileIOInterface{

    public function setDelimiter($delimiter);

    public function setEnclosure($enclosure);
}