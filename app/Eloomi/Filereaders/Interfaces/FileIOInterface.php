<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 29/12/14
 * Time: 10.01
 */

namespace Eloomi\Filereaders\Interfaces;

interface FileIOInterface {

    public function fileToArray($filepath);

    public function arrayToFile($array, $filepath, $headers = null);

    public function getType();
}