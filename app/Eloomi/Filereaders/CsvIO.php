<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 29/12/14
 * Time: 10.10
 */

namespace Eloomi\Functionality\Filereaders;


use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Filereaders\Interfaces\CsvIOInterface;
use Symfony\Component\Filesystem\Exception\IOException;

class CsvIO implements CsvIOInterface {

    protected $delimiter = ";";
    protected $enclosure = '"';
    protected $type = 'csv';

    public function fileToArray($filepath)
    {
        if (($file = fopen($filepath, "r")) === FALSE){
            throw new IOException('Unable to open file');
        }

        $all_rows = array();
        $keys = fgetcsv($file, 0, $this->delimiter, $this->enclosure);
        while ($row = fgetcsv($file, 0, $this->delimiter, $this->enclosure)) {
            $new_row = @array_combine($keys, $row);
            if (!$new_row){
                fclose($file);
                throw new InvalidInputException('Mismatch between number of headers and values');
            }
            $all_rows[] = $new_row;
        }
        fclose($file);

        return $all_rows;
    }

    public function setDelimiter($delimiter){
        $this->delimiter = $delimiter;

        return $this;
    }

    public function setEnclosure($enclosure){
        $this->enclosure = $enclosure;

        return $this;
    }

    public function arrayToFile($array, $filepath, $headers = null)
    {
        $delimiter_esc = preg_quote($this->delimiter, '/');
        $enclosure_esc = preg_quote($this->enclosure, '/');

        $output = array();
        foreach ( $array as $field ) {

            // Enclose fields containing $delimiter, $this->enclosure or whitespace
            if ( $this->encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
                $output[] = $this->enclosure . str_replace($this->enclosure, $this->enclosure . $this->enclosure, $field) . $this->enclosure;
            }
            else {
                $output[] = $field;
            }
        }

        return implode( $this->delimiter, $output );
    }

    public function getType()
    {
        return $this->type;
    }
}