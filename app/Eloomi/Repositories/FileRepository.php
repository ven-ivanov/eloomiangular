<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories;

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Models\File;
use Symfony\Component\HttpFoundation\File\File as LaravelFile;
use Eloomi\Models\File as FileModel;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;

class FileRepository extends CrudRepository implements FileRepositoryInterface {

    protected $root;
    protected $id_needed_at_creation = true;

    public function __construct(
        File $file
    ){
        $this->root = join(DIRECTORY_SEPARATOR, [storage_path(), "upload"]);
        $this->model = $file;
    }

    protected function update_non_fillables($model, array $info){
        if (!isset($info['file'])){
            throw new InvalidInputException("file must be set when adding or changing a file");
        }

        $file = $info['file'];
        if (!$file instanceof LaravelFile){
            throw new InvalidInputException("file must be a file");
        }

        $extension = $file->getExtension() ?: $file->guessExtension();
        $name = $model->id . '.' . $extension;
        $file->move($this->root, $name);

        $model->name = $name;
        $model->save();

        return $model;
    }

    public function getFile($id){
        $file = FileModel::findOrFail($id);
        $path = $this->getFilePath($file);

        return new LaravelFile($path);
    }

    public function getFilePath($model){
        if (!$model instanceof File){
            $model = $this->getById($model);
        }

        return $this->root . DIRECTORY_SEPARATOR . $model->name;
    }

    public function delete($model, $deleteFile = true){
        if ($deleteFile){
            $path = $this->getFilePath($model);
            unlink($path);
        }
        $model->delete();
    }

}