<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories;

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;
use Eloquent;
use ErrorException;

abstract class CrudRepository implements CrudRepositoryInterface {

    protected $id_needed_at_creation = false;
    protected $required_attributes_at_creation = [];

    public function __construct(){
        if (!isset($this->model)){
            throw new ErrorException('Model must be set for a CrudRepository');
        }
    }

    public function index($all = false){
        $instances = $this->getModel();
        if ($all){
            $instances->allTenants();
        }
        return $instances->get();
    }

    public function create(array $info){

        $info = $this->creationModifier($info);
        foreach($this->required_attributes_at_creation as $attribute){
            if (!isset($info[$attribute])){
                throw new InvalidInputException("$attribute needs to be set at creation");
            }
        }

        $class = get_class($this->getModel());
        $model = new $class;
        if ($this->id_needed_at_creation){
            $model->save();
        }

        return $this->update($model, $info);
    }

    /**
     * @return Eloquent
     * @throws \Exception
     */
    protected function getModel(){
        if (is_null($this->model)){
            throw new \Exception("Model not set for instance of crud repository");
        }

        return $this->model;
    }

    public function delete($model)
    {
        $model->delete();
    }

    public function deleteById($id)
    {
        $model = $this->getById($id);
        $this->delete($model);
    }

    public function deleteByIds(array $ids)
    {
        $models = $this->getByIds($ids);
        foreach($models as $model){
            $this->delete($model);
        }
    }

    public function getById($id)
    {
        return $this->getModel()->findOrFail($id);
    }

    public function getByIds(array $ids)
    {
        $modelQuery = $this->model->whereIn('id', $ids);
        if ($modelQuery->count() != count($ids)){
            throw new TenantModelNotFoundException('Not all models were found');
        }

        return $modelQuery->get();
    }

    public function updateById($id, array $info)
    {
        $model = $this->getById($id);
        return $this->update($model, $info);
    }

    public function update($model, array $info)
    {
        $model->fill($info);
        $model = $this->update_non_fillables($model, $info);
        $model->push();
        return $model;
    }

    protected function update_non_fillables($model, array $info){
        return $model;
    }

    protected function creationModifier($info){ return $info; }

}