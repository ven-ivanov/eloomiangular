<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 09/02/15
 * Time: 16.16
 */

namespace Eloomi\Repositories\Statistics;


use Eloomi\Repositories\Statistics\Interfaces\StatisticsRepositoryInterface;
use Eloomi\Repositories\Statistics\Interfaces\CoureseStatisticsRepositoryInterface;

class StatisticsRepository implements StatisticsRepositoryInterface{

    /**
     * @var CoureseStatisticsRepositoryInterface
     */
    private $courseStats;

    public function __construct(
        CoureseStatisticsRepositoryInterface $courseStats
    ){

        $this->$courseStats = $courseStats;
    }

    public function course(Course $course)

    {
        return [
            'users_assigned'     => $this->courseStats->users_assigned($course),
            'users_started'      => $this->courseStats->users_started($course),
            'users_completed'    => $this->courseStats->users_completed($course),
            'completion_rate'    => $this->courseStats->completion_rate($course),
            'fastest_completion' => $this->courseStats->fastest_completion($course),
            'slowest_completion' => $this->courseStats->slowest_completion($course),
            'average_completion' => $this->courseStats->average_completion($course),
        ];
    }

}