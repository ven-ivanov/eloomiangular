<?php

namespace Eloomi\Repositories\Statistics;
use Eloomi\Models\Course;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\Statistics\CourseStatisticsRepositoryInterface;
use Eloquent;

class CourseStatisticsRepository implements CourseStatisticsRepositoryInterface
{

    private $assigned_users;
    private $users_started;
    private $users_completed;
    private $users_completed_ids;
    /**
     * @var User
     */
    private $user;

    public function __construct(
        User $user
    ){

        $this->user = $user;
    }

    public function users_assigned_count(Course $course){
        return $this->users_assigned($course)->count();
    }

    public function users_assigned(Course $course)
    {
        if ($this->assigned_users){
            return $this->assigned_users;
        }

        $individual_users = $course->users;
        $course->load('organizational_units');
        $unit_users = $course->organizational_units->map(function($unit){
            return $unit->descendantsAndSelf()->with('users')->get();
        })->collapse();


        $unit_users = $unit_users->map(function($unit){
            return $unit->users;
        })->collapse();

        $team_users = $course->teams()->with('users')->get()->map(function($element){
            return $element->users;
        })->collapse();

        $this->assigned_users = $individual_users->merge($unit_users)->merge($team_users)->unique();
        return $this->assigned_users;
    }

    public function users_started_count(Course $course)
    {
        return $this->users_started($course)->count();
    }

    public function users_started(Course $course)
    {
        if ($this->users_started){ return $this->users_started; }

        $modules = $course->modules()->get();

        $this->users_started = $modules->map(function($module){
            return $module->user_progress()
                ->wherePivot('progress', 'started')
                ->get();
        })->collapse()->unique();

        return $this->users_started;
    }

    public function users_completed_ids(Course $course){

        if (isset($this->users_completed_ids)) { return $this->users_completed_ids; }

        $module_ids = $course->modules()->lists('id');
        $this->users_completed_ids = \DB::table('module_user')
            ->whereIn('module_id', $module_ids)
            ->whereProgress('completed')
            ->groupBy('user_id')
            ->having(\DB::raw('count(*)'), '=', count($module_ids))
            ->lists('user_id');

        return $this->users_completed_ids;
    }

    public function users_completed(Course $course)
    {
        if ($this->users_completed) { return $this->users_completed; }

        $ids = $this->users_completed_ids($course);
        $this->users_completed = $this->user->getByIds($ids);

        return $this->users_completed;
    }

    public function users_completed_count(Course $course)
    {
        return $this->users_completed($course)->count();
    }

    public function completion_rate(Course $course)
    {
        return $this->users_completed($course) / $this->users_started($course);
    }

    private function complete_query(Course $course){

        $user_completed_ids = $this->users_completed_ids($course);
        $module_ids = $module_ids = $course->modules()->lists('id');

        return \DB::table('module_user')
            ->whereIn('user_id', $user_completed_ids)
            ->whereIn('module_id', $module_ids)
            ->groupBy('user_id')
            ->select(\DB::raw('TIMESTAMPDIFF(SECOND, MIN(created_at), MAX(created_at)) as seconds, user_id'));

    }

    public function fastest_completion(Course $course)
    {
        return $this->complete_query($course)->orderBy('seconds','asc')->first();

    }

    public function slowest_completion(Course $course)
    {
        return $this->complete_query($course)->orderBy('seconds','desc')->first();
    }

    public function average_completion(Course $course)
    {
        $completion_times = $this->complete_query($course)->lists('seconds');
        return array_sum($completion_times) / count($completion_times);
    }
}