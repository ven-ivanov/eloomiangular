<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories;

use Eloomi\Models\Company;
use Eloomi\Models\Interfaces\ModuleInterface;
use Eloomi\Repositories\Interfaces\LMS\VideoRepositoryInterface;
use Vimeo;
use Queue;
use Config;
use Sentry;
use Whoops\Exception\ErrorException;

abstract class ApiRepository extends CrudRepository implements VideoRepositoryInterface {

    protected $root;
    protected $company;
    protected $id_needed_at_creation = true;

    public function __construct(
        ModuleInterface $model,
        Company $company
    ){
        $this->root = storage_path('TempVimeoFiles');
        $this->model = $model;
        $this->company = $company;
        parent::__construct();
    }

    public function getType(){
        return $this->model->getType();
    }

    protected function getWorkerClassName(){
        if (!isset($this->workerClass)){
            throw new ErrorException('workerClass has to be set for ApiRepository');
        }

        return $this->workerClass;
    }

    protected function getWorkerString($method){
        return $this->getWorkerClassName() . '@' . $method;
    }

    protected function getRandomFilename($extension){
        $user_id = Sentry::getUser()->id;
        $timestamp = time();
        $random = str_random(5);

        $name = $user_id.'-'.$timestamp.'-'.$random;

        if ($extension){
            $name .= '.' . $extension;
        }
        return $name;
    }

    protected function queue($method, $info){
        $info['company_id'] = $this->company->current_company()->id;
        Queue::push($method, $info);
    }

}