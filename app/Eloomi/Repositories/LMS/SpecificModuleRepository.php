<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Models\Interfaces\ModuleInterface;
use Eloomi\Repositories\Interfaces\LMS\SpecificModuleRepositoryInterface;
use Vimeo;
use Queue;
use Config;
use Sentry;
use Whoops\Exception\ErrorException;
use Eloomi\Repositories\CrudRepository;

abstract class SpecificModuleRepository extends CrudRepository implements SpecificModuleRepositoryInterface {

    public function __construct(){
        if (!isset($this->type)){
            throw new ErrorException('Type must be set for a SpecificModuleRepository');
        }
    }

    public function getType()
    {
        return $this->type;
    }

    public function isReady(ModuleInterface $module){
        return $module->isReady();
    }
}