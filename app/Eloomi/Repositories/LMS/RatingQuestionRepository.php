<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Models\RatingQuestion;
use Eloomi\Repositories\Interfaces\LMS\RatingQuestionRepositoryInterface;
use Vimeo;
use Queue;
use Config;
use Eloomi\Repositories\CrudRepository;
use Sentry;

class RatingQuestionRepository extends CrudRepository implements RatingQuestionRepositoryInterface {

    public function __construct(
        RatingQuestion $model
    ){
        $this->model = $model;
        parent::__construct();
    }

    public function update_non_fillables($module, array $info)
    {
        return $module;
    }

    public function getType(){
        return $this->getModel()->getType();
    }

    public function evaluateAnswers($question, $answer)
    {
        if (!is_numeric($answer)){
            throw new InvalidInputException("Answers to ratings must be a number");
        }

        if ($question->min <= $answer && $question->max >= $answer){
            throw new InvalidInputException("Answers to rating question must be between the given min and max");
        }

        return 1;
    }

    public function copy($model)
    {
        $model = $model->replicate();
        $model->save();
        return $model;
    }
}