<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Exceptions\UserExceptions\WrongAnswersException;
use Eloomi\Models\Module;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\ProgressRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Vimeo;
use Queue;
use Config;
use Sentry;

class ProgressRepository implements ProgressRepositoryInterface {

    /**
     * @var UserRepositoryInterface
     */
    private $users;
    /**
     * @var ModuleRepositoryInterface
     */
    private $modules;

    public function __construct(
        UserRepositoryInterface $users,
        ModuleRepositoryInterface $modules
    ){

        $this->users = $users;
        $this->modules = $modules;
    }

    public function startModule(User $user, Module $module){
        $module->user_progress()->attach($user, ['progress' => 'started']);
    }

    public function updateProgress(User $user, $info){

        if (!isset($info['module_id'])){
            throw new InvalidInputException('module_id needs to be set in order to update progress');
        }

        $module = $this->modules->getById($info['module_id']);
        $this->modules->assertUserAllowedToAnswer($module, $user);

        try{
            $this->modules->assertAnswers($module, $info);
            $module->user_progress()->attach($user, ['progress' => 'completed']);
        } catch(WrongAnswersException $e){
            $module->user_progress()->attach($user, ['progress' => 'failed']);
            throw $e;
        } finally{
            $module->save();
        }

        return $this->modules->nextModule($module);
    }

    public function deleteAllProgressForModule(Module $module){

        $module->load("course.modules");
        $course = $module->course;
        $modulesInCourse = $module->course->modules;

        $moduleIds = $modulesInCourse->lists('id');

        $user_ids = \DB::table('module_user')->whereIn('module_id', $moduleIds)->select('user_id')->get();
        \DB::table('module_user')->whereIn('module_id', $moduleIds)->delete();

        $user_ids = array_map( function($user){ return $user->user_id; },$user_ids);
        $data = [
            'course_id' => $course->id,
            'user_ids'  => $user_ids
        ];

        Queue::push("NotificationWorker@courseUpdated", $data);
    }
}