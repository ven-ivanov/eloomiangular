<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Exceptions\UserExceptions\WrongAnswersException;
use Eloomi\Models\FreeText;
use Eloomi\Exceptions\MissingInputException;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\FreeTextRepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;
use Vimeo;
use Queue;
use Config;
use Sentry;
use Eloomi\Repositories\CrudRepository;

class FreeTextRepository extends CrudRepository implements FreeTextRepositoryInterface {

    protected $file_repository;
    protected $id_needed_at_creation = true;

    public function __construct(
        FreeText $model,
        FileRepositoryInterface $file_repository
    ){
        $this->model = $model;
        $this->file_repository = $file_repository;
        parent::__construct();
    }

    public function update_non_fillables($module, array $info){

        if (isset($info['text'])){
            $module->text = $info['text'];
        }

        $module->load('image');
        if (isset($info['remove_image'])){
            $this->file_repository->deleteById($module->image_id);
            $module->image_id = null;
            return $module;
        }

        if(!isset($info['image'])){
            return $module;
        }

        if ( !$info['image'] instanceof File ){
            throw new MissingInputException('Image is not the correct type');
        }

        if (!is_null($module->image)){
            $file = $this->file_repository->update($module->image, ['file' => $info['image']]);
        } else {
            $file = $this->file_repository->create(['file' => $info['image']]);
        }

        $module->image()->associate($file);

        return $module;
    }

    public function getType(){
        return 'free_text';
    }

    public function assertAnswers($teachable, $info)
    {
        return 1;
    }

    public function copy($model)
    {
        $model = $model->replicate();
        $model->save();
        return $model;
    }
}