<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories\LMS;

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Eloomi\Data\Permissions;
use Eloomi\Exceptions\SessionExceptions\NotAllowedException;
use Eloomi\Models\Company;
use Eloomi\Models\Course;
use Eloomi\Models\OrganizationalUnit;
use Eloomi\Models\Team;
use Eloomi\Models\User;
use Eloomi\Repositories\CrudRepository;
use Eloomi\Repositories\Interfaces\LMS\CertificateRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\CourseShopRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\TeamRepositoryInterface;
use Eloomi\Repositories\Interfaces\TimespanRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Repositories\Organization\OrganizationalUnitRepository;
use Illuminate\Database\Eloquent\Collection;

class CourseShopRepository implements CourseShopRepositoryInterface {

    /**
     * @var CompanyRepositoryInterface
     */
    private $companies;
    /**
     * @var CourseRepositoryInterface
     */
    private $courses;
    /**
     * @var ModuleRepositoryInterface
     */
    private $modules;
    /**
     * @var Course
     */
    private $course;
    /**
     * @var Company
     */
    private $company;

    public function __construct(
        CompanyRepositoryInterface $companies,
        CourseRepositoryInterface $courses,
        ModuleRepositoryInterface $modules,
        Company $company,
        Course $course
    ){
        $this->companies = $companies;
        $this->courses = $courses;
        $this->modules = $modules;
        $this->course = $course;
        $this->company = $company;
    }


    public function buy($course_id)
    {
        \TenantScope::disable();
        $course = $this->courses->getById($course_id);

        if(!$this->isSellable($course)){
            throw new NotAllowedException("It is not possible to buy this course");
        }

        // TODO: Check for permission
        $company_id = $this->company->current_company();
        $course = $this->assign($course_id, $company_id);
        \TenantScope::enable();
        return $course;


    }

    public function isSellable($courses){
        if (!is_array($courses)){ $courses = [$courses];}

        \TenantScope::disable();
        try{
        foreach($courses as $course){

            if (!$course instanceof Course){ $course = $this->courses->getById($course);}
            if (is_null($course->price)){ return false; }

            }
        } catch (TenantModelNotFoundException $e) {
            return false;
        } finally {
            \TenantScope::enable();
        }

        return true;
    }

    public function listCourses()
    {
        \TenantScope::disable();
        $courses = $this->course->whereNotNull('price')->whereActive(true);
        \TenantScope::enable();
        return $courses->get();
    }

    public function assign($course_id, $company_id, $price = null){

        // checking if company exists
        if ($company_id instanceof Company) { $company_id = $company_id->id; }

        // Hacking this as a global variable
        \TenantScope::addTenant('company_id', $company_id);
        \TenantScope::disable();

        $course = $this->courses->getById($course_id);
        $course->load(['modules.teachable']);
        $course_copy = $course->replicate();
        $course_copy->company_id = $company_id;
        $course_copy->due_date = null;
        $course_copy->price = null;
        $course_copy->push();
        foreach($course_copy->modules as $module){
            $this->modules->copy($module, $course_copy);
        }

        $course->copies()->attach($course_copy, [
            'company_id' => $company_id,
            'price'      => $price ?: $course->price
        ]);

        $course->save();
        \TenantScope::enable();
        return $course_copy;
    }

    public function request($course_id, $user)
    {
        \TenantScope::disable();
        $course = $this->courses->getById($course_id);
        if (!$this->isSellable($course)){
            throw new TenantModelNotFoundException("No course found with that Id");
        }
        \TenantScope::enable();

        $admin_group       = \Sentry::findGroupByName(Permissions::COMPANY_ADMIN);
        $super_admin_group = \Sentry::findGroupByName(Permissions::CROSS_COMPANY_ADMIN);

        $admins       = \Sentry::findAllUsersInGroup($admin_group);
        $super_admins = \Sentry::findAllUsersInGroup($super_admin_group);

        $emails = $admins->merge($super_admins)->lists('email');

        \Mail::queue('emails.courses.request', ['user' => $user, 'course' => $course], function($message) use ($user, $course, $emails)
        {
            $subject = \Lang::get('email.subject.request', ['name' => $user->name(), 'course' => $course->name]);
            $message->to($emails)->subject($subject);
        });
    }
}