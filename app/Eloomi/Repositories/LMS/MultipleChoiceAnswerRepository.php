<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories\LMS;

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Models\MultipleChoiceAnswer;
use Eloomi\Models\MultipleChoiceQuestion;
use Eloomi\Repositories\Interfaces\LMS\MultipleChoiceAnswerRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\MultipleChoiceQuestionRepositoryInterface;
use Eloomi\Repositories\CrudRepository;

class MultipleChoiceAnswerRepository extends CrudRepository implements MultipleChoiceAnswerRepositoryInterface {

    /**
     * @var MultipleChoiceQuestion
     */
    private $question;

    public function __construct(
        MultipleChoiceQuestion $question,
        MultipleChoiceAnswer $anwser
    ){
        $this->question = $question;
        $this->model = $anwser;
    }

    public function create(array $info){
        if (!isset($info['multiple_choice_question_id'])){
            throw new InvalidInputException("multiple_choice_question_id must be set when creating new answers");
        }

        if (!isset($info['answer'])){
            throw new InvalidInputException("answer must be set when creating new answers");
        }

        if (!isset($info['is_correct'])){
            throw new InvalidInputException("is_correct must be set when creating new answers");
        }

        try {
            $question = $this->question->getById($info['multiple_choice_question_id']);
        }
        catch(TenantModelNotFoundException $e){
            throw new InvalidInputException('Multiple Choice Question with the given id does not exist');
        }

        $answer = $this->getModel()->create($info);

        $answer->load('question')->question()->associate($question);
        $this->update($answer, $info);

        return $answer;
    }

    public function copy($answer, $question)
    {
        $answer = $answer->replicate();
        $answer->question_id = $question->id;
        $answer->save();
        return $answer;
    }
}