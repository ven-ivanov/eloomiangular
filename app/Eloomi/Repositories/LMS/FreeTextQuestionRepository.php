<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Models\FreeTextQuestion;
use Eloomi\Repositories\Interfaces\LMS\FreeTextQuestionRepositoryInterface;
use Eloomi\Repositories\CrudRepository;

class FreeTextQuestionRepository extends CrudRepository implements FreeTextQuestionRepositoryInterface {

    protected $file_repository;

    public function __construct(
        FreeTextQuestion $model
    ){
        $this->model = $model;
        parent::__construct();
    }

    public function getType(){
        return $this->getModel()->getType();
    }

    public function evaluateAnswers($question, $answer)
    {
        if (!is_string($answer)){
            throw new InvalidInputException("Text should be provided when answering free text questions");
        }

        return 1;
    }

    public function copy($model)
    {
        $model = $model->replicate();
        $model->save();
        return $model;
    }
}