<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Models\Company;
use Eloomi\Models\Slideshare as SlideshareModel;
use Eloomi\Exceptions\MissingInputException;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\PowerpointRepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;
use Vimeo;
use Queue;
use Config;
use Sentry;
use Eloomi\Repositories\ApiRepository;

class SlideshareRepository extends ApiRepository implements PowerPointRepositoryInterface {

    protected $root;
    protected $id_needed_at_creation = true;
    /**
     * @var FileRepositoryInterface
     */
    private $files;

    public function __construct(
        SlideshareModel $model,
        Company $company,
        FileRepositoryInterface $file_repository
    ){
        $this->root = storage_path('TempSlideshareFiles');
        $this->workerClass = 'Eloomi\Workers\SlideshareWorker';
        parent::__construct($model, $company);
        $this->files = $file_repository;
    }

    public function delete($model){

        $slideshare_id = $model->slideshare_id;
        $model->delete();
        if ($this->getModel()->where('slideshare_id', $slideshare_id)->count() > 0){
            return;
        }

        $method = $this->getWorkerString('delete');
        $workerData = ['slideshare_id' => $slideshare_id];

        $this->queue($method, $workerData);

    }

    public function update_non_fillables($model, array $info){

        if(isset($info['presentation'])){
            if ( !$info['presentation'] instanceof File ){
                throw new MissingInputException('Presentation is not the correct type');
            }

            if ( !$info['presentation']->isValid()) {
                throw new MissingInputException('Presentation was not uploaded correctly');
            }

            $info['presentation_id'] = $this->files->create(['file' => $info['presentation']])->id;
        }

        if (!isset($info['presentation_id'])){
            return $model;
        }

        $workerData = [
            'user_id'         => Sentry::getUser()->id,
            'model_id'        => $model->id,
            'presentation_id' => $info['presentation_id']
        ];

        $method = $this->getWorkerString("upload");
        $this->queue($method, $workerData);

        return $model;
    }

    public function assertAnswers($teachable, $info)
    {
        return 1;
    }

    public function copy($model)
    {
        $model = $model->replicate();
        $model->save();
        return $model;
    }
}