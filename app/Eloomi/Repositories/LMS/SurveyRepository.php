<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Exceptions\UserExceptions\WrongAnswersException;
use Eloomi\Models\Survey;
use Eloomi\Repositories\Interfaces\LMS\QuestionRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\SurveyRepositoryInterface;
use Vimeo;
use Queue;
use Config;
use Sentry;
use Eloomi\Repositories\CrudRepository;

class SurveyRepository extends CrudRepository implements SurveyRepositoryInterface {

    /**
     * @var QuestionRepositoryInterface
     */
    private $questions;

    public function __construct(
        QuestionRepositoryInterface $questions,
        Survey $model
    ){
        $this->model = $model;
        $this->questions = $questions;
        parent::__construct();
    }

    public function delete($model){
        foreach($model->questions as $question){
            $question->delete();
        }
        $model->delete();
    }

    public function update_non_fillables($model, array $info)
    {
        return $model;
    }

    public function getType()
    {
        return 'survey';
    }

    /**
     * @param Survey $survey
     * @param array $info
     * @throws InvalidInputException
     * @throws WrongAnswersException
     */
    public function assertAnswers($survey, $info)
    {
        if (!isset($info['answers'])){
            throw new InvalidInputException("answers needs to be set");
        }
        $answers      = $info['answers'];
        $questions    = $survey->questions;

        $correct = 0;
        foreach($questions as $question){
            if (array_key_exists($question->id, $answers)){
                $correct += $this->questions->evaluateAnswers($question, $answers[$question->id]);
            }
        }

        $correct = $correct / $questions->count();
        $pass_percentage = $survey->pass_percentage / 100;

        if($correct < $pass_percentage){
            throw new WrongAnswersException("You have not answered correctly on enough questions to pass");
        }
    }

    public function copy($survey)
    {
        $questions = $survey->questions;
        $survey = $survey->replicate();
        $survey->save();
        $survey->load('questions.questionable');
        foreach($questions as $question){
            $this->questions->copy($question, $survey);
        }

        return $survey;

    }
}