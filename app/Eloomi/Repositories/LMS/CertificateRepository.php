<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Models\Certificate;
use Eloomi\Repositories\CrudRepository;
use Eloomi\Repositories\Interfaces\LMS\CertificateRepositoryInterface;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Vimeo;
use Queue;
use Config;
use Sentry;

class CertificateRepository extends CrudRepository implements CertificateRepositoryInterface {

    /**
     * @var FileRepositoryInterface
     */
    private $files;

    public function __construct(
        Certificate $model,
        FileRepositoryInterface $files
    ){
        $this->model = $model;
        parent::__construct();
        $this->files = $files;
    }

    public function update_non_fillables($certificate, array $info)
    {

        if (isset($info['file'])){
            $certificate->load('file');
            if (!is_null($certificate->image)){
                $file = $this->files->update($certificate->image, $info);
            } else {
                $file = $this->files->create($info);
            }
            
            $certificate->file()->associate($file);
        }

        return $certificate;

    }

}