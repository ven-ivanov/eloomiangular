<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Models\Company;
use Eloomi\Models\Vimeo as VimeoModel;
use Eloomi\Exceptions\MissingInputException;
use Eloomi\Repositories\ApiRepository;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\VideoRepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;
use Vimeo;
use Queue;
use Config;
use Sentry;

class VimeoRepository extends ApiRepository implements VideoRepositoryInterface {

    protected $root;
    protected $id_needed_at_creation = true;
    /**
     * @var FileRepositoryInterface
     */
    private $files;

    public function __construct(
        VimeoModel $model,
        Company $company,
        FileRepositoryInterface $files
    ){
        Vimeo::setToken(Config::get('vimeo.token'));
        $this->root = storage_path('TempVimeoFiles');
        $this->workerClass = 'Eloomi\Workers\VimeoWorker';
        parent::__construct($model, $company);
        $this->files = $files;
    }

    public function delete($model){


        $path = $model->path;
        $model->delete();

        if ($this->getModel()->where('path', $path)->count() > 0){
            return;
        }

        $method = $this->getWorkerString('delete');
        $workerData = ['video_path' => $path];

        $this->queue($method, $workerData);
    }

    public function update_non_fillables($model, array $info){

        if (isset($info['ready'])){
            $model->ready = $info['ready'] === true || $info['ready'] == 1 || $info['ready'] == true;
        }

        if (isset($info['video'])){
            if ( !$info['video'] instanceof File ){
                throw new MissingInputException('Video is not the correct type');
            }

            if ( !$info['video']->isValid()) {
                throw new MissingInputException('Video was not uploaded correctly');
            }

            $file = $this->files->create(['file' => $info['video']]);
        }

        if(isset($info['video_id'])){
            $file = $this->files->getById($info['video_id']);
        }

        if (!isset($file)){
            return $model;
        }

        $model->ready = false;

        $method = $this->getWorkerString('upload');

        $workerData = [
            'user_id'    => Sentry::getUser()->id,
            'model_id'   => $model->id,
            'video_path' => $this->files->getFilePath($file)
        ];

        $this->queue($method, $workerData);
        $this->files->delete($file, false);

        return $model;
    }

    public function assertAnswers($teachable, $info)
    {
        return 1;
    }

    public function copy($model)
    {
        $model = $model->replicate();
        $model->save();
        return $model;

    }
}