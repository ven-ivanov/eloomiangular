<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories\LMS;

use Eloomi\Models\MultipleChoiceQuestion;
use Eloomi\Repositories\Interfaces\LMS\MultipleChoiceAnswerRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\MultipleChoiceQuestionRepositoryInterface;
use Vimeo;
use Queue;
use Config;
use Sentry;
use Eloomi\Repositories\CrudRepository;

class MultipleChoiceQuestionRepository extends CrudRepository implements MultipleChoiceQuestionRepositoryInterface {

    protected $file_repository;
    /**
     * @var MultipleChoiceAnswerRepositoryInterface
     */
    private $answers;

    public function __construct(
        MultipleChoiceQuestion $model,
        MultipleChoiceAnswerRepositoryInterface $answers
    ){
        $this->model = $model;
        parent::__construct();
        $this->answers = $answers;
    }

    public function update_non_fillables($module, array $info)
    {
        return $module;
    }

    public function getType(){
        return $this->getModel()->getType();
    }

    public function evaluateAnswers($question, $answer)
    {
//        $question->load('ques')
        $real_answers = $question->questionable->answers()->whereIsCorrect(true)->lists('id');

        if ($real_answers != $answer){
            return 0;
        }

        return 1;
    }

    public function copy($model)
    {
        $answers = $model->answers;
        $question = $model->replicate();
        $question->save();
        foreach($answers as $answer){
            $this->answers->copy($answer, $question);
        }
        return $question;
    }
}