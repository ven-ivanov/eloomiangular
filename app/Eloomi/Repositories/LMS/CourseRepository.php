<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories\LMS;

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Eloomi\Models\Company;
use Eloomi\Models\Course;
use Eloomi\Models\OrganizationalUnit;
use Eloomi\Models\Team;
use Eloomi\Models\User;
use Eloomi\Repositories\CrudRepository;
use Eloomi\Repositories\Interfaces\LMS\CertificateRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\TeamRepositoryInterface;
use Eloomi\Repositories\Interfaces\TimespanRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Repositories\Organization\OrganizationalUnitRepository;
use Illuminate\Database\Eloquent\Collection;

class CourseRepository extends CrudRepository implements CourseRepositoryInterface {

    protected $users;
    protected $teams;
    protected $collection;
    protected $organizational_units;
    protected $files;

    /**
     * @var ModuleRepository
     */
    private $modules;
    /**
     * @var CertificateRepositoryInterface
     */
    private $certificates;
    /**
     * @var TimespanRepositoryInterface
     */
    private $timespans;
    /**
     * @var CompanyRepositoryInterface
     */
    private $companies;
    /**
     * @var Company
     */
    private $company;

    public function __construct(
        UserRepositoryInterface $users,
        OrganizationalUnitRepository $organizational_units,
        TeamRepositoryInterface $teams,
        FileRepositoryInterface $files,
        ModuleRepositoryInterface $modules,
        Course $model,
        Collection $collection,
        CertificateRepositoryInterface $certificates,
        TimespanRepositoryInterface $timespans,
        CompanyRepositoryInterface $companies,
        Company $company
    ){
        $this->users = $users;
        $this->teams = $teams;
        $this->collection = $collection;
        $this->organizational_units = $organizational_units;
        $this->files = $files;
        $this->model = $model;
        $this->modules = $modules;
        $this->certificates = $certificates;
        $this->timespans = $timespans;
        $this->companies = $companies;
        $this->company = $company;
    }

    public function index($all = false){
        if ($all){
            return $this->getModel()->get();
        }
        $user = $this->users->getCurrentUser();
        $courses = $this->getCoursesByUser($user, true);
        return $courses->filter(function($element){
            return $element->active;
        });
    }

    public function create(array $info){

        if (!isset($info['name'])){
            throw new InvalidInputException("name needs to be set at creation");
        }

        $class = get_class($this->getModel());
        $model = new $class;

        // TODO: check real permissions/settings
        if (true){

            $model->company_id = null;
            if (isset($info['is_sellable'])){
                $model->is_sellable = $info['is_sellable'];
            }

            if (isset($info['is_sellable'])){
                $model->is_sellable = $info['is_sellable'];
            }
        }

        $info['active'] = false;
        $model = $this->update($model, $info);



        return $model;
    }

    public function delete($model){
        $modules = $model->modules;
        foreach ($modules as $module){
            $this->modules->delete($module);
        }

        $model->delete();
    }

    public function search($term)
    {
        $users = Course::where(function ($query) use ($term) {
            $query
                ->where('name', 'like', "%$term%")
                ->orWhere('code', 'like', "%$term%");
        })->get();

        return $users;
    }

    public function getCoursesByUser($user, $include_inherited)
    {
        if (!$user instanceof User){
            $user = $this->users->getById($user);
        }


        // TODO: Move work to database
        $courses_from_user = $user->courses;

        if(!$include_inherited){
            return $courses_from_user;
        }

        $courses_from_team = $this->collection->make([]);
        foreach($user->teams()->get() as $team){
            $courses_from_team = $courses_from_team->merge(
                $this->getCoursesByTeam($team)->get()
            );
        }

        $courses_from_organizational_unit = $this->getCoursesByOrganizationalUnit($user->organizational_unit(), $include_inherited);

        return $courses_from_user->merge($courses_from_team)->merge($courses_from_organizational_unit);
    }

    public function getCoursesByTeam($team)
    {
        if (!$team instanceof User){
            $team = $this->teams->getById($team);
        }


        return $team->courses();
    }

    public function getCoursesByOrganizationalUnit($organizational_unit = null, $include_inherited = true)
    {
        if(!$organizational_unit instanceof OrganizationalUnit){
            $organizational_unit = $this->organizational_units->getById($organizational_unit);
        }

        $courses_from_organizational_units = $this->collection->make([]);

        if (is_null($organizational_unit)){
            return $courses_from_organizational_units;
        }

        if (!$include_inherited){
            $units = $this->collection->make($organizational_unit);
        } else {
            $units = $organizational_unit->getAncestorsAndSelf();
        }

        foreach($units as $unit){
            $courses_from_organizational_units = $courses_from_organizational_units->merge($unit->courses()->get());
        }

        return $courses_from_organizational_units;
    }

    protected function update_non_fillables($course, array $info)
    {
        if (isset($info['image'])) {
            $course->load('image');
            $image = $this->files->create(['file' => $info['image']]);

            $info['image_id'] = $image->id;
        }

        if (isset($info['image_id'])){
            $image = $this->files->getById($info['image_id']);
            $course->image()->associate($image);
        }


        if (isset($info['user_ids'])) {
            if (!is_array($info['user_ids'])) {
                $info['user_ids'] = explode(',', $info['user_ids']);
            }
            $course->users()->sync($info['user_ids']);
        }

        if (isset($info['unit_ids'])) {
            if (!is_array($info['unit_ids'])) {
                $info['unit_ids'] = explode(',', $info['unit_ids']);
            }
            $course->organizational_units()->sync($info['unit_ids']);
        }

        if (isset($info['team_ids'])) {
            if (!is_array($info['team_ids'])) {
                $info['team_ids'] = explode(',', $info['team_ids']);
            }
            $course->teams()->sync($info['team_ids']);
        }

        if(isset($info['valid_for'])){
            $timespan = $this->timespans->getById($info['valid_for']);
            $course->lasts_for()->associate($timespan);
        }

        $certificate_info = [];
        if (isset($info['certificate_text'])) {
            $certificate_info['text'] = $info['certificate_text'];
        }

        if (isset($info['certificate_file'])){
            $certificate_info['file'] = $info['certificate_file'];
        }

        if ($certificate_info){
            $certificate = $this->certificates->create($certificate_info);
            $course->certificate()->associate($certificate);
        }

        // TODO: Check for permission
        if(isset($info['price']) && is_numeric($info['price'])){
            $course->price = $info['price'];
        }

        return $course;
    }

}