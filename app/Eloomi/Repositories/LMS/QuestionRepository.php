<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories\LMS;

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Models\Module;
use Eloomi\Models\Question;
use Eloomi\Models\Survey;
use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\SurveyRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\FreeTextQuestionRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\MultipleChoiceQuestionRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\QuestionRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\RatingQuestionRepositoryInterface;
use Eloomi\Traits\MultiRepository;
use Eloomi\Traits\Orderable;
use Eloomi\Repositories\CrudRepository;

class QuestionRepository extends CrudRepository implements QuestionRepositoryInterface {

    use MultiRepository, Orderable;

    /**
     * @var SurveyRepositoryInterface
     */
    protected $surveys;
    /**
     * @var ModuleRepositoryInterface
     */
    private $module;

    public function __construct(
        FreeTextQuestionRepositoryInterface $free_texts,
        MultipleChoiceQuestionRepositoryInterface $multiple_choice,
        RatingQuestionRepositoryInterface $ratings,
        Question $question,
        Module $module
    ){
        $this->repositories[] = $free_texts;
        $this->repositories[] = $multiple_choice;
        $this->repositories[] = $ratings;
        $this->model = $question;
        $this->module = $module;
    }

    public function getById($id){
        $question = $this->model
            ->with('questionable')
            ->find($id);

        if (is_null($question)){
            throw new TenantModelNotFoundException("Model not found");
        }

        return $question;
    }

    public function getByIds(array $ids){
        $modelQuery = $this->model->with('specific')->whereIn('id', $ids);
        if ($modelQuery->count() != count($ids)){
            throw new TenantModelNotFoundException('Not all models were found');
        }

        return $modelQuery->get();
    }

    public function create(array $info){
        if (!isset($info['type'])){
            throw new InvalidInputException("type must be set when creating new questions");
        }
        $repo = $this->getRepository($info['type']);

        if (!isset($info['module_id'])){
            throw new InvalidInputException("module_id must be set when creating new questions");
        }
        try {
            $module = $this->module->getById($info['module_id']);
            $survey = $module->teachable;
        }
        catch(TenantModelNotFoundException $e){
            throw new InvalidInputException('Module with the given module_id does not exist');
        }

        if (!$survey instanceof Survey){
            throw new InvalidInputException("Module with the given module_id is not a survey");
        }

        $question = $this->getModel()->create([]);
        $orderMax = $survey->questions->max('order');
        $question->order = $orderMax + 1;

        $specific = $repo->create($info);
        $survey->load('questions')->questions()->save($question);
        $question->load('questionable')->questionable()->associate($specific);
        $question->save();

        return $question;
    }

    public function delete($model){
        $orderMax = $model->survey->questions->max('order');
        $this->changeOrder($model, $orderMax);

        $specific = $model->questionable;
        $repo = $this->getRepository($specific->getType());
        $repo->delete($specific);
        $model->delete();
    }

    public function update_non_fillables($model, array $info){
        $model->load('questionable', 'survey');

        if (isset($info['order']) && in_array($info['order'], $model->survey->questions->lists('order'))){
            $this->changeOrder($model, $info['order']);
        }

        $specific = $model->questionable;
        $repo = $this->getRepository($specific->getType());
        $repo->update($model->questionable, $info);

        return $model;
    }

    public function evaluateAnswers(Question $question, $answer)
    {
        $repo = $this->getRepository($question->getType());
        return $repo->evaluateAnswers($question, $answer);
    }

    public function copy(Question $question, Survey $survey)
    {
        $question = $question->replicate();
        $question->survey_id = $survey->id;
        $question->load('questionable');
        $repo = $this->getRepository($question->questionable->getType());
        $questionable = $repo->copy($question->questionable);

        $question->questionable()->associate($questionable);
        $question->save();
        return $question;

    }
}