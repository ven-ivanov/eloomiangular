<?php
namespace Eloomi\Repositories\LMS;

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Exceptions\SessionExceptions;
use Eloomi\Exceptions\UserExceptions\NotQualifiedException;
use Eloomi\Models\Company;
use Eloomi\Models\Module;
use Eloomi\Models\Course;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use Eloomi\Repositories\Interfaces\FreeTextModuleRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\FreeTextRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\ModuleRepositoryInterface;
use Cartalyst\Sentry\Users as SentryUserExceptions;
use Eloomi\Repositories\Interfaces\LMS\PowerpointRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\SurveyRepositoryInterface;
use Eloomi\Repositories\Interfaces\LMS\VideoRepositoryInterface;
use Eloomi\Traits\MultiRepository;
use Eloomi\Traits\Orderable;
use Eloomi\Repositories\CrudRepository;
use Exception;
use Queue;

class ModuleRepository extends CrudRepository implements ModuleRepositoryInterface{

    use MultiRepository, Orderable;

    protected $company;
    /**
     * @var CourseRepositoryInterface
     */
    private $courses;

    /**
     * @param VideoRepositoryInterface $video
     * @param Company $company
     * @param FreeTextRepositoryInterface $free_text
     * @param PowerPointRepositoryInterface $powerpoint
     * @param SurveyRepositoryInterface $survey
     * @param Module $model
     * @param Course $course
     */
    public function __construct(
        VideoRepositoryInterface $video,
        Company $company,
        FreeTextRepositoryInterface $free_text,
        PowerPointRepositoryInterface $powerpoint,
        SurveyRepositoryInterface $survey,
        Module $model,
        Course $course
    ){
        $this->repositories[] = $video;
        $this->repositories[] = $free_text;
        $this->repositories[] = $survey;
        $this->repositories[] = $powerpoint;
        $this->model = $model;
        $this->company = $company;
        $this->courses = $course;
    }

//    public function getById($id){
//        return with(new $this->model)->with('teachable')->find($id);
//    }

    public function delete($model){
        $repo = $this->getRepository($model->getType());
        $repo->delete($model->teachable);
        $model->delete();
    }

    public function create(array $info)
    {
        if (!isset($info['name'])){
            throw new InvalidInputException('name not specified');
        }

        if(!isset($info['course_id'])){
            throw new InvalidInputException('course_id not specified');
        }

        $course = $this->courses->getById($info['course_id']);

        if (!isset($info['module_type'])){
            throw new InvalidInputException('module_type not specified');
        }

        $repo = $this->getRepository($info['module_type']);

        $orderMax = $course->modules->max('order');

        $module = $this->getModel()->create($info);
        $module->order = $orderMax + 1;
        $specific_module = $repo->create($info);

        $module->course()->associate($course);
        $module->teachable()->associate($specific_module);
        $module->save();

        return $module;
    }
//15
    /**
     * @param Module $model
     * @param array $info
     * @return Module
     * @throws Exception
     */
    protected function update_non_fillables($model, array $info)
    {
        $repo = $this->getRepository($model->getType());

        if (isset($info['retake_required']) && $info['retake_required']){
            Queue::push('Eloomi\Workers\Modules@retakeModule', [
                'company_id' => $this->company->current_company(),
                'module_id'  => $model->id,

            ]);
        }

        if (isset($info['order']) && in_array($info['order'], $model->course->modules->lists('order'))){
            $this->changeOrder($model, $info['order']);
        }

        $repo->update($model->teachable, $info);

        return $model;
    }

    public function assertUserAllowedToAnswer(Module $module, User $user)
    {
        // Has the user already passed?
        $user->load('module_progress');
        $already_passed = $user->module_progress()
            ->wherePivot('module_id', '=', $module->id)
            ->wherePivot('progress', '=', 'completed')
            ->count();

        if ($already_passed){
            throw new NotQualifiedException("You have already finished the course");
        }

        $course = $module->course;

        if (!$course->in_order){ return; }

        $order = $module->order;
        if ($order == 1){ return; }

        $actualCompleted = $user->module_progress()
            ->whereProgress('completed')
            ->whereCourseId($course->id)
            ->whereIsSkippable(false)
            ->lists('module_id');

        $required_completed = $this->getModel()
            ->whereCourseId($course->id)
            ->whereIsSkippable(false)
            ->where('order', '<', $module->order)
            ->lists('id');

        $modules_missing = (bool) array_diff($required_completed, $actualCompleted);
        if ($modules_missing){
            dd($required_completed, $actualCompleted);
            throw new NotQualifiedException("You have not finished the prerequisite modules");
        }
    }

    public function assertAnswers(Module $module, $info)
    {
        $repo = $this->getRepository($module->getType());
        $module->with('teachable');
        $repo->assertAnswers($module->teachable, $info);
    }

    public function nextModule(Module $module)
    {
        return $module->course->modules()->whereOrder($module->order + 1)->first();
    }

    public function copy(Module $module, Course $course)
    {
        $module = $module->replicate();
        $module->course_id = $course->id;
        $repo = $this->getRepository($module->getType());
        $teachable = $repo->copy($module->teachable);
        $module->teachable()->associate($teachable);
        $module->save();
    }
}