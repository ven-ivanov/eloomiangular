<?php namespace Eloomi\Repositories\Interfaces;

use Eloomi\Models\OrganizationalUnit;
use Eloomi\Models\User;

interface LanguageRepositoryInterface extends CrudRepositoryInterface {

    public function getByCode($code);
}