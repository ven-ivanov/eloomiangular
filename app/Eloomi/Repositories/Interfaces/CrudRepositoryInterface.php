<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 13.20
 */

namespace Eloomi\Repositories\Interfaces;
use Eloquent;

interface CrudRepositoryInterface {

    public function create(array $info);

    public function getById($id);

    public function getByIds(array $ids);

    public function update($model, array $info);

    public function updateById($id, array $info);

    public function delete($model);

    public function deleteById($id);

    public function deleteByIds(array $id);

    public function index();

}