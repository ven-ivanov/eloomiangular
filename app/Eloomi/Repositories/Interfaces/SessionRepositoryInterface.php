<?php namespace Eloomi\Repositories\Interfaces;

interface SessionRepositoryInterface {

    public function store($credentials, $rememberme);
	public function destroy();
}