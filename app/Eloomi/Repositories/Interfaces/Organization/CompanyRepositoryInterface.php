<?php namespace Eloomi\Repositories\Interfaces\Organization;

use Eloomi\Models\Course;
use Eloomi\Models\OrganizationalUnit;
use Eloomi\Models\Team;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface CompanyRepositoryInterface extends CrudRepositoryInterface {

}