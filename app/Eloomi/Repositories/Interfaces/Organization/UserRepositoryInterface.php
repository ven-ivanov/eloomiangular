<?php namespace Eloomi\Repositories\Interfaces\Organization;

use Eloomi\Models\Course;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface UserRepositoryInterface extends CrudRepositoryInterface{

    public function updateByEmail($email, array $info);

    public function deleteByEmail($email);

    public function getByEmail($email);

    public function getByEmails(array $emails);

    public function getByReference($reference);

    public function getByReferences(array $references);

    public function search($term, $start, $stop, $order = "first_name", $direction = "desc");

    public function activateUser(User $user, $code, $info);

    public function requestActivation(User $user);

    public function resetPassword(User $user, $code, $info);

    public function requestPasswordReset(User $user);

    public function importUsersFromFile($file_id, $type);

    /**
     * @return User
     */
    public function getCurrentUser();

    public function getMyProgress(Course $course);

    public function deactivateByIds($info);

    public function deactivate(User $user);

    public function requestActivations($info);

}