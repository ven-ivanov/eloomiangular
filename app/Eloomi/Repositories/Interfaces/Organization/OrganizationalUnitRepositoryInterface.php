<?php namespace Eloomi\Repositories\Interfaces\Organization;

use Eloomi\Models\OrganizationalUnit;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface OrganizationalUnitRepositoryInterface extends CrudRepositoryInterface {

    public function getAll(OrganizationalUnit $root = null, $depth = null);

    public function moveToNewParent(OrganizationalUnit $ou, OrganizationalUnit $parent);

    public function getTotalUserCount(OrganizationalUnit $ou);

    public function importUnitsFromFile($file, $type);

    public function importUsersFromFile(OrganizationalUnit $unit, $file, $type);
}