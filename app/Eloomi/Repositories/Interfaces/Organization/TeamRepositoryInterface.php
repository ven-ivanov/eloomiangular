<?php namespace Eloomi\Repositories\Interfaces\Organization;

use Eloomi\Models\Team;
use Symfony\Component\HttpFoundation\File\File;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface TeamRepositoryInterface extends CrudRepositoryInterface {

    public function search($term);

    public function importTeamsFromFile(File $file, $type);

    public function importUsersFromFile(Team $team, File $file, $type);
}