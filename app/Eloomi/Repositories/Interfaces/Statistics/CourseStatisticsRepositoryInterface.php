<?php

namespace Eloomi\Repositories\Interfaces\Statistics;
use Eloomi\Models\Course;
use Eloquent;

interface CourseStatisticsRepositoryInterface {

    public function users_assigned(Course $course);

    public function users_assigned_count(Course $course);

    public function users_started(Course $course);

    public function users_started_count(Course $course);

    public function users_completed(Course $course);

    public function users_completed_count(Course $course);

    public function completion_rate(Course $course);

    public function fastest_completion(Course $course);

    public function slowest_completion(Course $course);

    public function average_completion(Course $course);

}