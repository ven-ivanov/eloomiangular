<?php namespace Eloomi\Repositories\Interfaces\LMS;

use Eloomi\Models\OrganizationalUnit;
use Eloomi\Models\Team;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface CourseShopRepositoryInterface {

    public function buy($course_id);

    public function listCourses();

    public function assign($course_id, $company_id, $price = null);

    public function request($course_id, $user);

}