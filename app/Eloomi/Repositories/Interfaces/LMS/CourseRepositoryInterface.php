<?php namespace Eloomi\Repositories\Interfaces\LMS;

use Eloomi\Models\Course;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface CourseRepositoryInterface extends CrudRepositoryInterface {

    public function index($all = false);

    public function search($term);

    public function getCoursesByUser($user, $include_inherited);

    public function getCoursesByTeam($team);

    public function getCoursesByOrganizationalUnit($organizational_unit, $include_inherited);

}