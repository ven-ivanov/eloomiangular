<?php
namespace Eloomi\Repositories\Interfaces\LMS;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface SurveyQuestionRepositoryInterface extends CrudRepositoryInterface {

    public function evaluateAnswers($question, $answer);

    public function copy($model);
}