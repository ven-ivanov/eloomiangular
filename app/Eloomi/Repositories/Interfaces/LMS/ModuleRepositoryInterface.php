<?php
namespace Eloomi\Repositories\Interfaces\LMS;
use Eloomi\Models\Course;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

use Eloomi\Models\Module;
use Eloomi\Models\User;

interface ModuleRepositoryInterface extends CrudRepositoryInterface {

    public function assertUserAllowedToAnswer(Module $module, User $user);

    public function assertAnswers(Module $module, $info);

    public function nextModule(Module $module);

    public function copy(Module $module, Course $course);
}