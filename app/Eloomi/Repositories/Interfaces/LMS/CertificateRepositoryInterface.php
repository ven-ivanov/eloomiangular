<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 13.20
 */

namespace Eloomi\Repositories\Interfaces\LMS;
use Eloquent;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface CertificateRepositoryInterface extends CrudRepositoryInterface {

}