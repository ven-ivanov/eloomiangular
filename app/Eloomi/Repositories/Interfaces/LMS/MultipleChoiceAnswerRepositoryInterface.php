<?php
namespace Eloomi\Repositories\Interfaces\LMS;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;

interface MultipleChoiceAnswerRepositoryInterface extends CrudRepositoryInterface {

    public function copy($answer, $question);

}