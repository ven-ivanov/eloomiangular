<?php
namespace Eloomi\Repositories\Interfaces\LMS;

use Eloomi\Models\Survey;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;
use Eloomi\Models\Question;

interface QuestionRepositoryInterface extends CrudRepositoryInterface {

    public function evaluateAnswers(Question $question, $answer);

    public function copy(Question $question, Survey $survey);
}