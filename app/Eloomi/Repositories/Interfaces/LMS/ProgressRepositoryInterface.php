<?php
namespace Eloomi\Repositories\Interfaces\LMS;

use Eloomi\Models\Module;
use Eloomi\Models\User;

interface ProgressRepositoryInterface {

    public function updateProgress(User $user, $info);

    public function deleteAllProgressForModule(Module $module);

    public function startModule(User $user, Module $module);
}