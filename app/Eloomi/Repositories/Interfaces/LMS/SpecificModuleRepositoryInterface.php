<?php
namespace Eloomi\Repositories\Interfaces\LMS;
use Eloomi\Repositories\Interfaces\CrudRepositoryInterface;
use Eloomi\Repositories\Interfaces\TypeInterface;

interface SpecificModuleRepositoryInterface extends CrudRepositoryInterface, TypeInterface {

    public function assertAnswers($teachable, $info);

    public function copy($teachable);
}