<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 17.18
 */

namespace Eloomi\Repositories\Interfaces;

use Eloomi\Models\File;


interface FileRepositoryInterface extends CrudRepositoryInterface{

    public function getFile($id);

    public function getFilePath($model);

    public function delete($model, $unlink = true);

}