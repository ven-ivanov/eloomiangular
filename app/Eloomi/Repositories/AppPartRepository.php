<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories;

use Eloomi\Models\AppPart;
use Eloomi\Repositories\Interfaces\AppPartRepositoryInterface;
use TenantScope;

class AppPartRepository extends CrudRepository implements AppPartRepositoryInterface {

    protected $required_attributes_at_creation = ['name'];

    public function __construct(
        AppPart $model
    ){
        $this->model = $model;
    }

}