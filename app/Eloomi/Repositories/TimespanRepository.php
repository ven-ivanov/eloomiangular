<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 06/01/15
 * Time: 22.14
 */

namespace Eloomi\Repositories;

use Eloomi\Models\Timespan;
use Eloomi\Repositories\Interfaces\TimespanRepositoryInterface;
use Vimeo;
use Queue;
use Config;
use Sentry;

class TimespanRepository extends CrudRepository implements TimespanRepositoryInterface {

    public function __construct(
        Timespan $model
    ){
        $this->model = $model;
        parent::__construct();
    }
}