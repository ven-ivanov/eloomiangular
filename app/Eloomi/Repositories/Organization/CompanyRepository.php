<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories\Organization;

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Models\Company;
use Eloomi\Repositories\Interfaces\AppPartRepositoryInterface;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Repositories\Interfaces\LanguageRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;
use Predis\Cluster\Distribution\EmptyRingException;
use TenantScope;
use Eloomi\Repositories\Interfaces\Organization\CompanyRepositoryInterface;
use Eloomi\Repositories\CrudRepository;

class CompanyRepository extends CrudRepository implements CompanyRepositoryInterface {

    protected $required_attributes_at_creation = ['name', 'domain'];
    protected $id_needed_at_creation = true;

    /**
     * @var LanguageRepositoryInterface
     */
    private $languages;
    /**
     * @var FileRepositoryInterface
     */
    private $files;
    /**
     * @var OrganizationalUnitRepositoryInterface
     */
    private $organizations;
    /**
     * @var AppPartRepositoryInterface
     */
    private $apps;

    public function __construct(
        Company $company,
        LanguageRepositoryInterface $languages,
        FileRepositoryInterface $files,
        OrganizationalUnitRepositoryInterface $organizations,
        AppPartRepositoryInterface $apps
    ){
        $this->model = $company;
        $this->languages = $languages;
        $this->files = $files;
        $this->organizations = $organizations;
        $this->apps = $apps;
    }

    public function create(array $info)
    {

        foreach ($this->required_attributes_at_creation as $attribute) {
            if (!isset($info[$attribute])) {
                throw new InvalidInputException("$attribute needs to be set at creation");
            }
        }
        // Removing scope temporarily

        $scoped = TenantScope::hasTenant("company_id");
        if ($scoped) {
            $scope_id = TenantScope::getTenantId("company_id");
        }
        TenantScope::disable();

        try {
            $company = $this->getModel()->create([
                "name" => $info['name'],
                "domain" => $info['domain']
            ]);

            if (isset($info['image_id'])){
                TenantScope::enable();
                $image = $this->files->getById($info['image_id']);
                $image->company_id = $company->id;
                $image->save();
                TenantScope::disable();
            }

            TenantScope::addTenant("company_id", $company->id);

            $company = $this->update($company, $info);

            // reapply scope
            if ($scoped) {
                TenantScope::addTenant("company_id", $scope_id);
            } else {
                TenantScope::disable();
            }
        } catch(EmptyRingException $e){
            $company->delete();
            throw new $e;
        }

        return $company;
    }


    public function getById($id)
    {
        return $this->getModel()->with('app_parts')->findOrFail($id);
    }

    public function getByIds(array $ids)
    {

        $modelQuery = $this->getModel()->with('app_parts')->whereIn('id', $ids);
        if ($modelQuery->count() != count($ids)){
            throw new TenantModelNotFoundException('Not all models were found');
        }

        return $modelQuery->get();
    }

    protected function update_non_fillables($model, array $info){

        if(isset($info['language_id'])){
            $language = $this->languages->getById($info['language_id']);
            $model->language()->associate($language);
        }

        if (isset($info['app_part_ids'])){
            $model->load('app_parts');

            // Making sure all these app_parts exist
            $this->apps->getByIds($info['app_part_ids']);

            $model->app_parts()->sync($info['app_part_ids']);
        }

        if(isset($info['image'])){
            $image = $this->files->update($model->logo, ['file' => $info['image']]);
            $info['image_id'] = $image->id;
        }

        if (isset($info['image_id'])){
            $image = $this->files->getById($info['image_id']);
            $model->logo()->associate($image);
        }

        return $model;
    }

}