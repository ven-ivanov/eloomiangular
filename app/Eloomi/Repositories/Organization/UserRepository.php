<?php
namespace Eloomi\Repositories\Organization;

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Cartalyst\Sentry\Users\UserExistsException;
use Eloomi\Exceptions\EloomiException;
use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Filereaders\Interfaces\CsvIOInterface;
use Eloomi\Models\Company;
use Eloomi\Models\Course;
use Eloomi\Repositories\Interfaces\File;
use Eloomi\Repositories\Interfaces\FileRepositoryInterface;
use Eloomi\Models\User;
use Cartalyst\Sentry\Users as SentryUserExceptions;
use Eloomi\Exceptions\UserExceptions as EloomiUserExceptions;
use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Eloomi\Repositories\LanguageRepository;
use Eloomi\Validators\ImageValidator;
use Mail;
use Eloomi\Models\OrganizationalUnit as Units;
use Sentry;
use Sentry\Users\UserAlreadyActivatedException;
use Input;
use Eloomi\Traits\FileReader;
use Eloomi\Repositories\CrudRepository;

class UserRepository extends CrudRepository implements UserRepositoryInterface {

    use FileReader;

    /**
     * @var ImageValidator
     */
    protected $image_validator;
    /**
     * @var FileRepositoryInterface
     */
    protected $file_repository;
    /**
     * @var LanguageRepository
     */
    protected $languages;
    /**
     * @var Company
     */
    private $company;

    public function __construct(
        ImageValidator $image_validator,
        Company $company,
        FileRepositoryInterface $file_repository,
        LanguageRepository $languages,
        CsvIOInterface $csvIo,
        OrganizationalUnitRepositoryInterface $units,
        User $model
    ){
        $this->image_validator = $image_validator->setMaxSize(1024);
        $this->file_repository = $file_repository;
        $this->model = $model;
        $this->fileReaders[] = $csvIo;
        $this->languages = $languages;
        $this->company = $company;
    }

    public function create(array $info)
    {
        try{
            unset($info['password']);
            $user = Sentry::createUser([
                'email' => $info['email'],
                'password' => str_random(64)  // Should never be used
            ]);

            $user = $this->update($user, $info);


            if (isset($info['activate']) && $info['activate']){
                $this->requestActivation($user);
            }

            return $user;
        }
        catch(UserExistsException $e){
            throw new EloomiException("A user already exist with the given email");
        }

    }

    public function activateUser(User $user, $code, $info){

        if (!isset($info['password'])){
            throw new EloomiException('password is required');
        }

        if (!isset($info['password_confirmation'])){
            throw new EloomiException('password_confirmation is required');
        }

        if ($info['password'] != $info['password']){
            throw new EloomiException('password and password_confirmation does not match');
        }
        try {
            $userActivated = $user->attemptActivation($code);
        } catch (UserAlreadyActivatedException $e) {
            throw new EloomiException("User already activated");
        }

        if (!$userActivated)
        {
            $this->requestActivation($user);
            throw new EloomiException("Activation failed - a new activation code is sent");
        }

        $user->password = $info['password'];
        $user->save();

        return $user;

    }

    public function requestActivation(User $user) {

        if ($user->isActivated()){
            return;
        }

        $activation_link =
            $this->company->current_company()->domain . "/".
            "#/user/" .
            $user->id . '/' .
            'activation' . '/' .
            $user->getActivationCode();


        $data = [
            'activation_code' => $activation_link,
            'first_name'      => $user->first_name,
            'last_name'       => $user->last_name,
            'user_id'         =>$user->id
        ];

        Mail::queue('emails.user.activate', $data, function($message) use ($user)
        {
            $message
                ->to($user->email, $user->name())
                ->subject(trans('email.users.activate.subject'));
        });
    }

    public function requestActivations($info){
        if (!isset($info['ids'])){
            throw new InvalidInputException("ids need to be set");
        }

        $users = $this->getByIds($info['ids']);
        foreach($users as $user){
            $this->requestActivation($user);
        }

        return $users;
    }

    public function deactivate(User $user){
        $user->activation_code = null;
        $user->activated = false;
        $user->activated_at = null;
        $user->save();
    }

    public function deactivateByIds($info){
        if (!isset($info['ids'])){
            throw new InvalidInputException("ids need to be set");
        }

        $users = $this->getByIds($info['ids']);
        foreach($users as $user){
            $this->deactivate($user);
        }

        return $users;
    }

    public function resetPassword(User $user, $code, $info){

        if (!$user->checkResetPasswordCode($code)){
            throw new EloomiException('The reset code is incorrect');
        }

        if (!isset($info['password'])){
            throw new EloomiException('password is required');
        }

        if (!isset($info['password_confirmation'])){
            throw new EloomiException('password_confirmation is required');
        }

        if ($info['password'] != $info['password']){
            throw new EloomiException('password and password_confirmation does not match');
        }

        $user->attemptResetPassword($code, $info['password']);

        return $user;

    }

    public function requestPasswordReset(User $user){

        $password_link =
            $this->company->current_company()->domain . "/".
            "#/user/" .
            $user->id . '/' .
            'resetpassword' . '/' .
            $user->getResetPasswordCode();

        $data = [
            'password_reset_link' => $password_link,
            'first_name'          => $user->first_name,
            'last_name'           => $user->last_name,
            'user_id'             =>$user->id
        ];


        Mail::queue('emails.user.reset_password', $data, function($message) use ($user)
        {
            $message
                ->to($user->email, $user->name())
                ->subject(trans('email.users.reset_password.subject'));
        });
    }

    public function getByEmail($email)
    {
        return $this->getModel()->getByEmail($email);
    }

    public function getByEmails(array $emails){
        return $this->getModel()->getByEmails($emails);
    }

    public function updateByEmail($email, array $info)
    {
        $user = $this->getByEmail($email);
        $user = $this->update($user, $info);
        return $user;
    }

    public function deleteByEmail($email){
        $user = $this->getByEmail($email);
        $this->delete($user);
    }

    public function search($term, $start = null, $stop = null, $order = 'first_name', $direction = 'desc'){

        if((!is_null($start) && !ctype_digit($start)) || (!is_null($stop) && !ctype_digit($stop))){
            throw new InvalidInputException("If set, limits should be integers");
        }

        if(!in_array($order, ['first_name', 'last_name', 'email'])){
            throw new InvalidInputException("You can only sort by first name, last name or email");
        }

        if(!in_array($direction, ['desc', 'asc'])){
            throw new InvalidInputException("The only directions you can choose from are 'Ascending' or 'Descending'");
        }

        $users = User::with('language')
                ->join('organizational_units', 'users.organizational_unit_id', '=', 'organizational_units.id')
                ->where(function($query) use ($term)
                        {
                        $query
                            ->where('first_name', 'like', "%$term%")
                            ->orWhere('last_name', 'like', "%$term%")
                            ->orWhere('email', 'like', "%$term%")
                            ->orWhere('organizational_units.name', 'like', "%$term%");
                })
                ->select(
                    'users.id',
                    'users.email',
                    'users.first_name',
                    'users.last_name',
                    'users.birthday',
                    'users.gender',
                    'users.title',
                    'users.phone',
                    'users.timezone',
                    'users.language_id'
                )
                ->orderBy("users." . $order, $direction);

        if (!is_null($start)){
            $users = $users->skip($start);
        }
        if (!is_null($start)){
            $users = $users->take($start);
        }
        return $users->get();
    }

    public function update_non_fillables($user, array $info)
    {

        if (isset($info['password'])){
            if (!isset($info['password_confirmation'])){
                throw new InvalidInputException('password_cofirmation must be set when changing password');
            }

            if ($info['password_confirmation'] != $info['password']){
                throw new InvalidInputException('password and password_cofirmation must be equal');
            }

            $user->password = $info['password'];
        }

        if (isset($info['organizational_unit_code'])){
            $unit = Units::getByCode($info['organizational_unit_code']);
            $user->organizational_units()->sync([$unit->id]);
        }

        if (isset($info['organizational_unit_id'])){
            $user->organizational_units()->sync([$info['organizational_unit_id']]);
        }

        if (isset($info['language_code'])){
            $language = $this->languages->getByCode($info['language_code']);
            $user->language()->associate($language);
        }

        if (isset($info['language_id'])){
            $language = $this->languages->getById($info['language_id']);
            $user->language()->associate($language);
        }

        if (isset($info['trainee_ids'])){
            if (!is_array($info['trainee_ids'])) { $info['trainee_ids'] = explode(',', $info['trainee_ids']); }
            $user->trainees()->sync($info['trainee_ids']);
        }

        if (isset($info['coach_ids'])){
            if (!is_array($info['coach_ids'])) { $info['coach_ids'] = explode(',', $info['coach_ids']); }
            $user->coaches()->sync($info['coach_ids']);
        }


        if (isset($info['image'])){
            $user->load('image');
            if (!is_null($user->image)){
                $image = $this->file_repository->update($user->image, ['file' => $info['image']]);
            } else {
                $image = $this->file_repository->create(['file' => $info['image']]);
            }
            $info['image_id'] = $image->id;
        }

        if (isset($info['image_id'])){
            $image_model = $this->file_repository->getById($info['image_id']);
            $user->image()->associate($image_model);
        }


        if (isset($info['group_id'])){
            $user->groups->sync([$info['group_id']]);
        }

        return $user;
    }

    public function importUsersFromFile($file_id, $type)
    {
        $fileModel  = $this->file_repository->getById($file_id);
        $filePath   = $this->file_repository->getFilePath($fileModel);

        $fileReader = $this->matchFileReaderType($type);
        $rows = $fileReader->fileToArray($filePath);
        if(!$rows){
            throw new InvalidInputException('No users were parsed');
        }

        $errors = [];
        $users  = [];
        foreach ($rows as $index => $row) {
            try {
                unset($row['password']);        // Wont allow that
                if (!isset($row['change'])) {
                    $users[] = $this->create($row);
                    continue;
                }

                switch ($row) {
                    case "add":
                        $users[] = $this->create($row);
                        break;
                    case "change":
                        $user = $this->getByEmail($row['email']);
                        $users[] = $this->update($user, $row);
                        break;
                    case "remove":
                        $user = $this->getByEmail($row['email']);
                        $this->delete($user);
                        break;
                    default:
                        throw new InvalidInputException("Only 'add', 'change' and 'remove allowed for column 'change");
                }
            }
            catch(InvalidInputException $e){
                $errors[] = [
                    [
                        'entity' => $row,
                        'message' => $e->getMessage()
                    ]
                ];
            }
            catch(UserNotFoundException $e){ // Leader, or users cant be found
                $errors[] = [
                    [
                        'entity' => $row,
                        'message' => $e->getMessage()
                    ]
                ];
            }
        }

        return [$errors, $users];
    }

    /**
     * @return User
     */
    public function getCurrentUser()
    {
        return Sentry::getUser();
    }


    public function getMyProgress(Course $course)
    {
        return $this->getCurrentUser()->module_progress()->whereCourseId($course->id)->count();
    }

    public function getByReference($reference)
    {
        return $this->getModel()->getByReference($reference);
    }

    public function getByReferences(array $references)
    {
        return $this->getModel()->getByReferences($references);
    }

}