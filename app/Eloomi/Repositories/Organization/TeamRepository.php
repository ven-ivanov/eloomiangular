<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories\Organization;

use Cartalyst\Sentry\Users\UserNotFoundException;
use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Filereaders\Interfaces\CsvIOInterface;
use Eloomi\Models\Team;
use Eloomi\Repositories\Interfaces\Organization\TeamRepositoryInterface;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;
use Eloomi\Traits\FileReader;
use Eloomi\Repositories\CrudRepository;

class TeamRepository extends CrudRepository implements TeamRepositoryInterface {

    protected $id_needed_at_creation = true;
    protected $required_attributes_at_creation = ['name'];

    use FileReader;

    protected $users;

    /** @var \Eloomi\Filereaders\Interfaces\FileIOInterface[] */
    protected $fileReaders = [];

    public function __construct(
        UserRepositoryInterface $users,
        CsvIOInterface $csvIo,
        Team $model
    ){
        $this->users = $users;
        $this->fileReaders[] = $csvIo;
        $this->model = $model;
    }

    protected function update_non_fillables($team, array $info)
    {

        if (isset($info['leader_email'])){
            $info['leader'] = $this->users->getByEmail($info['leader_email']);
        }

        if (isset($info['leader_id'])){
            $info['leader'] = $this->users->getById($info['leader_id']);
        }

        if (isset($info['leader_reference'])){
            $info['leader'] = $this->users->getByReference($info['leader_reference']);
        }

        if (isset($info['leader'])){

            $team->leader()->associate($info['leader']);
        }

        if (isset($info['user_references'])) {
            if (!is_array($info['user_references'])) { $info['user_references'] = explode(',', $info['user_references']); }

            $users = $this->users->getByReferences($info['user_emails']);
            $row['user_ids'] = array_pluck($users, 'id');
        }

        if (isset($info['user_emails'])) {
            if (!is_array($info['user_emails'])) { $info['user_emails'] = explode(',', $info['user_emails']); }

            $users = $this->users->getByEmails($info['user_emails']);
            $row['user_ids'] = array_pluck($users, 'id');
        }

        if (isset($info['user_ids'])) {
            if(!is_array($info['user_ids'])) { $info['user_ids'] = explode(',', $info['user_ids']); }
            $team->users()->sync($info['user_ids']);

        }

        if (isset($info['course_ids'])) {
            if (!is_array($info['course_ids'])) { $info['course_ids'] = explode(',', $info['course_ids']); }

            $team->courses()->sync($info['course_ids']);
        }

        return $team;
    }

    public function search($term)
    {
        $teams = Team::with('users')
            ->where('name', 'like', "%$term%")
            ->select('name','users')
            ->get();

        return $teams;
    }



    public function importTeamsFromFile(File $file, $type)
    {

        $fileReader = $this->matchFileReaderType($type);
        $rows = $fileReader->fileToArray($file->getPathname());
        if(!$rows){
            throw new InvalidInputException('No teams were parsed');
        }

        \DB::beginTransaction();
        $errors = [];
        $newTeams = [];
        foreach ($rows as $index => $row) {

            try {
                $newTeams[] = $this->create($row);

            } catch(UserNotFoundException $e){ // Leader, or users cant be found
                $errors[] = [
                    [
                        'entity' => $row,
                        'message' => $e->getMessage()
                    ]
                ];
            } catch (\Exception $e) {
                \DB::rollback();
                $errors[] = [
                    [
                        'entity' => 'undefined',
                        'message' => $e->getMessage()
                    ]
                ];
                return [$errors, []];
            }
        }

        \DB::commit();

        return [$errors, $newTeams];
    }

    public function importUsersFromFile(Team $team, File $file, $type)
    {
        $fileReader = $this->matchFileReaderType($type);
        $rows = $fileReader->fileToArray($file->getPathname());
        if(!$rows){
            throw new InvalidInputException('No emails were parsed');
        }
        $emails = array_pluck($rows, 'email');
        $team = $this->update($team, [
            'user_emails'    => $emails
        ]);

        return $team;
    }

}