<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.50
 */

namespace Eloomi\Repositories\Organization;

use Eloomi\Exceptions\InvalidInputException;
use Eloomi\Filereaders\Interfaces\CsvIOInterface;
use Eloomi\Models\Company;
use Eloomi\Models\OrganizationalUnit;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;
use Eloomi\Traits\FileReader;
use Eloomi\Repositories\CrudRepository;

class OrganizationalUnitRepository extends CrudRepository implements OrganizationalUnitRepositoryInterface {

    use FileReader;

    protected $id_needed_at_creation = true;
    protected $required_attributes_at_creation = ['name'];

    protected $user_counts = [];
    protected $course_counts = [];
    protected $fileReaders = [];
    /**
     * @var Company
     */
    private $company;
    /**
     * @var User
     */
    private $user;

    public function __construct(
        OrganizationalUnit $organizationalUnit,
        CsvIOInterface $csvReader,
        Company $company,
        User $user
    ){
        $this->model = $organizationalUnit;
        $this->fileReaders[] = $csvReader;
        $this->company = $company;
        $this->user = $user;
    }

    public function getAll(OrganizationalUnit $root = null, $depth = null)
    {
        $level = $depth;
        if (is_null($root)){
            $units = $this->index();
        } else {
            $units = $root->getDescendantsAndSelf();
            if (!is_null($level)){
                $level += $root->getLevel();
            }

        }

        if (!is_null($level)){
            $units = $units->filter(function ($element) use ($level){
                return $element->getLevel() <= $level;
            });
        }

        return $units;
    }

    public function getTotalUserCount(OrganizationalUnit $ou){
        return $this->getTotal($ou, $this->user_counts, 'users');
    }

    protected function deleteByCode($code){
        $this->getByCode($code)->delete();
    }

    // TODO: To be optimized
    protected function getTotal($ou, &$cache, $attribute){
        if (isset($cache[$ou->id])){
            return $cache[$ou->id];
        }

        $cache[$ou->id] = $ou->{$attribute}->count();
        if ($ou->isLeaf()){
            $cache[$ou->id];
        }

        foreach ($ou->getImmediateDescendants() as $child) {
            $cache[$ou->id] += $this->getTotal($child, $cache, $attribute);
        }

        return $cache[$ou->id];
    }

    public function moveToNewParent(OrganizationalUnit $ou, OrganizationalUnit $parent)
    {
        $ou->makeChildOf($parent);
    }

    protected function update_non_fillables($ou, array $info)
    {

        if (isset($info['parent_code'])) {
            $info['parent'] = $this->getByCode($info['parent_code']);
        }

        if (isset($info['parent_id'])){
            $info['parent'] = $this->getById($info['parent_id']);
        }

        if (isset($info['leader_email'])) {

            $info['leader'] = $this->user->getByEmail($info['leader_email']);
        }

        if (isset($info['leader_id'])) {
            $info['leader'] = $this->user->getById($info['leader_id']);
        }

        if(isset($info['leader_reference'])) {
            $info['leader'] = $this->user->getByReference($info['leader_reference']);
        }

        if (isset($info['leader'])) {
            $ou->leader()->associate($info['leader']);
        }

        if (isset($info['user_emails'])) {
            if (!is_array($info['user_emails'])) { $info['user_emails'] = explode(',', $info['user_emails']); }
            $users = $this->user->getByEmails($info['user_emails']);
            $row['user_ids'] = array_pluck($users, 'id');
        }

        if (isset($info['user_ids'])) {
            $ou->users()->sync($info['user_ids']);
        }

        if (isset($info['course_ids'])){
            $ou->courses()->sync($info['course_ids']);
        }

        \DB::transaction(function() use ($ou, $info) {
            if (isset($info['parent'])) {
                $ou->save();
                $this->moveToNewParent($ou, $info['parent']);
            }
        });

        \DB::transaction(function() use ($ou, $info) {
            if (array_key_exists('parent_id', $info) && is_null($info['parent_id'])) {
                $ou->save();
                $ou->makeRoot();
            }
        });
        return $ou;

    }

    public function updateByCode($code, array $info){
        $unit = $this->getByCode($code);
        return $this->update($unit, $info);
    }

    public function importUnitsFromFile($file, $type)
    {
        $fileReader = $this->matchFileReaderType($type);
        $updates = $fileReader->fileToArray($file->getPathname());
        if(!$updates){
            throw new InvalidInputException('No units read from the file');
        }

        $delta_update = array_key_exists('change', $updates[0]);
        $units = [];
        $errors = [];
        if (!$delta_update){
            foreach($updates as $update) {
                $info = $this->updateRowInfo($update);
                list($errors[], $units[]) = $this->create($info);
            }

            return [$errors, $units];
        }

        \DB::beginTransaction();
        foreach ($updates as $index => $row) {
            try {
                switch ($row['change']){
                    case "add":
                        $units[] = $this->create($row);
                        break;
                    case "change":
                        $units[] = $this->updateByCode($row['code'], $row);
                        break;
                    case 'remove':
                        $this->deleteByCode($row['code']);
                        break;
                    default:
                        throw new InvalidInputException("Only 'add', 'change' and 'remove allowed for column 'change");
                }
            } catch(UserNotFoundException $e){ // Leader, or users cant be found
                $errors[] = [
                    [
                        'entity' => $index,
                        'message' => $e->getMessage()
                    ]
                ];
            } catch (\Exception $e) {
                \DB::rollback();
                $errors[] = [
                    [
                        'entity' => 'undefined',
                        'message' => $e->getMessage()
                    ]
                ];
                return [$errors, []];
            }
        }

        \DB::commit();

        return [$errors, $units];
    }

    public function importUsersFromFile(OrganizationalUnit $unit, $file, $type)
    {
        $fileReader = $this->matchFileReaderType($type);
        $updates = $fileReader->fileToArray($file->getPathname());
        if(!$updates){
            throw new InvalidInputException('No people read from the file');
        }

        $delta_update = array_key_exists('change', $updates[0]);

        $emails = array_pluck($updates, 'email');
        $users = User::getByEmails($emails);
        if (!$delta_update){
            foreach($users as $user){
                $unit->users()->save($user);
            }
            return $unit;
        }

        $updates = array_build($updates, function($key, $value){
           return [$value['email'], $value['change']];
        });

        $added = [];
        $removed = [];
        foreach ($users as $user) {

            $change = $updates[$user->email];
            if ($change == 'add') {
                $added[] = $user->id;
            } elseif ($change == 'remove') {
                $removed[] = $user->id;
            }
        }

        \DB::beginTransaction();
            $unit->users()->attach($added);
            $unit->users()->detach($removed);
        \DB::commit();

        return  $unit;

    }

}