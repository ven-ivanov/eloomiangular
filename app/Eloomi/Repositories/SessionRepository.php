<?php namespace Eloomi\Repositories;

use Eloomi\Exceptions\SessionExceptions;
use Eloomi\Repositories\Interfaces\SessionRepositoryInterface;
use Exception;
use Cartalyst\Sentry\Users as SentryUserExceptions;

class EloquentSessionRepository implements SessionRepositoryInterface{

    public function store($credentials, $rememberme)
    {
        try
        {
//            dd($credentials);
            // Authenticate the user
            $user = \Sentry::authenticate($credentials);
            \Sentry::login($user, $rememberme);
            \Event::fire('login', array($user));

        }
        catch (SentryUserExceptions\LoginRequiredException $exception)
        {
            $exception = new SentryUserExceptions\LoginRequiredException('Email is required');
        }
        catch (SentryUserExceptions\PasswordRequiredException $exception)
        {
            $exception = new SentryUserExceptions\PasswordRequiredException('Password is required');
        }
        catch (SentryUserExceptions\WrongPasswordException $exception)
        {
            $exception = new SentryUserExceptions\WrongPasswordException('Wrong password was provided');
        }
        catch (SentryUserExceptions\UserNotFoundException $exception)
        {
            $exception = new SentryUserExceptions\UserNotFoundException('Wrong email was provided');
        }
        catch (SentryUserExceptions\UserNotActivatedException $exception)
        {
            $exception = new SentryUserExceptions\UserNotActivatedException('User has not been activated');
        }
        catch (SentryUserExceptions\UserSuspendedException $exception)
        {
            $exception = new SentryUserExceptions\UserSuspendedException('User has been suspended');
        }
        catch (SentryUserExceptions\UserBannedException $exception)
        {
            $exception = new SentryUserExceptions\UserSuspendedException('User has been banned');
        }

        if (isset($exception)){
            throw $this->castSessionException($exception);
        }

        return $user;

    }

    public function destroy()
    {
        \Sentry::logout();
    }

    protected function castSessionException(Exception $exception){

        if ($exception instanceof SentryUserExceptions\LoginRequiredException ||
            $exception instanceof SentryUserExceptions\PasswordRequiredException)
        {
            return new SessionExceptions\MissingInputException($exception->getMessage());
        }

        if ($exception instanceof SentryUserExceptions\WrongPasswordException ||
            $exception instanceof SentryUserExceptions\UserNotFoundException)
        {
            // Overwriting because we are not interesting in giving too much info
            return new SessionExceptions\WrongInputException('User or password not correct');
        }

        if ($exception instanceof SentryUserExceptions\UserNotActivatedException){
            return new SessionExceptions\UserNotActivatedException($exception->getMessage());
        }

        if ($exception instanceof SentryUserExceptions\UserSuspendedException ||
            $exception instanceof SentryUserExceptions\UserBannedException){
            return new SessionExceptions\NotAllowedException($exception->getMessage());
        }

        return $exception;
    }
}