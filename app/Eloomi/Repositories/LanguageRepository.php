<?php namespace Eloomi\Repositories;

use AuraIsHere\LaravelMultiTenant\Exceptions\TenantModelNotFoundException;
use Eloomi\Models\Language;
use Eloomi\Repositories\Interfaces\LanguageRepositoryInterface;

class LanguageRepository extends CrudRepository implements LanguageRepositoryInterface{

    public function __construct(
        Language $model
    ){
        $this->model = $model;
    }

    public function getByCode($code){
        $language = Language::where('code', $code)->first();
        if (!$language){
            throw new TenantModelNotFoundException('No language was found with the code '. $code);
        }

        return $language;
    }

}