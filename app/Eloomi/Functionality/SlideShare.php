<?php namespace Eloomi\Functionality;
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 21/01/15
 * Time: 13.25
 */
use Curl;
use Config;
use SebastianBergmann\Exporter\Exception;

class SlideShare {

    const UPLOAD_URL = 'https://www.slideshare.net/api/2/upload_slideshow';
    const DELETE_URL = 'https://www.slideshare.net/api/2/delete_slideshow';

    protected $api_key;
    protected $shared_secret;
    protected $username;
    protected $password;
    /**
     * @var Curl
     */
    private $curl;

    public function __construct(
        Curl $curl
    ){
        $this->curl = $curl;
        $this->api_key = Config::get('slideshare.api_key');
        $this->shared_secret = Config::get('slideshare.shared_secret');
        $this->username = Config::get('slideshare.username');
        $this->password = Config::get('slideshare.password');

        $this->apiurl='https://www.slideshare.net/api/1/';
        $this->api_posturl='/api/1/';
        $this->api_postserver='www.slideshare.net';
        $this->api_postport=443;
    }

    /**
     * @param $path Path to the presentation
     */
    public function upload (
        $path
    ){
        $params = [
            'username' => $this->username,
            'password' => $this->password,
            'slideshow_title' => 'Slideshow',
            'make_src_public' => 'N',
            'make_slideshow_private' => 'Y',
            'generate_secret_url' => 'N',
            'allow_embeds' => 'Y',
            'share_with_contacts' => 'N',
        ];
        $files['slideshow_srcfile'] = $path;

        list(, $body) = explode("\r\n\r\n",$this->post_data("upload_slideshow", $params, $files));
        $parsedResponse = $this->parseResponse($body);
        if (!isset($parsedResponse['SlideShowID'])){
            \Log::error($body);
            throw new \ErrorException("Slide not uploaded correctly to Slideshare");
        }

        return $parsedResponse['SlideShowID'];
    }

    public function delete(
        $slideshare_id
    ){
        $params = [
            'slideshow_id' => $slideshare_id,
        ];

        $this->get_data("delete_slideshow", $params);

    }

    protected function parseResponse($body){
        return \Parser::xml($body);
    }


    // Copy pasted from slideshare-phpkit - not sure how it works
    protected function post_data($call, $params, $files) {
        $standard_params = $this->createStandardParams();
        $params = array_merge($standard_params, $params);
        $url = $this->api_posturl.$call;
        // create boundary
        srand((double)microtime()*1000000);
        $boundary = "---------------------".substr(md5(rand(0,32000)),0,10);
        // build header
        $header = "POST $url HTTP/1.0\r\n";
        $header .= "Host: $this->api_postserver\r\n";
        $header .= "Content-type: multipart/form-data, boundary=$boundary\r\n";
        $data = '';
        foreach($params AS $index => $value) {
            $data .="--$boundary\r\n";
            $data .= "Content-Disposition: form-data; name=\"".$index."\"\r\n";
            $data .= "\r\n".$value."\r\n";
            $data .="--$boundary\r\n";
        }
        foreach($files AS $index => $value) {
            $data .= "--$boundary\r\n";
            $content_file = join("", file($value));
            $name_file = strchr($value,"/");
            if ($name_file === false)
                $name_file = $value;
            else $name_file = substr($name_file,1);
            $data .="Content-Disposition: form-data; name=\"$index\"; filename=\"$name_file\"\r\n";
            $data .= "Content-Type: application/octet-stream\r\n\r\n";
            $data .= "".$content_file."\r\n";
            $data .="--$boundary--\r\n";
        }
        $header .= "Content-length: " . strlen($data) . "\r\n\r\n";
        try {
            $fp = fsockopen('ssl://' . $this->api_postserver, $this->api_postport);
            fputs($fp, $header.$data);
            $response = @stream_get_contents($fp);
            fclose($fp);
        } catch (Exception $e) {
            $response = "";
        }
        return utf8_encode($response);
    }

    protected function get_data($call,$params) {
        $standard_params = $this->createStandardParams();
        $url_params = http_build_query(array_merge($standard_params, $params));

        try {
            $res=file_get_contents($this->apiurl.$call . "?" . $url_params);
        } catch (Exception $e) {
            // Log the exception and return $res as blank
        }
        return utf8_encode($res);
    }

    private function createStandardParams(){
        $params['ts'] = time();
        $params['hash'] = sha1($this->shared_secret . $params['ts']);
        $params['api_key'] = $this->api_key;
        $params['username'] = $this->username;
        $params['password'] = $this->password;

        return $params;
    }
}
