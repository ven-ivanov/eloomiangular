<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 30/12/14
 * Time: 15.34
 */

namespace Eloomi\Functionality\Notifiers;

use Eloomi\Functionality\Receivers\ReceiverInterface;


abstract class BaseNotifier {

    protected $receiver;
    protected $message;

    public function setReceiver(ReceiverInterface $receiver){

        $this->receiver = $receiver;
        return $this;
    }

    public function setMessage($msg){

        $this->message = $msg;
        return $this;
    }

    abstract public function notify();

}