<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 17/12/14
 * Time: 14.05
 */

namespace Eloomi\Observers;

use Eloomi\Validators\UserValidator;
use Eloomi\Models\User;

class CourseObserver extends BaseObserver {

    public function __construct(CourseValidator $validator)
    {
        $this->validator = $validator;
    }

    public function deleted($model){
        $model->modules()->delete();
    }
}