<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 17/12/14
 * Time: 14.53
 */

namespace Eloomi\Observers;

use Eloomi\Models\User;
use Eloomi\Validators\BaseValidator;

class BaseObserver {

    protected $validator;

    public function __construct(BaseValidator $validator)
    {
        $this->validator = $validator;
    }

    public function saving($model){

        foreach ($model->toArray() as $name => $value) {
            if (empty($value)) {
                $model->{$name} = null;
            }
        }

        $this->validator->validateModel($model);

    }

    public function saved($model){}

    public function updating($model){}

    public function updated($model){}

    public function creating($model){}

    public function created($model){}

    public function deleting($model){}

    public function deleted($model){}

    public function restoring($model){}

    public function restored($model){}

}