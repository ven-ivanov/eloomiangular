<?php namespace Eloomi\Transformers;

use Eloomi\Models\Certificate;
use League\Fractal;

class CertificateTransformer extends Fractal\TransformerAbstract{

	/**
	 * @var \FileController
	 */
	private $files;

	public function __construct(
		\FileController $files
	){

		$this->files = $files;
	}

	public function transform(Certificate $certificate){

		if (is_null($certificate->file_id)) {

		}
		$path = is_null($certificate->file_id) ? null : $this->files->getUrlForFile($certificate->file_id);

		return [
			'id' 		  => (int) $certificate->id,
			'description' => (string) $certificate->description,
	        'path'        => (string) $path,
	    ];
	}

}