<?php namespace Eloomi\Transformers;

use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use League\Fractal;

class ProgressTransformer extends Fractal\TransformerAbstract{

	/**
	 * @var CourseTransformer
	 */
	private $courseTransformer;

	protected $defaultIncludes =[
		'course'
	];
	/**
	 * @var CourseRepositoryInterface
	 */
	private $courses;

	public function __construct(
		CourseTransformer $courseTransformer,
		CourseRepositoryInterface $courses
	){
		$this->courseTransformer = $courseTransformer;
		$this->courses = $courses;
	}

	public function transform(array $info){
		return [
			'modules_finished' => $info['modules_finished'],
		];
	}

	public function includeCourse(array $info)
	{
		$course = $this->courses->getById($info['course_id']);
		return $this->item($course, $this->courseTransformer);
	}

}