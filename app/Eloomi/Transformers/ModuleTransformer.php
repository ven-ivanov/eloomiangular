<?php namespace Eloomi\Transformers;

use Eloomi\Models\Interfaces\ModuleInterface;
use Eloomi\Models\Module;
use League\Fractal;

class ModuleTransformer extends Fractal\TransformerAbstract{

	protected $availableIncludes = [
		'type_specifics'
	];

	public function transform(Module $module){
		return [
			'id' => $module->id,
			'name' => $module->name,
			'description' => $module->description,
			'is_skippable' => (boolean) $module->is_skippable,
			'order' => $module->order,
			'module_type' => $module->getType(),
		];
	}

	public function includeTypeSpecifics(Module $module)
	{
		$teachable = $module->teachable;
		return $this->item($teachable, $teachable->getTransformer());
	}
}