<?php namespace Eloomi\Transformers;

use Eloomi\Models\MultipleChoiceAnswer;
use League\Fractal;

class MultipleChoiceAnswerTransformer extends Fractal\TransformerAbstract{

	public function transform(MultipleChoiceAnswer $answer){
		return [
			'id'		 => $answer->id,
			'answer'	 => $answer->answer,
			'is_correct' => (boolean) $answer->is_correct
		];
	}
}