<?php namespace Eloomi\Transformers;

use Eloomi\Models\FreeTextQuestion;
use Eloomi\Models\RatingQuestion;
use League\Fractal;

class RatingTransformer extends Fractal\TransformerAbstract{

	public function transform(RatingQuestion $question){
		return [
			'min' => $question->min,
			'max' => $question->max
		];
	}

}