<?php namespace Eloomi\Transformers;

use Cartalyst\Sentry\Groups\Eloquent\Group;
use Eloomi\Data\Permissions;
use Eloomi\Models\User;
use League\Fractal;

class UserTransformer extends BaseTransformer {

	protected $availableIncludes = [
		'language',
		'unit_name',
		'image',
		'permissions'
	];

	private $cross_company_admin;
	private $company_admin;
	private $user;

	/**
	 * @var FileTransformer
	 */
	private $fileTransformer;

	public function __construct(
		LanguageTransformer $languageTransformer,
		FileTransformer $fileTransformer
	){
		$this->languageTransformer = $languageTransformer;
		$this->fileTransformer = $fileTransformer;

		$this->cross_company_admin = \Sentry::findGroupByName(Permissions::CROSS_COMPANY_ADMIN);
		$this->company_admin 	   = \Sentry::findGroupByName(Permissions::COMPANY_ADMIN);
		$this->user 			   = \Sentry::findGroupByName(Permissions::COMPANY_USER);
	}

	public function transform(User $user){
		$birthday = $user->birthday ? $user->birthday->format("Y-m-d") : null;
		$permission =
			($user->inGroup($this->cross_company_admin) ?
				Permissions::CROSS_COMPANY_ADMIN :
				($user->inGroup($this->company_admin) ?
					Permissions::COMPANY_ADMIN :
					($user->inGroup($this->user) ?
						Permissions::COMPANY_USER :
						null
					)
				)
			);

		return [
			'id'		 	=> $user->id,
			'email'			=> $user->email,
			'first_name'	=> $user->first_name,
			'last_name'     => $user->last_name,
			'birthday'      => $birthday,
			'gender'        => $user->gender,
			'title'      	=> $user->title,
			'phone'      	=> $user->phone,
			'timezone'      => $user->timezone,
			'active'		=> $user->activeState(),
			'permission'	=> $permission
		];
	}

	public function includePermissions(User $user){

	}

	public function includeImage(User $user){

		if (is_null($user->image_id)) { return; }

		$user->load('image');
		return $this->item($user->image, $this->fileTransformer);
	}

	public function includeLanguage(User $user)
	{
		$language = $user->language;
		if (is_null($language)){
			return null;
		}
		return $this->item($language, $this->languageTransformer);
	}

	public function includeUnitName(User $user)
	{
		$unit = $user->organizational_unit();
		if (is_null($unit)){
			return null;
		}

		return $this->item($unit, function($unit){
			return ['name' => $unit->name];
		});
	}

}