<?php namespace Eloomi\Transformers;

use Eloomi\Models\Timespan;
use League\Fractal;

class TimespanTransformer extends BaseTransformer {

	public function transform(Timespan $user){

		return [
			'id'		 	=> $user->id,
			'string'		=> $user->human_string,
		];
	}
}