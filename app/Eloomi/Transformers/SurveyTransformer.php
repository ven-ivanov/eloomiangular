<?php namespace Eloomi\Transformers;

use Eloomi\Models\Interfaces\ModuleInterface;
use Eloomi\Models\Module;
use Eloomi\Models\Survey;
use Eloomi\Models\Vimeo;
use League\Fractal;

class SurveyTransformer extends Fractal\TransformerAbstract{

	/**
	 * @var
	 */
	protected $questionTransformer;

	public function __construct(
		QuestionTransformer $questionTransformer
	){

		$this->questionTransformer = $questionTransformer;
	}

	protected $availableIncludes = [
		'questions'
	];

	public function transform(Survey $survey){

		return [
			'pass_percentage' => $survey->pass_percentage
		];
	}

	public function includeQuestions(Survey $module)
	{
		$teachable = $module->questions()->getQuery()->orderBy('order')->get();
		return $this->collection($teachable, $this->questionTransformer);
	}
}