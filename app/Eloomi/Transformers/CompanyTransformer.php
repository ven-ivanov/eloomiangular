<?php namespace Eloomi\Transformers;

use Eloomi\Models\Company;
use Eloomi\Models\User;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use League\Fractal;

class CompanyTransformer extends Fractal\TransformerAbstract{

	protected $defaultIncludes = [
		'language',
		'app_parts'
	];

	protected $availableIncludes = [
		'admin_attributes'
	];
	/**
	 * @var LanguageTransformer
	 */
	private $languageTransformer;
	/**
	 * @var AppPartTransformer
	 */
	private $appPartTransformer;
	/**
	 * @var ArrayTransformer
	 */
	private $arrayTransformer;
	/**
	 * @var User
	 */
	private $user;

	public function __construct(
		LanguageTransformer $languageTransformer,
		AppPartTransformer $appPartTransformer,
		ArrayTransformer $arrayTransformer,
		User $user
	){

		$this->languageTransformer = $languageTransformer;
		$this->appPartTransformer = $appPartTransformer;
		$this->arrayTransformer = $arrayTransformer;
		$this->user = $user;
	}

	public function transform(Company $company){

		return [
	        'id'		=> $company->id,
			'name'		=> $company->name,
			'color'		=> $company->color,
			'domain'	=> $company->domain,
			'logo'		=> \FileController::getUrlForFile($company->image_id)
	    ];
	}

	public function includeLanguage(Company $company){
		$language = $company->language()->get()->first();
		if (is_null($language)) { return; }
		return $this->item($language, $this->languageTransformer);
	}

	public function includeAdminAttributes(Company $company){
		\TenantScope::disable();
		$total_users = $this->user->whereCompanyId($company->id)->count();
		$activated_users = $this->user->whereCompanyId($company->id)->whereActivated(true)->count();
		\TenantScope::enable();

		return $this->item([
			'bought_courses' => $company->bought_courses()->count(),
			'created_at'   	 => $company->created_at->format("Y-m-d"),
			'all_users'      => $total_users,
			'active_users'   => $activated_users,
			'address'	   	 => $company->address,
			'zip'		  	 => $company->zip,
			'city'		  	 => $company->city,
			'cvr'		  	 => $company->cvr,
			'phone'		  	 => $company->phone,
			'homepage'	  	 => $company->homepage,
			'contact'	  	 => $company->contact_name
		],$this->arrayTransformer);
	}

	public function includeAppParts(Company $company){
		$company->load('app_parts');
		return $this->collection($company->app_parts, $this->appPartTransformer);
	}

}