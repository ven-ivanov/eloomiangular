<?php namespace Eloomi\Transformers;

use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;
use Eloomi\Models\OrganizationalUnit;
use League\Fractal;

class OrganizationalUnitTransformer extends Fractal\TransformerAbstract{

	protected $userTransformer;
	protected $nullTransformer;
	protected $ouRepository;

	protected $availableIncludes = [
		'users',
		'user_count',
		'leader',
		'courses',
		'course_count',
	];
	/**
	 * @var CourseTransformer
	 */
	private $courseTransformer;

	public function __construct(
		UserTransformer $userTransformer,
		ArrayTransformer $nullTransformer,
		OrganizationalUnitRepositoryInterface $ouRepository,
		CourseTransformer $courseTransformer
	){
		$this->userTransformer = $userTransformer;
		$this->nullTransformer = $nullTransformer;
		$this->ouRepository = $ouRepository;
		$this->courseTransformer = $courseTransformer;
	}

	public function transform(OrganizationalUnit $organizationalUnit){
		return [
			'id'		=> $organizationalUnit->id,
			'code'		=> $organizationalUnit->code,
	        'name'      => $organizationalUnit->name,
			'parent_id' => $organizationalUnit->parent_id,
			'depth'		=> $organizationalUnit->depth
	    ];
	}

	public function includeUsers(OrganizationalUnit $organizationalUnit)
	{
		$users = $organizationalUnit->users;

		return $this->collection($users, $this->userTransformer);
	}

	public function includeCourses(OrganizationalUnit $organizationalUnit)
	{
		$courses = $organizationalUnit->courses;

		return $this->collection($courses, $this->courseTransformer);
	}

	public function includeUserCount(OrganizationalUnit $organizationalUnit)
	{
		$userCounts =[
			'total' => $this->ouRepository->getTotalUserCount($organizationalUnit),
			'this'  => $organizationalUnit->users()->count()
		];

		return $this->item($userCounts, function(array $data){
			return $data;
		});
	}

	public function includeCourseCount(OrganizationalUnit $organizationalUnit)
	{
		$userCounts =[
			'this'  => $organizationalUnit->courses()->count()
		];

		return $this->item($userCounts, function(array $data){
			return $data;
		});
	}

	public function includeLeader(OrganizationalUnit $organizationalUnit)
	{
		$leader = $organizationalUnit->leader;
		if (is_null($leader)){
			return;
		}

		return $this->item($leader, $this->userTransformer);
	}
}