<?php namespace Eloomi\Transformers;

use League\Fractal\TransformerAbstract;

class BaseTransformer extends TransformerAbstract{

    protected function hasProperty($class, $property){
        try {
            $class->{$property};
            return true;
        }
        catch (\ErrorException $e){
            return false;
        }
    }
}