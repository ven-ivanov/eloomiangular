<?php namespace Eloomi\Transformers;

use Eloomi\Models\Interfaces\ModuleInterface;
use Eloomi\Models\Module;
use Eloomi\Models\Vimeo;
use League\Fractal;

class VimeoTransformer extends Fractal\TransformerAbstract{

	public function transform(Vimeo $vimeo){

		return [
			'path' => $vimeo->path
		];
	}
}