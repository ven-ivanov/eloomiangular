<?php namespace Eloomi\Transformers;

use Eloomi\Models\FreeText;
use League\Fractal;

class FreeTextTransformer extends Fractal\TransformerAbstract{

	public function transform(FreeText $module){

		return [
			'text' => $module->text,
			'image' => \FileController::getUrlForFile($module->image_id)
		];
	}
}