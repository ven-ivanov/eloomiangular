<?php namespace Eloomi\Transformers;

use Eloomi\Models\Course;
use Eloomi\Models\File;
use Eloomi\Repositories\Interfaces\LMS\CourseRepositoryInterface;
use League\Fractal;

class FileTransformer extends Fractal\TransformerAbstract{

	public function transform(File $file){
		return [
			'id'   => $file->id,
			'path' => \FileController::getUrlForFile($file->id)
		];
	}

}