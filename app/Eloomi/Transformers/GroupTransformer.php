<?php namespace Eloomi\Transformers;

use Cartalyst\Sentry\Groups\Eloquent\Group;
use League\Fractal;

class GroupTransformer extends Fractal\TransformerAbstract{

	public function transform(Group $group){

		return [
			'id'   => (int) $group->id,
	        'name' => (string) $group->name,
	    ];
	}

}