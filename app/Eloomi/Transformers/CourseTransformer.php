<?php namespace Eloomi\Transformers;

use Eloomi\Models\Course;
use Eloomi\Repositories\Interfaces\Organization\UserRepositoryInterface;
use League\Fractal;

class CourseTransformer extends Fractal\TransformerAbstract{

	protected $userTransformer;
	protected $nullTransformer;
	protected $timespanTransformer;

	protected $availableIncludes = [
		'users',
		'units',
		'teams',
		'certificate',
		'my_progress',
		'image',
		'modules'
	];

	protected $defaultIncludes = [
		'valid_for'
	];
	/**
	 * @var TeamTransformer
	 */
	private $teamTransformer;
	/**
	 * @var UserRepositoryInterface
	 */
	private $users;
	/**
	 * @var FileTransformer
	 */
	private $files;
	/**
	 * @var CertificateTransformer
	 */
	private $certificateTransformer;
	/**
	 * @var ModuleTransformer
	 */
	private $modules;

	public function __construct(
		UserTransformer $userTransformer,
		TeamTransformer $teamTransformer,
		ArrayTransformer $nullTransformer,
		TimespanTransformer $timespanTransformer,
		UserRepositoryInterface $users,
		FileTransformer $files,
		CertificateTransformer $certificateTransformer,
		ModuleTransformer $modules

	){
		$this->userTransformer = $userTransformer;
		$this->nullTransformer = $nullTransformer;
		$this->timespanTransformer = $timespanTransformer;
		$this->teamTransformer = $teamTransformer;
		$this->users = $users;
		$this->files = $files;
		$this->certificateTransformer = $certificateTransformer;
		$this->modules = $modules;
	}

	public function transform(Course $course){
		$due_date = $course->due_date;
		if ($due_date){
			$due_date = $course->due_date->format('Y-m-d');
		}

		return [
			'id' 		   => (int) $course->id,
	        'name'         => (string) $course->name,
			'code'		   => (string) $course->code,
			'description'  => (string) $course->description,
			'is_active'    => (boolean) $course->active,
			'points'       => (integer) $course->points,
			'price'        => $course->price,
			'in_order' 	   => (boolean) $course->in_order,
			'due_date' 	   => $due_date,
			'module_count' => $course->modules()->count()
	    ];
	}

	public function includeCertificate(Course $course){
		if (is_null($course->certificate_id)){
			return null;
		}

		$course->load('certificate');
		return $this->item($course->certificate, $this->certificateTransformer);
	}

	public function includeImage(Course $course)
	{
		$course->load('image');
		if (is_null($course->image)){
			return null;
		}

		return $this->item($course->image, $this->files);
	}

	public function includeModules(Course $course)
	{
		$course->load('modules');
		if (is_null($course->modules)){
			return null;
		}

		return $this->collection($course->modules, $this->modules);
	}

	public function includeValidFor(Course $course)
	{
		$course->load('lasts_for');
		$valid_for = $course->lasts_for;
		if (is_null($valid_for)){
			return null;
		}

		return $this->item($valid_for, $this->timespanTransformer);
	}

	public function includeUsers(Course $course){
		return $this->collection($course->users, $this->userTransformer);
	}

	public function includeUnits( Course $course ){
		$course->load('organizational_units');
		return $this->collection($course->organizational_units, function($organizationalUnit){
			return [
				'id'		=> $organizationalUnit->id,
				'code'		=> $organizationalUnit->code,
				'name'      => $organizationalUnit->name,
				'parent_id' => $organizationalUnit->parent_id,
			];
		});
	}

	public function includeTeams(Course $course){
		return $this->collection($course->teams, $this->teamTransformer);
	}

	public function includeMyProgress(Course $course){
		$progress = $this->users->getMyProgress($course);
		return $this->item($progress, function($progress){
			return ['finished_modules' => $progress];
		});
	}

}