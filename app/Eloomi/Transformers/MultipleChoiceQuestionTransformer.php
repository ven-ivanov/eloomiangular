<?php namespace Eloomi\Transformers;

use Eloomi\Models\Interfaces\ModuleInterface;
use Eloomi\Models\Module;
use Eloomi\Models\MultipleChoiceQuestion;
use League\Fractal;

class MultipleChoiceQuestionTransformer extends Fractal\TransformerAbstract{

	protected $defaultIncludes = [
		'answers'
	];

	public function transform(MultipleChoiceQuestion $module){
		return [];
	}

	public function includeAnswers(MultipleChoiceQuestion $module)
	{
		$anwsers = $module->answers;
		return $this->collection($anwsers, \App::make('Eloomi\Transformers\MultipleChoiceAnswerTransformer'));
	}
}