<?php namespace Eloomi\Transformers;

use Eloomi\Models\AppPart;
use League\Fractal;

class AppPartTransformer extends BaseTransformer {

	public function transform(AppPart $app_part){
		return [
			'id' => $app_part->id,
			'name' => $app_part->name
		];
	}
}