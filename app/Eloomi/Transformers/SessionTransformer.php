<?php namespace Eloomi\Transformers;

use League\Fractal;

class SessionTransformer extends Fractal\TransformerAbstract{

	protected $user;

	public function __construct(UserTransformer $user){
		$this->user = $user;
	}

	public function transform(SessionInterface $session){
		return [
			'user'		 => $this->user->transform($session->user),
	    ];
	}
}