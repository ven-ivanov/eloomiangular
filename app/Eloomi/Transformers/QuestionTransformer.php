<?php namespace Eloomi\Transformers;

use Eloomi\Models\Question;
use League\Fractal;

class QuestionTransformer extends BaseTransformer {

	protected $defaultIncludes = [
		'specifics'
	];

	public function transform(Question $question){
		return [
			'id'		=> $question->id,
			'question' 	=> $question->question,
			'order'    	=> $question->order,
			'type'		=> $question->getType()
		];
	}

	public function includeSpecifics(Question $question)
	{
		$questionable = $question->questionable;
		return $this->item($questionable, $questionable->getTransformer());
	}

}