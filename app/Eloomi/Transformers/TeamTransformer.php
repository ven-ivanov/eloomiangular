<?php namespace Eloomi\Transformers;

use Eloomi\Repositories\Interfaces\Organization\OrganizationalUnitRepositoryInterface;
use Eloomi\Models\Team;
use League\Fractal;

class TeamTransformer extends Fractal\TransformerAbstract{

	protected $userTransformer;
	protected $nullTransformer;

	protected $availableIncludes = [
		'leader',
		'users',
		'user_count',
		'courses',
		'course_count'
	];
	/**
	 * @var CourseTransformer
	 */
	protected $courseTransformer;

	public function __construct(
//		CourseTransformer $courseTransformer,
		UserTransformer $userTransformer,
		ArrayTransformer $nullTransformer
	){
		$this->userTransformer = $userTransformer;
		$this->nullTransformer = $nullTransformer;
//		$this->courseTransformer = $courseTransformer;
	}

	public function transform(Team $organizationalUnit){
		return [
			'id'		  => $organizationalUnit->id,
	        'name'        => $organizationalUnit->name,
			'description' => $organizationalUnit->description,
	    ];
	}

	// Mutual dependancy between TeamTransformer and CourseTransformer made me do this non-DRY approach
	public function includeCourses(Team $team){

		$courses = $team->courses;
		return $this->collection($courses, function($course){
			$due_date = $course->due_date;
			if ($due_date){
				$due_date = $course->due_date->format('Y-m-d');
			}
			$certificate = $course->certificate_id ? \FileController::getUrlForFile($course->certificate_id) : null;
			return [
				'id' 		  => (int) $course->id,
				'name'        => (string) $course->name,
				'code'		  => (string) $course->code,
				'is_active'   => (boolean) $course->active,
				'in_order' 	  => (boolean) $course->in_order,
				'due_date' 	  => $due_date,
				'certificate' => $certificate,
				'image' 	  => \FileController::getUrlForFile($course->image_id),
			];
		});

	}

	public function includeUsers(Team $team)
	{
		$users = $team->users;
		return $this->collection($users, $this->userTransformer);
	}

	public function includeCourseCount(Team $team){

		$count = $team->courses()->count();
		return $this->item($count, function($count){
			return [ 'count' => $count ];
		});

	}

	public function includeUserCount(Team $team)
	{
		$count = $team->users()->count();
		return $this->item($count, function($count){
			return [ 'count' => $count ];
		});
	}

	public function includeLeader(Team $team)
	{
		$leader = $team->leader;
		if (is_null($leader)){
			return;
			return $this->item(null, $this->nullTransformer);
		}

		return $this->item($leader, $this->userTransformer);
	}
}