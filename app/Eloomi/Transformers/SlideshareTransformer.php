<?php namespace Eloomi\Transformers;

use Eloomi\Models\Slideshare;
use League\Fractal;

class SlideshareTransformer extends Fractal\TransformerAbstract{

	public function transform(Slideshare $presentation){
		return [
			'slideshare_id'   => $presentation->slideshare_id,
		];
	}

}