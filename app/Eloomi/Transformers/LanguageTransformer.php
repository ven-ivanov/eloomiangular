<?php namespace Eloomi\Transformers;

use Eloomi\Models\Language;
use League\Fractal;

class LanguageTransformer extends BaseTransformer {

	public function transform(Language $language){

		if (!$language){
			return null;
		}

		return [
			'id'	=> $language->id,
			'name'	=> $language->name,
			'code'  => $language->code
		];
	}
}