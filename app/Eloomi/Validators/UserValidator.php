<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 17/12/14
 * Time: 13.42
 */


namespace Eloomi\Validators;

use Eloomi\Exceptions\UserExceptions\ValidationException;
use Illuminate\Support\Facades\Validator;

class UserValidator extends BaseValidator {

    protected $rules = [
        'email'         => 'required|email',
        'first_name'    => 'required',
        'last_name'     => 'required',
        'timezone'      => 'timezone',
        'password'      => 'min:8',
        'language_id'   => 'exists:languages,id',
        'image_id'      => 'exists:files,id',
        'birthday'      => 'min:5',
        'title'         => 'min:2',
        'phone'         => 'min:2',
    ];

    public function validateModel($model){

        if (is_null($model->id)){
            $this->rules['password'] .= '|required';
        }

        // TODO: A hack to check password confirmation even though it is hashed
        if (array_key_exists('password', $model->getDirty()) && !$model->checkHash($model->password_confirmation, $model->password)){

            // This will always produce an error, which is why we check explicitly first - This will result in correct feedback
            $this->rules['password'] .= '|confirmed';
        }

        $validator = Validator::make(
            $model->getAttributes(),
            $this->rules

        );

        if ($validator->fails())
        {
            throw new ValidationException('Validation failed', $validator->messages()->getMessages());
        }
    }

    public function validateArray($array){
        throw new \RuntimeException('Unable to validate a user array due to hashing issues');

    }

}