<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 17/12/14
 * Time: 13.44
 */

namespace Eloomi\Validators;


class ValidationException extends \Exception {

    protected $errors;

    public function __construct($msg, Array $errors){
        parent::__construct($msg);
        $this->errors = $errors;
    }

    public function getErrors(){
        return $this->errors;
    }
}