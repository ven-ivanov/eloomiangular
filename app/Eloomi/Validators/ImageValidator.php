<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 17/12/14
 * Time: 13.42
 */


namespace Eloomi\Validators;

class ImageValidator extends BaseValidator {

    protected $max_size = null;
    protected $min_size = null;

    protected $rules = [
        'image'         => 'required|image',
    ];

    public function validateModel($file){
        $this->validateArray(['image' => $file]);

    }

    public function setMaxSize($max){
        $this->max_size = $max;

        return $this;
    }

    public function setMinSize($min){
        $this->min_size = $min;

        return $this;
    }

    protected function setCustomRules($array){
        if (!is_null($this->min_size)){
            $this->rules['image'] .= "|min:{$this->min_size}";
        }

        if (!is_null($this->max_size)){
            $this->rules['image'] .= "|max:{$this->max_size}";
        }
    }

}