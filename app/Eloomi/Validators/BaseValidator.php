<?php
/**
 * Created by PhpStorm.
 * User: RickMarker
 * Date: 17/12/14
 * Time: 14.56
 */

namespace Eloomi\Validators;
use Validator;


abstract class BaseValidator {

    protected $rules = [];

    public function validateModel($model){
        $this->validateArray($model->getAttributes());
    }

    public function validateArray($array){
        $this->setCustomRules($array);

        $validator = Validator::make(
            $array,
            $this->rules

        );

        if ($validator->fails())
        {
            throw new ValidationException('Validation failed', $validator->messages()->getMessages());
        }
    }

    protected function setCustomRules($array) { }
}