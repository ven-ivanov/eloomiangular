<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
Hi {{ $first_name }}<br />
You can reset your password with the code below:<br />
{{ $password_reset_link }}
<div>
</div>
</body>
</html>
