Hi
{{$user['first_name']}} {{$user['last_name']}} needs your help to continue evolving.
To be able to continue learning {{$user['first_name']}} would like to take the course "{{$course['name']}}"

This is only possible if it is bought by an admin.

Do you think {{$user['first_name']}} could benefit from it?
Then buy it at the course shop!