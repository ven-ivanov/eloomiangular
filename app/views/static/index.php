<!DOCTYPE html>
<html lang="en" ng-app="xenon-app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="eloomi e-learning website" />
    <meta name="author" content="Purplecow" />

    <title>eloomi</title>
    <link rel="shortcut icon" href="favicon.ico" />

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="assets/css/fonts/linecons/css/linecons.css">
    <link rel="stylesheet" href="assets/css/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/fonts/fonts.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/xenon-core.css">
    <link rel="stylesheet" href="assets/css/xenon-forms.css">
    <link rel="stylesheet" href="assets/css/xenon-components.css">
    <link rel="stylesheet" href="assets/css/xenon-skins.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/test1.css">

    <!-- TABLE CSS -->
    <link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
    <!-- NESTABLE CSS -->
    <link rel="stylesheet" href="assets/js/uikit/uikit.css">
    <!-- Dropzone -->
    <link rel="stylesheet" href="assets/js/dropzone/css/dropzone.css">
    <!-- Datepicker -->
    <link rel="stylesheet" href="assets/js/daterangepicker/daterangepicker-bs3.css">


    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <script>
        var appHelper = {
            // Vars (paths without trailing slash)
            templatesDir: 'app/tpls',
            assetsDir: 'assets',

            // Methods
            templatePath: function(view_name)
            {
                return this.templatesDir + '/' + view_name + '.html';
            },

            assetPath: function(file_path)
            {
                return this.assetsDir + '/' + file_path;
            }
        };
    </script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="page-body" ng-controller="MainCtrl" ng-class="{'settings-pane-open': layoutOptions.settingsPane.isOpen, 'chat-open': layoutOptions.chat.isOpen, 'login-page': isLoginPage && isMainPage == false, 'login-light': isLightLoginPage && isMainPage == false, 'lockscreen-page': isLockscreenPage && isMainPage == false, 'right-sidebar': layoutOptions.sidebar.isRight, 'boxed-container': layoutOptions.container.isBoxed}">


<!-- 	<settings-pane></settings-pane>

	<horizontal-menu ng-if="layoutOptions.horizontalMenu.isVisible"></horizontal-menu> -->

<div class="" ui-view></div>

<!-- Remove this code if you want to disable Loading Overlay in the beginning of document loading -->
<div class="page-loading-overlay {{isLoading}}">
    <div class="loader-2"></div>
</div>

<!-- Bottom Scripts -->
<script src="app/js/angular/angular.min.js"></script>
<script src="app/js/angular-ui/angular-ui-router.min.js"></script>
<script src="app/js/angular/angular-animate.js"></script>
<script src="app/js/angular/angular-route.js"></script>
<script src="app/js/angular/angular-breadcrumb.js"></script>
<script src="assets/js/toastr/toastr.min.js"></script>

<!-- custom directives and models -->
<script src="app/js/angular/checklist-model.js"></script>

<script src="app/js/angular/ng-flow-standalone.js"></script>
<script src="app/js/angular/ng-flow.js"></script>

<script src="app/js/angular/autoActive.js"></script>
<script src="app/js/angular-ui/ui-bootstrap-tpls-0.11.2.min.js"></script>
<script src="app/js/angular/angular-cookies.min.js"></script>
<script src="app/js/oc-lazyload/ocLazyLoad.min.js"></script>
<script src="app/js/angular-fullscreen.js"></script>
<script src="assets/js/TweenMax.min.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>

<!-- External Libraries -->
<script src="app/js/underscore/underscore.js"></script>


<!-- UIKIT -->
<script src="assets/js/uikit/js/uikit.min.js"></script>
<script src="assets/js/uikit/js/addons/nestable.min.js"></script>

<!-- JavaScripts initializations and stuff -->
<script src="assets/js/xenon-custom.js"></script>

<!-- Dropzone -->
<script src="assets/js/dropzone/dropzone.min.js"></script>

<!-- Datepicker -->
<script src="assets/js/daterangepicker/daterangepicker.js"></script>
<script src="assets/js/datepicker/bootstrap-datepicker.js"></script>

<!-- Widgets -->
<script src="assets/js/xenon-widgets.js"></script>
<script src="assets/js/devexpress-web-14.1/js/knockout-3.1.0.js"></script>
<script src="assets/js/devexpress-web-14.1/js/globalize.min.js"></script>
<script src="assets/js/devexpress-web-14.1/js/dx.chartjs.js"></script>

<!-- Validator JS -->
<script src="assets/js/jquery-validate/jquery.validate.min.js"></script>

<!-- Nestable -->
<script src="assets/js/uikit/js/uikit.min.js"></script>
<script src="assets/js/uikit/js/addons/nestable.min.js"></script>

<!-- UI tree -->
<script src="assets/js/ui-tree/angular-ui-tree.js"></script>
<link rel="stylesheet" href="assets/js/ui-tree/angular-ui-tree.min.css">
<!-- App -->
<script src="assets/js/ng-flow/ng-flow-standalone.min.js"></script>
<script src="app/js/app.js"></script>

<!-- services -->
<script src="app/js/services/index.js"></script>
<script src="app/js/services/user.js"></script>
<script src="app/js/services/alert.js"></script>
<script src="app/js/services/team.js"></script>
<script src="app/js/services/orgunit.js"></script>
<script src="app/js/services/company.js"></script>

<!--xeon services -->
<script src="app/js/services.js"></script>

<script src="app/js/directives.js"></script>
<script src="app/js/factory.js"></script>

<!-- controllers -->
<script src="app/js/controllers/index.js"></script>
<script src="app/js/controllers/users.js"></script>
<script src="app/js/controllers/teams.js"></script>
<script src="app/js/controllers/orgunits.js"></script>
<script src="app/js/controllers/companies.js"></script>


<!-- xeon controllers-->
<script src="app/js/controllers.js"></script>


<!-- JavaScripts initializations and stuff -->
<script src="app/js/xenon-custom.js"></script>
<script src="app/js/main.js"></script>

</body>
</html>