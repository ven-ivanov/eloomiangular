<?php
use Eloomi\Models\Company;
use Cartalyst\Sentry\Facades\Laravel\Sentry;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Resources
$numberRegex = '[1-9][0-9]*';
Route::pattern('id', $numberRegex);
Route::pattern('user', $numberRegex);
Route::pattern('organizational_unit', $numberRegex);
Route::pattern('course', $numberRegex);
Route::pattern('course', $numberRegex);
Route::pattern('team', $numberRegex);
Route::pattern('module', $numberRegex);
Route::pattern('question', $numberRegex);
Route::pattern('answer', $numberRegex);
Route::pattern('search_term', $numberRegex);
Route::pattern('company', $numberRegex);
Route::filter('scope', function()
{
    $company = Company::where('domain', '=', Request::getHost())->select('id')->first(1);
    if (!$company){
        $company_id = -1; // No data will be available with this company_id, but correct error messages will be shown
    } else {
        $company_id = $company->id;
    }
//    $company_id = 2;
    TenantScope::addTenant('company_id', $company_id);
});

Route::filter('authorized', function()
{

    if ( ! Sentry::check()) {
        throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('You are not logged in');
    }
    $user = Sentry::getUser();
    if ($user->language){
        App::setLocale($user->language->code);
        return;
    }

    if ($user->company->language){
        App::setLocale($user->company->language->code);
        return;
    }

});

Route::filter('has_permissions', function($route, $request, $value)
{
    try
    {
        $user = Sentry::getUser();

        if( ! $user->hasAccess($value))
        {
            throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('You are not allowed to do this');
        }
    }
    catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
    {
        throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException('You are not logged in');
    }

});


//Route::when('*', 'csrf', ['post', 'put', 'patch']);

Route::get('file/{id}', [
    'as' => 'file.download',
    'before' => 'scope|authorized',
    'uses' => 'FileController@download'
]);
Route::post('api/user/{user}/activate', [
    'as' => 'api.user.activate',
    'before' => 'scope',
    'uses' => 'UserController@activate'
]);

Route::post ('api/user/forgotpassword', [
    'as' => 'api.user.password_reset_request',
    'before' => 'scope',
    'uses' => 'UserController@requestPasswordReset'
]);
Route::post('api/user/{user}/reset_password', [
    'as' => 'api.user.password_reset_action',
    'before' => 'scope',
    'uses' => 'UserController@resetPassword'
]);

Route::group(['prefix' => 'api/misc'], function() {
    Route::get('timezones', 'MiscController@timezones');
    Route::get('languages', 'MiscController@languages');
    Route::get('timespans', 'MiscController@timespans');
    Route::get('app_parts', 'MiscController@app_parts');
    Route::get('groups', ['before' => 'scope|authorized', 'uses' => 'MiscController@groups']);
});

Route::group(['before' => 'scope', 'prefix' => 'api'], function() {
    Route::post('sessions', 'SessionController@store');
    Route::get('sessions', 'SessionController@create');
    Route::group(['before' => 'authorized'], function() {

        Route::resource('file', 'FileController');


        Route::delete('sessions', 'SessionController@destroy');

        Route::resource('user', 'UserController');
        Route::post('user/multi_delete',          ['as' => 'api.user.multi_destroy', 'uses' => 'UserController@multiDestroy']);
        Route::post('user/deactivate',            ['as' => 'api.user.deactivate', 'uses' => 'UserController@deactivate']);
        Route::post('user/request_activation',    ['as' => 'api.user.request_activation', 'uses' => 'UserController@requestActivation']);
        Route::post('user/import',                ['as' => 'api.user.import_users', 'uses' => 'UserController@importUsers']);
        Route::get('user/search/{search_term}',   ['as' => 'api.user.search', 'uses' => 'UserController@search']);

        Route::resource('team', 'TeamController');
        Route::delete('team', ['as' => 'api.team.multi_delete', 'uses' => 'TeamController@multiDestroy']);
        Route::post('team/import', ['as' => 'api.team.import_teams', 'uses' => 'TeamController@importTeams']);
        Route::post('team/{team}/import', ['as' => 'api.team.import_users', 'uses' => 'TeamController@importUsers']);
        Route::get('team/search/{search_term}', ['as' => 'api.team.search', 'uses' => 'TeamController@search']);

        Route::resource('organizational-unit', 'OrganizationalUnitController');
        Route::post('organizational-unit/multi_delete', ['as' => 'api.team.multi_destroy', 'uses' => 'OrganizationalUnitController@multiDestroy']);
        Route::post('organizational-unit/import', ['as' => 'api.organizational-unit.import_units', 'uses' => 'OrganizationalUnitController@importUnits']);
        Route::post('organizational-unit/{unit}/import', ['as' => 'api.organizational-unit.import_users', 'uses' => 'OrganizationalUnitController@importUsers']);
        Route::get('organizational-units/search/{search_term}', ['as' => 'api.organizational-unit.search', 'uses' => 'OrganizationalUnitController@search']);

        Route::resource('course', 'CourseController');
        Route::get('course/all', ['as' => 'api.course.all', 'uses' => 'CourseController@indexAll']);
        Route::get('course/search/{search_term}', ['as' => 'api.course.search', 'uses' => 'CourseController@search']);

        Route::resource('module', 'ModuleController', ['except' => ['index']]);

        Route::resource('question', 'QuestionController', ['except' => ['index']]);
        Route::resource('answer', 'MultipleChoiceAnswerController', ['except' => ['index']]);

        Route::resource('company', 'CompanyController');
        Route::get('company/me', 'CompanyController@current_company');

        Route::post('progress', 'ProgressController@userCompletedModules');

        Route::get('shop',          'CourseShopController@index');
        Route::post('shop/buy',     'CourseShopController@buy');
        Route::post('shop/assign',  'CourseShopController@assign');
        Route::post('shop/request', 'CourseShopController@request');
    });

});

Route::get('/', function(){
    return \View::make('static.index');
});